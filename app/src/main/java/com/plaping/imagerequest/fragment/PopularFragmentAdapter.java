package com.plaping.imagerequest.fragment;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Post;
import com.plaping.imagerequest.api.model.custom.PostResponse;
import com.plaping.imagerequest.view.PostListItem;
import com.plaping.imagerequest.view.popular.PopularUserListItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by deknaew_bws on 3/31/16.
 */
public class PopularFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ImageRequestApplication app;
    private List<Post> postList = new ArrayList<>();

    public PopularFragmentAdapter(Context context) {
        this.context = context;
        app = (ImageRequestApplication) context.getApplicationContext();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) return new PopularUserViewHolder(new PopularUserListItem(context));
        return new PostViewHolder(new PostListItem(context));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostViewHolder) {
            ((PostViewHolder) holder).fill(postList.get(position - 1));
        }
    }

    @Override
    public int getItemCount() {
        return postList.size() + 1;
    }

    public void reload(Handler.Callback callback) {
        postList.clear();
        load(callback);
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {

        private PostListItem item;


        public PostViewHolder(PostListItem itemView) {
            super(itemView);
            item = itemView;
        }


        public void fill(Post post) {
            item.fill(post);
        }


    }

    public void load(final Handler.Callback callback) {
        app.getHttpService().getPopularFeed(app.getAccessToken(), new ImageRequestHttpService.OnResponseCallback<PostResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, PostResponse data) {

                Log.d("FeedFragmentAdapter", "Success : " + success);
                callback.handleMessage(new Message());

                if (success) {
                    postList.clear();
                    Log.d("FeedFragmentAdapter", "Length : " + data.getPosts().length);
                    postList.addAll(Arrays.asList(data.getPosts()));
                    notifyDataSetChanged();
                }

            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return 0;
        else return 1;
    }


    private class PopularUserViewHolder extends RecyclerView.ViewHolder {

        private PopularUserListItem item;

        public PopularUserViewHolder(PopularUserListItem itemView) {
            super(itemView);
            this.item = itemView;
        }


    }
}

package com.plaping.imagerequest.fragment.user;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.fragment.BaseFragment;


public class PostFragment extends BaseFragment {


    private int scrollState;
    private RecyclerView rcvUser;
    private User user;
    private SwipeRefreshLayout sloPost;


    public static PostFragment newInstance() {
        PostFragment fragment = new PostFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        rcvUser = (RecyclerView) view.findViewById(R.id.rcv_post);
        rcvUser.setLayoutManager(new LinearLayoutManager(getActivity()));
        sloPost = (SwipeRefreshLayout) view.findViewById(R.id.slo_post);

        final PostFragmentAdapter adapter = new PostFragmentAdapter(user, getActivity());

        rcvUser.setAdapter(adapter);
        adapter.load(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                return false;
            }
        });


        sloPost.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.load(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {

                        sloPost.setRefreshing(false);
                        return false;
                    }
                });
            }
        });


        rcvUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                Log.d("PostOnScroll", "State : " + newState);
                scrollState = newState;
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {

                    getActivity().sendBroadcast(new Intent("scrollToTop"));
                    Log.d("PostOnScroll", "dY : " + dy);
                }


            }
        });


        return view;
    }


    public void setUser(User user) {
        this.user = user;
    }
}

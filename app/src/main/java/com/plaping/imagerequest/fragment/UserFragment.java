package com.plaping.imagerequest.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.MainActivity;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.utils.ScreenUtils;
import com.plaping.imagerequest.view.profile.UserProfileListItem;

/**
 * Created by deknaew_bws on 4/9/16.
 */
public class UserFragment extends BaseFragment {

    private BroadcastReceiver scrollToTopBroadcastReceiver;
    private IntentFilter filter;

    private ViewPager vpUser;
    private TabLayout tabLayout;
    private LinearLayout rloContent;

    private RelativeLayout rloOverVp;
    private ScrollView scvUser;
    private RelativeLayout rloUserProfileHeader;

    private UserProfileListItem userProfileListItem;
    private ImageRequestApplication app;
    private User user;

    public static UserFragment newInstance(User user) {
        UserFragment fragment = new UserFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_user, container, false);
        app = (ImageRequestApplication) getActivity().getApplicationContext();
        rloContent = (LinearLayout) view.findViewById(R.id.rlo_content);
        vpUser = (ViewPager) view.findViewById(R.id.vp_user);
        rloOverVp = (RelativeLayout) view.findViewById(R.id.rlo_over_vp);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        scvUser = (ScrollView) view.findViewById(R.id.scv_user);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#f1c40f"));
        tabLayout.setTabTextColors(Color.WHITE, Color.parseColor("#f1c40f"));
        rloUserProfileHeader = (RelativeLayout) view.findViewById(R.id.rlo_user_profile_header);
        userProfileListItem = (UserProfileListItem) view.findViewById(R.id.user_profile_list_item);

        user = (User) getArguments().getSerializable("user");

        if (user == null) {
            userProfileListItem.fill(app.getUserManager().getUser());
        } else {
            userProfileListItem.fill(user);
        }

        scrollToTopBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Log.d("PostOnScroll", "ScrollToTop");


                scrollToTop();


            }
        };

        filter = new IntentFilter("scrollToTop");
        getActivity().registerReceiver(scrollToTopBroadcastReceiver, filter);

        rloOverVp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        rloOverVp.setVisibility(View.GONE);


        float allHeight = 0;
        if (getActivity() instanceof MainActivity) {
            allHeight = ((MainActivity) getActivity()).getTabHeight();

            Log.d("UserFragment", "Tab Height : " + ((MainActivity) getActivity()).getTabHeight());
        } else {
            allHeight += ScreenUtils.convertDpToPixel(50, getActivity());
        }
        allHeight += ScreenUtils.convertDpToPixel(50, getActivity());

        allHeight += getStatusBarHeight();

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        Log.d("UserFragment", "Height : " + height + " >> Left part heigh : " + (int) allHeight);
        rloContent.setLayoutParams(new LinearLayout.LayoutParams(width, height - (int) allHeight));

        User user = (User) getArguments().getSerializable("user");
        if (user == null) {
            user = app.getUserManager().getUser();
        }

        UserFragmentAdapter adapter = new UserFragmentAdapter(getActivity(), user, getChildFragmentManager());
        vpUser.setAdapter(adapter);
        vpUser.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(vpUser);


        return view;

    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void scrollToTop() {

        scvUser.smoothScrollTo(0, scvUser.getBottom());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(scrollToTopBroadcastReceiver);
    }
}

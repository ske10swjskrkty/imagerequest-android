package com.plaping.imagerequest.fragment.user;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Post;
import com.plaping.imagerequest.api.model.custom.PostResponse;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.view.PostListItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by deknaew_bws on 4/1/16.
 */
public class PostFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ImageRequestApplication app;
    private List<Post> postList = new ArrayList<>();
    private User user;

    public PostFragmentAdapter(User user, Context context) {
        this.context = context;
        app = (ImageRequestApplication) context.getApplicationContext();
        this.user = user;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PostViewHolder(new PostListItem(context));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostViewHolder) {
            ((PostViewHolder) holder).fill(postList.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    private class PostViewHolder extends RecyclerView.ViewHolder {

        private PostListItem item;


        public PostViewHolder(PostListItem itemView) {
            super(itemView);
            item = itemView;
        }

        public void fill(Post post) {
            item.fill(post);
        }


    }


    public void load(final Handler.Callback callback) {
        app.getHttpService().getPosts(app.getAccessToken(), user.getId(), new ImageRequestHttpService.OnResponseCallback<PostResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, PostResponse data) {
                if (success) {


                    postList.clear();
                    Log.d("PostFragment", "DataLen : " + data.getPosts().length);
                    postList.addAll(Arrays.asList(data.getPosts()));
                    Collections.reverse(postList);
                    notifyDataSetChanged();
                } else {
                    Log.d("PostFragment", "Failed");

                }
                callback.handleMessage(new Message());
            }
        });


    }
}
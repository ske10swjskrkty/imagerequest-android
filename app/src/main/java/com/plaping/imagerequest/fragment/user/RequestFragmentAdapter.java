package com.plaping.imagerequest.fragment.user;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Request;
import com.plaping.imagerequest.api.model.custom.RequestResponse;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.view.RequestListItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by deknaew_bws on 4/1/16.
 */
public class RequestFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ImageRequestApplication app;
    private List<Request> requestList = new ArrayList<>();

    private User user;


    public RequestFragmentAdapter(Context context, User user) {
        this.context = context;
        this.app = (ImageRequestApplication) context.getApplicationContext();
        this.user = user;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RequestViewHolder(new RequestListItem(context));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((RequestViewHolder) holder).fill(requestList.get(position));
    }

    @Override
    public int getItemCount() {
        return requestList.size();
    }

    private class RequestViewHolder extends RecyclerView.ViewHolder {

        private RequestListItem item;


        public RequestViewHolder(RequestListItem itemView) {
            super(itemView);
            item = itemView;
        }

        public void fill(Request request) {
            this.item.fill(request);
        }


    }

    public void load(final Handler.Callback callback) {
        app.getHttpService().getRequestToOthers(app.getAccessToken(), user.getId(), new ImageRequestHttpService.OnResponseCallback<RequestResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, RequestResponse data) {
                callback.handleMessage(new Message());
                if (success) {
                    requestList.clear();
                    requestList.addAll(Arrays.asList(data.getRequests()));
                    Collections.reverse(requestList);

                    notifyDataSetChanged();
                } else {

                }

            }
        });
    }

}
package com.plaping.imagerequest.fragment.feed;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Request;
import com.plaping.imagerequest.api.model.custom.RequestResponse;
import com.plaping.imagerequest.view.RequestListItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by deknaew_bws on 4/17/16.
 */
public class RequestFeedFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ImageRequestApplication app;
    private List<Request> requestList = new ArrayList<>();

    public RequestFeedFragmentAdapter(Context context) {
        this.context = context;
        app = (ImageRequestApplication) context.getApplicationContext();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RequestListViewHolder(new RequestListItem(context));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((RequestListViewHolder) holder).fill(requestList.get(position));
    }

    @Override
    public int getItemCount() {
        return requestList.size();
    }

    private class RequestListViewHolder extends RecyclerView.ViewHolder {

        private RequestListItem requestListItem;

        private RequestListViewHolder(RequestListItem itemView) {
            super(itemView);
            this.requestListItem = itemView;
        }

        private void fill(Request request) {
            this.requestListItem.fill(request);
        }


    }

    public void reload(Handler.Callback callback) {
        requestList.clear();
        load(callback);
    }


    public void load(final Handler.Callback callback) {
        app.getHttpService().getRequestFeed(app.getAccessToken(), new ImageRequestHttpService.OnResponseCallback<RequestResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, RequestResponse data) {
                callback.handleMessage(new Message());

                if (success) {
                    requestList.addAll(Arrays.asList(data.getRequests()));
                }
            }
        });
    }


}

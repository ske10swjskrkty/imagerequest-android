package com.plaping.imagerequest.fragment.user;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.fragment.BaseFragment;

/**
 * Created by deknaew_bws on 4/9/16.
 */
public class MyRequestFragment extends BaseFragment {

    private RecyclerView rcvUser;
    private int scrollState;
    private User user;
    private SwipeRefreshLayout sloMyRequest;

    public void setUser(User user) {
        this.user = user;
    }

    public static MyRequestFragment newInstance() {
        MyRequestFragment fragment = new MyRequestFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_request, container, false);
        rcvUser = (RecyclerView) view.findViewById(R.id.rcv_my_request);
        rcvUser.setLayoutManager(new LinearLayoutManager(getActivity()));

        final MyRequestFragmentAdapter adapter = new MyRequestFragmentAdapter(getActivity(), user);
        rcvUser.setAdapter(adapter);
        adapter.load(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                return false;
            }
        });
        sloMyRequest = (SwipeRefreshLayout) view.findViewById(R.id.slo_my_request);
        sloMyRequest.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.load(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {
                        sloMyRequest.setRefreshing(false);
                        return false;
                    }
                });
            }
        });
        rcvUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                Log.d("PostOnScroll", "State : " + newState);
                scrollState = newState;
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 10) {

                    getActivity().sendBroadcast(new Intent("scrollToTop"));
                    Log.d("PostOnScroll", "dY : " + dy);
                }


            }
        });

        return view;
    }


}

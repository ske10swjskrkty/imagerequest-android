package com.plaping.imagerequest.fragment.feed;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.fragment.BaseFragment;

public class RequestFeedFragment extends BaseFragment {

    private RequestFeedFragmentAdapter adapter;
    private RecyclerView rcvRequestFeed;
    private SwipeRefreshLayout sloRequestFeed;

    public static RequestFeedFragment newInstance() {
        RequestFeedFragment fragment = new RequestFeedFragment();
        return fragment;
    }

    public RequestFeedFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_feed, container, false);
        adapter = new RequestFeedFragmentAdapter(getActivity());
        rcvRequestFeed = (RecyclerView) view.findViewById(R.id.rcv_request_feed);
        rcvRequestFeed.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvRequestFeed.setAdapter(adapter);
        sloRequestFeed = (SwipeRefreshLayout) view.findViewById(R.id.slo_request_feed);

        sloRequestFeed.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.reload(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {

                        sloRequestFeed.setRefreshing(false);
                        return false;
                    }
                });
            }
        });
        adapter.load(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                return false;
            }
        });


        return view;
    }


}

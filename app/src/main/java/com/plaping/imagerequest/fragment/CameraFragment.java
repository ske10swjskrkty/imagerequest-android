package com.plaping.imagerequest.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.FilterActivity;
import com.plaping.imagerequest.activities.PhotoActivity;
import com.plaping.imagerequest.api.model.custom.Request;
import com.plaping.imagerequest.view.camera.CameraView;
import com.plaping.imagerequest.view.camera.DrawingView;

import java.util.List;


public class CameraFragment extends BaseFragment implements BitmapProduceable {

    private FrameLayout rloCameraView;
    private CameraView cameraView;
    private DrawingView drawingView;
    private SurfaceView surfaceView;
    private Camera camera;
    private RelativeLayout rloFlash;
    private RelativeLayout rloShutter;
    private RelativeLayout rloSwap;
    private Bitmap savedBitmap;
    private TextView tvFlash;
    private int cameraId;
    private ImageView imvFlash;

    private ScaleAnimation scaleAnimation;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        if (!checkCameraHardware(getActivity())) {
            Toast.makeText(getActivity(), "No Camera Detected!", Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }

        scaleAnimation = new ScaleAnimation(1, -1, 1, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(200);
        surfaceView = new SurfaceView(getActivity());
        tvFlash = (TextView) view.findViewById(R.id.tv_flash);
        tvFlash.setText("off");
        drawingView = new DrawingView(getActivity());
        drawingView.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        ));

        cameraView = new CameraView(getActivity(), surfaceView, drawingView);
        cameraView.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        ));
        cameraView.setKeepScreenOn(true);
//
        rloCameraView = (FrameLayout) view.findViewById(R.id.rlo_camera);
        rloFlash = (RelativeLayout) view.findViewById(R.id.rlo_flash);
        rloShutter = (RelativeLayout) view.findViewById(R.id.rlo_shutter);
        rloSwap = (RelativeLayout) view.findViewById(R.id.rlo_swap);
        imvFlash = (ImageView) view.findViewById(R.id.imv_flash);

        rloShutter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread() {
                    @Override
                    public void run() {
                        takePicture();
                    }
                }.start();
            }
        });

        rloSwap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.startAnimation(scaleAnimation);

                int camCount = Camera.getNumberOfCameras();
                if (camCount < 2) return;

                stopCamera();
                cameraId = (cameraId + 1) % 2;


                if (cameraId == 1) {
                    drawingView.setVisibility(View.GONE);
                    rloFlash.setAlpha(0.5f);

                } else {
                    drawingView.setVisibility(View.VISIBLE);
                    rloFlash.setAlpha(1f);
                }
                startCamera();
            }
        });


        rloFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (camera != null) {
                    Camera.Parameters p = cameraButtonSetup();
                    if (p != null) camera.setParameters(p);
                }
            }
        });


        rloCameraView.addView(cameraView);
        rloCameraView.addView(drawingView);

        cameraButtonSetup();


        return view;
    }

    private Camera.Parameters cameraButtonSetup() {
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters.getFlashMode().equals(Camera.Parameters.FLASH_MODE_ON)) {

                //TODO edit the flash reaction when toggle the flash
                tvFlash.setText("off");
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            } else {
                tvFlash.setText("on");
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            }

            return parameters;
        }
        return null;
    }

    protected void stopCamera() {
        if (camera != null) {
            camera.stopPreview();
            cameraView.setCamera(null);
            camera.release();
            camera = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopCamera();
    }

    @Override
    public void onResume() {
        super.onResume();

        startCamera();

    }

    private Camera.Size chooseBestSize(List<Camera.Size> sizes) {

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float heightPixels = metrics.heightPixels * 1.0f;
        float widthPixels = metrics.widthPixels * 1.0f;

        for (Camera.Size s : sizes) {

            if (s.equals(camera.getParameters().getPreviewSize())) {

                Log.d("ChooseBestSize", "Size : " + s.width + " x " + s.height);
                return s;
            }
        }
        return sizes.get(sizes.size() / 2);
    }

    protected void takePicture() {
        try {
            MediaActionSound sound = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                sound = new MediaActionSound();
                sound.play(MediaActionSound.SHUTTER_CLICK);
            }

            Log.d("Post", "capture!");


            Camera.Parameters parameters = camera.getParameters();
            Camera.Size biggestSize = chooseBestSize(parameters.getSupportedPictureSizes());

            parameters.setPictureSize(biggestSize.width, biggestSize.height);
            camera.setParameters(parameters);
            Log.d("Post", "CameraActivity : Width : " + parameters.getPictureSize().width + " /// Height : " + parameters.getPictureSize().height + "");
            for (Camera.Size s : parameters.getSupportedPictureSizes()) {
                Log.d("Post", "CameraActivity : Picture size : " + s.width + " x " + s.height);
            }

            camera.takePicture(
                    null,
                    null,
                    new Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(final byte[] data, Camera camera) {

                            final ProgressDialog dialog = new ProgressDialog(getActivity());
                            dialog.setMessage("In Progress...");
                            dialog.setCancelable(false);
                            dialog.show();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {


//                                    String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/pp_temp";
//                                    File dirFile = new File(dir);
//                                    if (!dirFile.exists()) {
//                                        dirFile.mkdir();
//                                    }
//
//                                    File pictureFile = new File(dirFile.getAbsolutePath() + "/" + new Date().getTime() + ".png");
//
//                                    try {
//                                        FileOutputStream fos = new FileOutputStream(pictureFile);
//                                        fos.write(data, 0, data.length);
//                                    } catch (FileNotFoundException e) {
//                                        e.printStackTrace();
//                                    } catch (IOException e) {
//                                        e.printStackTrace();
//                                    }


                                    savedBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                                    int width = Math.min(savedBitmap.getHeight(), savedBitmap.getWidth());

                                    Log.d("CameraFragment", "Image Width : " + savedBitmap.getWidth() + " || Height : " + savedBitmap.getHeight());

                                    Matrix matrix = new Matrix();
                                    Bitmap squareBitmap = null;
                                    if (cameraId == 0) {

                                        matrix.postRotate(90);
                                        squareBitmap = savedBitmap.createBitmap(savedBitmap, 0, 0, savedBitmap.getHeight(), savedBitmap.getHeight(), matrix, false);
                                    } else if (cameraId == 1) {

                                        matrix.postScale(-1, 1);
                                        matrix.postRotate(90);
                                        squareBitmap = savedBitmap.createBitmap(savedBitmap, savedBitmap.getWidth() - savedBitmap.getHeight(), 0, savedBitmap.getHeight(), savedBitmap.getHeight(), matrix, false);
                                    }


                                    if (squareBitmap != savedBitmap) {
                                        savedBitmap.recycle();
                                    }
//                            final Bitmap ret = squareBitmap.createScaledBitmap(squareBitmap, 800, 800, false);


                                    ImageRequestApplication app = (ImageRequestApplication) getActivity().getApplicationContext();
                                    app.getBitmapManager().saveBitmapToSharedPreference(squareBitmap, "postphoto");

                                    dialog.dismiss();
                                    Log.d("CameraFragment", "dismiss");


                                    Intent intent = new Intent();
//                                    intent.putExtra("imagepath", pictureFile.getAbsolutePath());
                                    getActivity().setResult(Activity.RESULT_OK, intent);

                                    if (getActivity().getClass().getName().contains("CameraActivity")) {
                                        getActivity().finish();
                                    }


                                    boolean isInAddEditMode = ((PhotoActivity) getActivity()).isInAddEditMode();

                                    if (!isInAddEditMode) {
                                        Intent cropIntent = new Intent(getActivity(), FilterActivity.class);

                                        Request request = (Request) getActivity().getIntent().getSerializableExtra("request");
                                        cropIntent.putExtra("request", request);

                                        startActivity(cropIntent);
                                    } else {
                                        getActivity().setResult(Activity.RESULT_OK);
                                        getActivity().finish();
                                    }
                                }
                            }).start();
                        }
                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void startCamera() {
        Log.d("CameraActivity", "StartCamera");
        int numCams = Camera.getNumberOfCameras();
        if (numCams > 0) {

            camera = Camera.open(cameraId);
            Camera.Parameters param = camera.getParameters();
            camera.startPreview();
            cameraView.setCamera(camera);
        }
    }


    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @Override
    public Bitmap getBitmap() {
        return null;
    }
}
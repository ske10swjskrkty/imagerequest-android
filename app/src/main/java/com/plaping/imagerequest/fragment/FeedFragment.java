package com.plaping.imagerequest.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.R;

public class FeedFragment extends BaseFragment {

    private FeedfragmentAdapter adapter;
    private ViewPager vpFeed;
    private TabLayout tabLayout;

    public static FeedFragment newInstance() {
        FeedFragment fragment = new FeedFragment();
        return fragment;
    }

    public FeedFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        adapter = new FeedfragmentAdapter(getChildFragmentManager());
        vpFeed = (ViewPager) view.findViewById(R.id.vp_feed);
        vpFeed.setAdapter(adapter);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(vpFeed);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#f1c40f"));
        tabLayout.setTabTextColors(Color.WHITE, Color.parseColor("#f1c40f"));

        return view;
    }

}

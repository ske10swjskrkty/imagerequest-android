package com.plaping.imagerequest.fragment.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.fragment.BaseFragment;

/**
 * Created by deknaew_bws on 4/18/16.
 */
public class SearchPostFragment extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_post, container, false);

        return view;
    }
}

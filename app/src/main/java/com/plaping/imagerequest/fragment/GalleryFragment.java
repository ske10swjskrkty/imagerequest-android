package com.plaping.imagerequest.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.ImageGalleryActivity;
import com.plaping.imagerequest.api.model.custom.Photo;
import com.plaping.imagerequest.utils.GalleryUtils;
import com.plaping.imagerequest.view.gallery.GalleryViewAdapter;
import com.plaping.imagerequest.view.gallery.ItemClickListener;
import com.plaping.imagerequest.view.photo.CropView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GalleryFragment extends BaseFragment implements BitmapProduceable {

    private RecyclerView rcvGallery;
    private CropView cvPhoto;
    private GalleryViewAdapter adapter;
    private ArrayAdapter<String> spArrayAdapter;
    private List<String> albumsNameList = new ArrayList<String>();
    private Spinner spAlbumName;
    private FloatingActionButton fabGallery;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_gallery, null, false);
        fabGallery = (FloatingActionButton) view.findViewById(R.id.fab_gallery);
        rcvGallery = (RecyclerView) view.findViewById(R.id.rcv_gallery);
        cvPhoto = (CropView) view.findViewById(R.id.cv_photo);
        List<Photo> photoList = GalleryUtils.getImagesPath(getActivity());

        Collections.reverse(photoList);
        if(photoList.size()<=0){
            cvPhoto.setEmpty(true);
        }else{
            cvPhoto.setEmpty(false);
        }


        if (photoList.size() > 0) {
            cvPhoto.setDisplayImage(photoList.get(0).getPath());
        }

        adapter = new GalleryViewAdapter(getActivity(), photoList, true);
        rcvGallery.setAdapter(adapter);
        rcvGallery.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, Object obj, int position) {
                Photo p = (Photo) obj;

                Log.d("PhotoActivity", "Path : " + p.getPath());
                cvPhoto.setDisplayImage(p.getPath());

            }
        });

        fabGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Pressed", Toast.LENGTH_SHORT).show();
                getActivity().startActivityForResult(new Intent(getActivity(), ImageGalleryActivity.class), 444);
            }
        });

        rcvGallery.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    fabGallery.show();
                } else {
                    fabGallery.hide();

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        return view;
    }

    public void setEmpty(boolean isEmpty) {
        cvPhoto.setEmpty(isEmpty);
    }

    public void setCurrentGalleryAlbum(String albumName) {
        adapter.setAlbumName(albumName);
    }

    @Override
    public Bitmap getBitmap() {
        return cvPhoto.getCroppedImageBitmap();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 444) {
            if (resultCode == Activity.RESULT_OK) {
                String path = data.getStringExtra("imagepath");
                cvPhoto.setDisplayImage(path);
            }
        }
    }
}


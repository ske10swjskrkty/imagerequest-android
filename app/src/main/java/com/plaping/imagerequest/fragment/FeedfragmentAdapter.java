package com.plaping.imagerequest.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.plaping.imagerequest.fragment.feed.PostFeedFragment;
import com.plaping.imagerequest.fragment.feed.RequestFeedFragment;
import com.plaping.imagerequest.fragment.user.RequestFragment;

/**
 * Created by deknaew_bws on 4/17/16.
 */
public class FeedfragmentAdapter extends FragmentPagerAdapter {

    private BaseFragment[] fragments;

    public FeedfragmentAdapter(FragmentManager fm) {
        super(fm);

        fragments = new BaseFragment[]{
                PostFeedFragment.newInstance(),
                RequestFeedFragment.newInstance()
        };
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) return "Post";
        return "Request";
    }
}

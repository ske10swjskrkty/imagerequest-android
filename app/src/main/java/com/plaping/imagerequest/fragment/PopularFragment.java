package com.plaping.imagerequest.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.R;


public class PopularFragment extends Fragment {

    private RecyclerView rcvPopular;
    private PopularFragmentAdapter adapter;
    private SwipeRefreshLayout sloPopular;


    public static PopularFragment newInstance() {
        PopularFragment fragment = new PopularFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_popular, container, false);
        rcvPopular = (RecyclerView) view.findViewById(R.id.rcv_popular);
        rcvPopular.setLayoutManager(new LinearLayoutManager(getActivity()));
        sloPopular = (SwipeRefreshLayout) view.findViewById(R.id.slo_popular);

        if (adapter == null) {
            adapter = new PopularFragmentAdapter(getActivity());
        }
        rcvPopular.setAdapter(adapter);
        adapter.load(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                return false;
            }
        });

        sloPopular.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.load(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {
                        sloPopular.setRefreshing(false);
                        return false;
                    }
                });
            }
        });


        return view;
    }


}

package com.plaping.imagerequest.fragment.feed;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Post;
import com.plaping.imagerequest.api.model.custom.PostResponse;
import com.plaping.imagerequest.view.PostListItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by deknaew_bws on 4/1/16.
 */
public class PostFeedFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ImageRequestApplication app;
    private List<Post> postList = new ArrayList<>();

    public PostFeedFragmentAdapter(Context context) {
        this.context = context;
        app = (ImageRequestApplication) context.getApplicationContext();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PostViewHolder(new PostListItem(context));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostViewHolder) {
            ((PostViewHolder) holder).fill(postList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public void reload(Handler.Callback callback) {
        postList.clear();
        load(callback);
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {

        private PostListItem item;


        public PostViewHolder(PostListItem itemView) {
            super(itemView);
            item = itemView;
        }


        public void fill(Post post) {
            item.fill(post);
        }


    }

    public void load(final Handler.Callback callback) {
        app.getHttpService().getFeed(app.getAccessToken(), new ImageRequestHttpService.OnResponseCallback<PostResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, PostResponse data) {

                Log.d("FeedFragmentAdapter", "Success : " + success);
                callback.handleMessage(new Message());
                if (success) {
                    Log.d("FeedFragmentAdapter", "Length : " + data.getPosts().length);
                    postList.addAll(Arrays.asList(data.getPosts()));
                    notifyDataSetChanged();
                }

            }
        });

    }
}

package com.plaping.imagerequest.fragment;

import android.graphics.Bitmap;

/**
 * Created by deknaew_bws on 11/26/15.
 */
public interface BitmapProduceable {

    Bitmap getBitmap();
}

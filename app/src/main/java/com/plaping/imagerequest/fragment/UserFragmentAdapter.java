package com.plaping.imagerequest.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.fragment.user.MyRequestFragment;
import com.plaping.imagerequest.fragment.user.PostFragment;
import com.plaping.imagerequest.fragment.user.RequestFragment;

/**
 * Created by deknaew_bws on 4/1/16.
 */
public class UserFragmentAdapter extends FragmentPagerAdapter {

    private BaseFragment[] fragments;
    private ImageRequestApplication app;
    private User user;

    public UserFragmentAdapter(Context context, User user, FragmentManager fm) {
        super(fm);

        PostFragment postFragment = PostFragment.newInstance();
        postFragment.setUser(user);
        RequestFragment requestFragment = RequestFragment.newInstance();
        requestFragment.setUser(user);
        MyRequestFragment myRequestFragment = MyRequestFragment.newInstance();
        myRequestFragment.setUser(user);
        fragments = new BaseFragment[]{
                postFragment,
                requestFragment,
                myRequestFragment
        };

        this.user = user;


    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        Log.d("UserFragmentAdapter", "User : " + user);

        if (position == 0) return user.getPostCount() + "\nPost";
        if (position == 1) return "Request\nto Others";
        return user.getRequestCount() + "\nRequest in";
    }
}
package com.plaping.imagerequest.fragment.feed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.fragment.BaseFragment;


public class PostFeedFragment extends BaseFragment {

    private RecyclerView rcvFeed;
    private SwipeRefreshLayout sloFeed;
    private BroadcastReceiver publicationFinishBroadcastReceiver;
    private IntentFilter filter;


    public static PostFeedFragment newInstance() {
        PostFeedFragment fragment = new PostFeedFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_feed, container, false);

        rcvFeed = (RecyclerView) view.findViewById(R.id.rcv_feed);
        sloFeed = (SwipeRefreshLayout) view.findViewById(R.id.slo_feed);


        rcvFeed.setLayoutManager(new LinearLayoutManager(getActivity()));
        final PostFeedFragmentAdapter adapter = new PostFeedFragmentAdapter(getActivity());
        rcvFeed.setAdapter(adapter);
        adapter.load(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {


                return false;
            }
        });


        publicationFinishBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                adapter.reload(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {
                        return false;
                    }
                });
            }
        };

        filter = new IntentFilter("publicationFinished");
        getActivity().registerReceiver(publicationFinishBroadcastReceiver,filter);

        sloFeed.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.reload(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {

                        sloFeed.setRefreshing(false);
                        return false;
                    }
                });
            }
        });


        return view;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(publicationFinishBroadcastReceiver);
    }
}

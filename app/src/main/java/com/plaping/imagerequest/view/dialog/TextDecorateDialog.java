package com.plaping.imagerequest.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.plaping.imagerequest.R;


/**
 * Created by Bestiiz on 18/3/2559.
 */
public class TextDecorateDialog extends Dialog {
    private EditText editText;
    private RelativeLayout rloOk;
    private RelativeLayout rloCancel;
    public TextDecorateDialog(Context context) {
        super(context);
    }

    public TextDecorateDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected TextDecorateDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        View decorView = getWindow().getDecorView();
        setContentView(R.layout.view_dialog_text_decorate);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        editText = (EditText) findViewById(R.id.editText);
        rloOk = (RelativeLayout) findViewById(R.id.rlo_ok);
        rloCancel = (RelativeLayout) findViewById(R.id.rlo_cancel);




    }

    public void setRloOKOnClickListener(View.OnClickListener listener){
        rloOk.setOnClickListener( listener);

    }

    public void setRloCancelOnClickListener(View.OnClickListener listener){
        rloCancel.setOnClickListener(listener);

    }

    public String getText(){

        return editText.getText().toString();
    }

    public void setText(String text){
        editText.setText(text);

    }
}

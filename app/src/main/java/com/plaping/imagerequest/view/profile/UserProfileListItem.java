package com.plaping.imagerequest.view.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.LoginActivity;
import com.plaping.imagerequest.activities.RequestActivity;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.utils.UrlUtils;

/**
 * Created by deknaew_bws on 4/1/16.
 */
public class UserProfileListItem extends RelativeLayout {

    private Context context;
    private ImageView imvProfile;
    private TextView tvUsername;
    private TextView tvTotalShare;
    private RelativeLayout rloRequest;
    private ImageRequestApplication app;
    private TextView tvLogout;

    public UserProfileListItem(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public UserProfileListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public UserProfileListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    public void fill(final User user) {

        if (user == null) return;

        if (user.getImgUrl() != null) {
            if (user.getImgUrl().contains(".gif")) {
                GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imvProfile);
                Glide.with(context)
                        .load(UrlUtils.BASE_IMG_URL + user.getImgUrl())
                        .placeholder(R.drawable.imgreq_placeholder)
                        .into(imageViewTarget);
            } else {
                Glide.with(context)
                        .load(UrlUtils.BASE_IMG_URL + user.getImgUrl())
                        .placeholder(R.drawable.imgreq_placeholder)
                        .into(imvProfile);
            }

        }

        if (user.getUsername().equals(app.getUserManager().getUser().getUsername())) {
            rloRequest.setVisibility(GONE);
        } else {
            rloRequest.setVisibility(VISIBLE);
        }

        if (app.getUserManager().getUser().getUsername().equals(user.getUsername())) {
            tvLogout.setVisibility(VISIBLE);

        } else {
            tvLogout.setVisibility(GONE);
        }

        rloRequest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RequestActivity.class);
                intent.putExtra("user", user);
                context.startActivity(intent);
            }
        });

        tvUsername.setText("" + user.getUsername());
        if (!app.getUserManager().getUser().getUsername().equals(user.getUsername())) {
            tvTotalShare.setText("" + user.getTotalShareCount() + " Total Shares");
        } else {
            tvTotalShare.setText("" + user.getTotalShareCount() + " Total Shares\n( " + user.getTotalCredits() + " credit )");
        }

    }

    private void setupView() {
        app = (ImageRequestApplication) context.getApplicationContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_list_user_profile, this);
        imvProfile = (ImageView) findViewById(R.id.imv_profile);
        tvTotalShare = (TextView) findViewById(R.id.tv_total_share);
        tvUsername = (TextView) findViewById(R.id.edt_username);
        rloRequest = (RelativeLayout) findViewById(R.id.rlo_request);
        tvLogout = (TextView) findViewById(R.id.tv_logout);
        tvLogout.setVisibility(GONE);
        tvLogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure want to log out?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPreferences sp = context.getSharedPreferences("ImageRequest", context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.remove("accessToken");
                                editor.commit();
                                context.startActivity(new Intent(context, LoginActivity.class));
                                context.sendBroadcast(new Intent("logOut"));
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                builder.create().show();
            }
        });

    }


}

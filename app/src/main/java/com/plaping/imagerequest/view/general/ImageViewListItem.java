package com.plaping.imagerequest.view.general;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.plaping.imagerequest.R;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ImageViewListItem extends RelativeLayout {

    private ImageView imvShow;
    private String imgPath;
    private RelativeLayout rloDarkHover;
    private ImageView imvDarkHover;
    private boolean isHorizontal = false;

    public ImageViewListItem(Context context, boolean isHorizontal) {
        super(context);
        this.isHorizontal = isHorizontal;
        setupView();

    }

    public ImageViewListItem(Context context) {
        super(context);
        this.isHorizontal = false;
        setupView();

    }

    private void setupView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (!isHorizontal) {
            view = inflater.inflate(R.layout.view_image_list_item, this, true);
        } else {
            view = inflater.inflate(R.layout.view_image_list_item_horizontal, this, true);
        }

        imvShow = (ImageView) view.findViewById(R.id.imv_show);
        rloDarkHover = (RelativeLayout) findViewById(R.id.rlo_click);
        imvDarkHover = (ImageView) findViewById(R.id.imv_click);
        imvDarkHover.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageViewListItem.this.performClick();
            }
        });
    }

    public void fill(String imagePath) {
        this.imgPath = imagePath;

        Picasso.with(getContext())
                .load(new File(imagePath))
                .config(Bitmap.Config.RGB_565)
                .resize(200, 200)
                .centerCrop()
                .placeholder(R.drawable.petpolar_plachholder)
                .into(imvShow);


    }
}

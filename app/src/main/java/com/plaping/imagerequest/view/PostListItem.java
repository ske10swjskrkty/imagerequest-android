package com.plaping.imagerequest.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.CommentActivity;
import com.plaping.imagerequest.activities.UserActivity;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Post;
import com.plaping.imagerequest.utils.UrlUtils;
import com.plaping.imagerequest.view.dialog.ShareDialog;
import com.plaping.imagerequest.view.general.TagListItem;

/**
 * Created by deknaew_bws on 3/31/16.
 */
public class PostListItem extends RelativeLayout {

    private TextView tvUsername;
    private ImageView imvProfile;
    private ImageView imvPhoto;
    private TextView tvComment;
    private RelativeLayout rloLike;
    private RelativeLayout rloComment;
    private RelativeLayout rloShare;
    private LinearLayout lloTags;
    private TextView tvLike;
    private TextView tvLikeCount;
    private Context context;
    private TextView tvCaption;
    private TextView tvTime;
    private boolean isLiked = false;
    private ImageRequestApplication app;


    public PostListItem(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public PostListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();

    }

    public PostListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();

    }

    private void setupView() {

        app = (ImageRequestApplication) context.getApplicationContext();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_list_post, this);
        tvComment = (TextView) findViewById(R.id.tv_comment);
        imvPhoto = (ImageView) findViewById(R.id.imv_photo);
        imvProfile = (ImageView) findViewById(R.id.imv_profile);
        tvLike = (TextView) findViewById(R.id.tv_like);
        rloComment = (RelativeLayout) findViewById(R.id.rlo_comment);
        rloLike = (RelativeLayout) findViewById(R.id.rlo_like);
        rloShare = (RelativeLayout) findViewById(R.id.rlo_post);
        lloTags = (LinearLayout) findViewById(R.id.llo_tags);
        tvCaption = (TextView) findViewById(R.id.tv_caption);
        tvUsername = (TextView) findViewById(R.id.edt_username);
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvLike = (TextView) findViewById(R.id.tv_like);
        tvLikeCount = (TextView) findViewById(R.id.tv_like_count);


//        Glide.with(context)
//                .load("http://49.media.tumblr.com/d5bd49ea12647946c7bf2da528b7888a/tumblr_o4v1ojajGZ1ql10y6o8_400.gif")
//                .into(imvPhoto);


    }

    public void like(Post post) {

        app.getHttpService().likePost(app.getAccessToken(), post.getId(), new ImageRequestHttpService.OnResponseCallback<String>() {
            @Override
            public void onResponse(boolean success, Throwable error, String data) {
                if (success) {
                    setIsLiked(true);
                }
            }
        });


    }

    public void unLike(Post post) {
        app.getHttpService().unLikePost(app.getAccessToken(), post.getId(), new ImageRequestHttpService.OnResponseCallback<String>() {
            @Override
            public void onResponse(boolean success, Throwable error, String data) {
                if (success) {
                    setIsLiked(false);
                }
            }
        });
    }


    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
        if (isLiked) {
            tvLike.setText("❤ Liked");
            tvLike.setTextColor(Color.parseColor("#e74c3c"));
        } else {
            tvLike.setText("Like");
            tvLike.setTextColor(Color.parseColor("#8C8C8C"));

        }
    }


    public void fill(final Post post) {
        isLiked = post.isLiked();
        setIsLiked(post.isLiked());


        lloTags.removeAllViews();
        for (String tag : post.getTags()) {
            TagListItem item = new TagListItem(context);
            item.setTagText(tag);
            lloTags.addView(item);
        }

        rloLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLiked) {
                    unLike(post);
                } else {
                    like(post);
                }
            }
        });


        if (post.getUser() != null) {
            tvUsername.setText(post.getUser().getUsername());

            Glide.with(context)
                    .load(UrlUtils.BASE_IMG_URL + post.getUser().getImgUrl())
                    .placeholder(R.drawable.imgreq_placeholder)
                    .into(imvProfile);
        }

        tvCaption.setText("" + post.getCaption() == null ? "" : post.getCaption());
        tvLikeCount.setText(post.getLikeCount() + " Like" + (post.getLikeCount() > 1 ? "s" : ""));
        tvComment.setText(post.getCommentCount() + " Comment" + (post.getCommentCount() > 1 ? "s" : ""));

        Log.d("DateCreated", "Date : " + post.getDateCreated());
        tvTime.setText(DateUtils.getRelativeTimeSpanString(post.getDateCreated(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));

        rloShare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareDialog dialog = new ShareDialog(context);
                dialog.show(post);
            }
        });
        Glide.with(context)
                .load(UrlUtils.BASE_IMG_URL + post.getImgUrl())
                .placeholder(R.drawable.imgreq_placeholder)
                .into(imvPhoto);
        imvProfile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context, UserActivity.class);
                intent.putExtra("user", post.getUser());
                context.startActivity(intent);
            }
        });

        rloComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommentActivity.class);
                intent.putExtra("postid", post.getId());
                context.startActivity(intent);

            }
        });
        tvComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommentActivity.class);
                intent.putExtra("postid", post.getId());
                context.startActivity(intent);

            }
        });
    }

}

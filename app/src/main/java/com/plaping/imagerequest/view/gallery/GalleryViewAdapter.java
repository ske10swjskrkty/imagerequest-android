package com.plaping.imagerequest.view.gallery;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.api.model.custom.Photo;
import com.plaping.imagerequest.view.general.ImageViewListItem;

import java.util.ArrayList;
import java.util.List;


public class GalleryViewAdapter extends RecyclerView.Adapter<GalleryViewAdapter.ImageViewHolder> {

    private List<Photo> allPhotoList;

    private List<Photo> showPhotoList = new ArrayList<Photo>();
    private Context context;

    private ItemClickListener itemClickListener;
    private boolean isHorizontal = false;

    public GalleryViewAdapter(Context context, List<Photo> allPhotoList, boolean isHorizontal) {
        this.allPhotoList = allPhotoList;
        this.context = context;
        this.isHorizontal = isHorizontal;
        this.showPhotoList.addAll(allPhotoList);
    }

    public GalleryViewAdapter(Context context, List<Photo> allPhotoList) {
         this(context,allPhotoList,false);
    }


    public void setOnItemClickListener(ItemClickListener listener) {
        this.itemClickListener = listener;
    }

    public void setAlbumName(String albumName) {

        this.showPhotoList.clear();

        if (albumName.equals("Gallery")) {

            showPhotoList.addAll(allPhotoList);

        } else {

            for (Photo photo : allPhotoList) {

                if (photo.getAlbums().equals(albumName)) {
                    showPhotoList.add(photo);
                }
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ImageViewListItem item = new ImageViewListItem(context,isHorizontal);
        ImageViewHolder holder = new ImageViewHolder(context, item);

        return holder;
    }

    public List<Photo> getShowPhotoList() {
        return showPhotoList;
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder holder, final int position) {
        holder.setImagePath(showPhotoList.get(position).getPath());

        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(context, "Galler Holder Clicked!", Toast.LENGTH_SHORT).show();
                if (itemClickListener != null)
                    itemClickListener.onClick(v, showPhotoList.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return showPhotoList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        private ImageViewListItem item;

        public ImageViewHolder(Context context, ImageViewListItem itemView) {
            super(itemView);
            this.item = itemView;
            this.context = context;
        }

        public void setImagePath(String imagePath) {
            this.item.fill(imagePath);
        }

        public void setOnClickListener(View.OnClickListener listener) {

            this.item.setOnClickListener(listener);

        }

    }
}

package com.plaping.imagerequest.view.profile;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.BaseActivity;

/**
 * Created by deknaew_bws on 4/9/16.
 */
public class PostRequestPagerView extends RelativeLayout {

    private Context context;
    private ViewPager vpPostRequest;


    public PostRequestPagerView(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public PostRequestPagerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public PostRequestPagerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    public void setupView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BaseActivity baseActivity = (BaseActivity) context;


        View view = inflater.inflate(R.layout.view_post_request_pager, this, true);
        vpPostRequest = (ViewPager) findViewById(R.id.vp_post_request);
        vpPostRequest.setAdapter(new PostRequestPagerViewAdapter(baseActivity.getSupportFragmentManager()));


    }


}

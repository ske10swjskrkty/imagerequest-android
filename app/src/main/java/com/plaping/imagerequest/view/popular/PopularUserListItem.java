package com.plaping.imagerequest.view.popular;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.plaping.imagerequest.R;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by deknaew_bws on 3/31/16.
 */
public class PopularUserListItem extends RelativeLayout {

    private Context context;
    private ViewPager vpPopular;

    private CirclePageIndicator indicator;

    public PopularUserListItem(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public PopularUserListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public PopularUserListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    private void setupView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_list_popular_user, this);
        vpPopular = (ViewPager) view.findViewById(R.id.vp_popular);
        PopularUserListItemAdapter adapter = new PopularUserListItemAdapter(context);
        indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);

        vpPopular.setAdapter(adapter);
        adapter.load(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                return false;
            }
        });

        indicator.setStrokeWidth(0);
        indicator.setPageColor(Color.parseColor("#f0f0f0"));
        indicator.setViewPager(vpPopular);


    }
}

package com.plaping.imagerequest.view.filter;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.plaping.imagerequest.R;
import com.plaping.imagerequest.view.general.WidthSquareImageView;

import java.util.ArrayList;
import java.util.List;

public class FrameItemView extends RelativeLayout {

    public interface OnFrameItemSelectListener {
        public void onFrameItemSelect(FrameItemView sender);
    }

    private static List<FrameItemView> itemList = new ArrayList<FrameItemView>();

    private View vwBorder;
    private RelativeLayout rloBackground;
    private WidthSquareImageView imvPreview;
    private TextView tvwName;
    private OnFrameItemSelectListener listener;

    private int imvFrameImageSrc;
    private String tvwNameText;

    public FrameItemView(Context context) {
        super(context);
        setupView();

    }

    public FrameItemView(Context context, AttributeSet attr) {
        super(context, attr);
        setupAttributes(attr);
        setupView();
    }

    public FrameItemView(Context context, AttributeSet attr, int defStyleRef) {
        super(context, attr, defStyleRef);
        setupAttributes(attr);
        setupView();
    }

    public void setOnFilterItemSelectListener(OnFrameItemSelectListener listener) {
        this.listener = listener;
    }

    protected void setupAttributes(AttributeSet attr) {
        itemList.add(this);

        TypedArray typedArr = getContext().getTheme().obtainStyledAttributes(
                attr,
                R.styleable.FrameItemButton,
                0, 0);

        try {
            imvFrameImageSrc = typedArr.getResourceId(R.styleable.FrameItemButton_frame_image_src, -1);
            tvwNameText = typedArr.getString(R.styleable.FrameItemButton_frame_name_text);
        } finally {
            typedArr.recycle();
        }
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);

        vwBorder.setSelected(selected);
        tvwName.setSelected(selected);
    }

    protected void setupView() {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_frame_item, this);

        vwBorder = (View) findViewById(R.id.vwBorder);

        tvwName = (TextView) findViewById(R.id.tvwName);
        tvwName.setText(tvwNameText == null ? "Frame" : tvwNameText);

        imvPreview = (WidthSquareImageView) findViewById(R.id.imvPreview);
        if (imvFrameImageSrc != -1) {
            imvPreview.setImageResource(imvFrameImageSrc);
        }

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                for (FrameItemView item : itemList) {
                    if (item == FrameItemView.this) continue;
                    item.setSelected(false);
                }
                setSelected(true);

                if (listener != null) {
                    listener.onFrameItemSelect(FrameItemView.this);
                }
            }
        });
    }
}

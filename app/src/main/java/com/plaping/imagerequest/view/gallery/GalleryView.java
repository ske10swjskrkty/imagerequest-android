package com.plaping.imagerequest.view.gallery;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;


import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.Photo;
import com.plaping.imagerequest.utils.GalleryUtils;
import com.plaping.imagerequest.view.general.OutBoundCancelRecyclerView;

import java.util.List;

/**
 * Created by deknaew_bws on 10/27/15.
 */
public class GalleryView extends RelativeLayout {

    private Context context;
    private OutBoundCancelRecyclerView rcGallery;
    private GalleryViewAdapter adapter;
    private RelativeLayout rloClick;

    public GalleryView(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.adapter.setOnItemClickListener(listener);
    }

    public GalleryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    private void setupView() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<Photo> photos = GalleryUtils.getImagesPath((Activity) context);
        adapter = new GalleryViewAdapter(context, photos);
        View view = inflater.inflate(R.layout.view_list_gallery, this, true);
        rloClick = (RelativeLayout) view.findViewById(R.id.rlo_click);
        rcGallery = (OutBoundCancelRecyclerView) view.findViewById(R.id.rc_gallery);
        rcGallery.setLayoutManager(new GridLayoutManager(context, 4));
        rcGallery.setHasFixedSize(true);
        rcGallery.setAdapter(adapter);
        rcGallery.setVerticalFadingEdgeEnabled(false);
        rcGallery.setOverScrollMode(OVER_SCROLL_NEVER);



    }
}

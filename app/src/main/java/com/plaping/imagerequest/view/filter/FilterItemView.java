package com.plaping.imagerequest.view.filter;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plaping.imagerequest.R;

import java.util.ArrayList;
import java.util.List;

public class FilterItemView extends RelativeLayout {

    public interface OnFilterItemSelectListener {
        public void onFilterItemSelect(FilterItemView sender);
    }

    private static List<FilterItemView> itemList = new ArrayList<FilterItemView>();

    private View btnSelect;
    private RelativeLayout rloBackground;
    private ImageView imvPreview;
    private TextView tvwName;
    private OnFilterItemSelectListener listener;

    private int imvFilterImageSrc;

    public FilterItemView(Context context) {
        super(context);
        setupView();

    }

    public FilterItemView(Context context, AttributeSet attr) {
        super(context, attr);
        setupAttributes(attr);
        setupView();
    }

    public FilterItemView(Context context, AttributeSet attr, int defStyleRef) {
        super(context, attr, defStyleRef);
        setupAttributes(attr);
        setupView();
    }

    public void setOnFilterItemSelectListener(OnFilterItemSelectListener listener) {
        this.listener = listener;
    }

    protected void setupAttributes(AttributeSet attr) {
        itemList.add(this);

        TypedArray typedArr = getContext().getTheme().obtainStyledAttributes(
                attr,
                R.styleable.FilterItemButton,
                0, 0);

        try {
            imvFilterImageSrc = typedArr.getResourceId(R.styleable.FilterItemButton_filter_image_src, -1);
        } finally {
            typedArr.recycle();
        }
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);

        tvwName.setSelected(selected);
        rloBackground.setSelected(selected);
        btnSelect.setSelected(selected);
    }

    protected void setupView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_filter_item, this);

        tvwName = (TextView) findViewById(R.id.tvwName);
        rloBackground = (RelativeLayout) findViewById(R.id.rloBackground);

        imvPreview = (ImageView) findViewById(R.id.imvPreview);
        imvPreview.setImageResource(imvFilterImageSrc);

        btnSelect = (View) findViewById(R.id.btnSelect);
        btnSelect.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                for (FilterItemView item : itemList) {
                    if (item == FilterItemView.this) continue;
                    item.setSelected(false);
                }
                setSelected(true);

                if (listener != null) {
                    listener.onFilterItemSelect(FilterItemView.this);
                }
            }
        });
    }

    public void setName(String name) {
        tvwName.setText(name);
    }
}

package com.plaping.imagerequest.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.GifActivity;
import com.plaping.imagerequest.activities.PhotoActivity;
import com.plaping.imagerequest.activities.PostActivity;
import com.plaping.imagerequest.activities.QuoteActivity;
import com.plaping.imagerequest.activities.RequestCommentActivity;
import com.plaping.imagerequest.activities.UserActivity;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Request;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.utils.UrlUtils;
import com.plaping.imagerequest.view.general.TagListItem;

public class RequestListItem extends RelativeLayout {

    private Context context;

    private ImageView imvUserFrom;
    private ImageView imvUserTo;
    private TextView tvUserFrom;
    private TextView tvUserTo;
    private TextView tvMessage;
    private AlertDialog alertDialog;
    private RelativeLayout rloSeePost;

    private RelativeLayout rloComment;

    private RelativeLayout rloLike;
    private LinearLayout lloTags;

    private TextView tvLike;
    private TextView tvCommentCount;
    private TextView tvLikeCount;
    private ImageView imvReference;
    private ImageView imvCheck;
    private RelativeLayout rloApply;

    private ImageRequestApplication app;
    private RelativeLayout rloTags;


    public RequestListItem(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public RequestListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public RequestListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    private void setupView() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        app = (ImageRequestApplication) context.getApplicationContext();
        View view = inflater.inflate(R.layout.view_list_request, this, true);
        imvUserFrom = (ImageView) findViewById(R.id.imv_user_from);
        imvUserTo = (ImageView) findViewById(R.id.imv_user_to);
        tvUserFrom = (TextView) findViewById(R.id.tv_user_from);
        tvUserTo = (TextView) findViewById(R.id.tv_user_to);
        imvReference = (ImageView) findViewById(R.id.imv_reference);
        tvMessage = (TextView) findViewById(R.id.tv_message);
        imvReference = (ImageView) findViewById(R.id.imv_reference);
        rloComment = (RelativeLayout) findViewById(R.id.rlo_comment);
        rloLike = (RelativeLayout) findViewById(R.id.rlo_like);
        tvLike = (TextView) findViewById(R.id.tv_like);
        tvLikeCount = (TextView) findViewById(R.id.tv_like_count);
        tvCommentCount = (TextView) findViewById(R.id.tv_comment_count);
        imvCheck = (ImageView) findViewById(R.id.imv_check);
        rloApply = (RelativeLayout) findViewById(R.id.rlo_apply);
        lloTags = (LinearLayout) findViewById(R.id.llo_tags);
        rloTags = (RelativeLayout) findViewById(R.id.rlo_tag);
        rloSeePost = (RelativeLayout) findViewById(R.id.rlo_see_post);

    }

    public void fill(final Request request) {
        if (request == null) return;
        setIsLiked(request.isLiked());
        User userFrom = request.getFromUser();
        User userTo = request.getToUser();

        tvUserTo.setText(userTo.getUsername());
        tvUserFrom.setText(userFrom.getUsername());

        Glide.with(context)
                .load(UrlUtils.BASE_IMG_URL + "" + userFrom.getImgUrl())
                .into(imvUserFrom);

        imvUserFrom.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserActivity.class);
                intent.putExtra("user", request.getFromUser());
                context.startActivity(intent);

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose type")
                .setItems(R.array.image_type, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {

                            Intent intent = new Intent(context, GifActivity.class);
                            intent.putExtra("request", request);
                            context.startActivity(intent);

                        } else if (which == 1) {
                            Intent intent = new Intent(context, PhotoActivity.class);
                            intent.putExtra("request", request);
                            context.startActivity(intent);


                        } else if (which == 1) {
                            Intent intent = new Intent(context, QuoteActivity.class);
                            intent.putExtra("request", request);
                            context.startActivity(intent);


                        }
                    }
                });
        alertDialog = builder.create();

        if (request.isOpen()) {
            imvCheck.setVisibility(GONE);
        } else {
            imvCheck.setVisibility(VISIBLE);
        }

        if (request.getToUser().getUsername().equals(app.getUserManager().getUser().getUsername())) {
            if (request.isOpen()) {
                rloApply.setVisibility(VISIBLE);
                rloSeePost.setVisibility(GONE);
            } else {
                rloApply.setVisibility(GONE);
                rloSeePost.setVisibility(VISIBLE);
                rloSeePost.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Log.d("PostId", "id : " + request.getReferPostId());
                        Intent intent = new Intent(context, PostActivity.class);
                        intent.putExtra("postid", request.getReferPostId());
                        context.startActivity(intent);
                    }
                });

            }
        } else {
            rloApply.setVisibility(GONE);
            if (!request.isOpen()){
                rloSeePost.setVisibility(VISIBLE);
                rloSeePost.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Log.d("PostId", "id : " + request.getReferPostId());
                        Intent intent = new Intent(context, PostActivity.class);
                        intent.putExtra("postid", request.getReferPostId());
                        context.startActivity(intent);
                    }
                });
            }
            else rloSeePost.setVisibility(GONE);


        }


        rloApply.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.show();
            }
        });


        Glide.with(context)
                .load(UrlUtils.BASE_IMG_URL + "" + userTo.getImgUrl())
                .into(imvUserTo);

        imvUserTo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserActivity.class);
                intent.putExtra("user", request.getToUser());
                context.startActivity(intent);
            }
        });

        lloTags.removeAllViews();
        if (request.getTags() != null) {

            rloTags.setVisibility(VISIBLE);


            for (String tag : request.getTags()) {
                TagListItem item = new TagListItem(context);
                item.setTagText(tag);
                lloTags.addView(item);

            }
        } else {
            rloTags.setVisibility(GONE);

        }

        rloLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        tvMessage.setText(request.getMessage());
        tvLikeCount.setText(request.getLikeCount() + " Like" + (request.getLikeCount() > 1 ? "s" : ""));
        tvCommentCount.setText(request.getCommentCount() + " Comment" + (request.getCommentCount() > 1 ? "s" : ""));
        rloLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLiked) {
                    unLikeRequest(request);
                } else {
                    likeRequest(request);
                }
            }
        });

        if (request.getImgUrl() != null) {
            imvReference.setVisibility(VISIBLE);
            Glide.with(context)
                    .load(UrlUtils.BASE_IMG_URL + request.getImgUrl())
                    .into(imvReference);

        } else {
            imvReference.setVisibility(GONE);
        }

        rloComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RequestCommentActivity.class);
                intent.putExtra("requestid", request.getId());
                context.startActivity(intent);
            }
        });

    }

    private void likeRequest(Request request) {
        app.getHttpService().likeRequest(app.getAccessToken(), request.getId(), new ImageRequestHttpService.OnResponseCallback<String>() {
            @Override
            public void onResponse(boolean success, Throwable error, String data) {

                if (success) {
                    setIsLiked(true);
                }
            }
        });


    }

    private void unLikeRequest(Request request) {
        app.getHttpService().unLikeRequest(app.getAccessToken(), request.getId(), new ImageRequestHttpService.OnResponseCallback<String>() {
            @Override
            public void onResponse(boolean success, Throwable error, String data) {

                if (success) {
                    setIsLiked(false);
                }
            }
        });
    }

    private boolean isLiked = false;

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
        if (isLiked) {
            tvLike.setText("❤ Liked");
            tvLike.setTextColor(Color.parseColor("#e74c3c"));
        } else {
            tvLike.setText("Like");
            tvLike.setTextColor(Color.parseColor("#8C8C8C"));

        }
    }
}

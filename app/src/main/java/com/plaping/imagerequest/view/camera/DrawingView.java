package com.plaping.imagerequest.view.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.CountDownTimer;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.BaseActivity;


enum FocusType {
    NORMAL, FAILED, COMPLETE
}

public class DrawingView extends View {

    boolean haveFace;
    private Paint drawingPaint;
    private Bitmap normalIndicator;
    private Bitmap failedIndicator;
    private Bitmap completeIndicator;
    private Bitmap bitmap;

    private boolean haveTouch;
    private Rect touchArea;
    private FocusType type = FocusType.NORMAL;

    private CountDownTimer timer;
    private BaseActivity activity;


    public DrawingView(Context context) {
        super(context);
        haveFace = false;
        drawingPaint = new Paint();
        drawingPaint.setColor(Color.GREEN);
        drawingPaint.setStyle(Paint.Style.STROKE);
        drawingPaint.setStrokeWidth(2);
        activity = (BaseActivity) context;

        normalIndicator = BitmapFactory.decodeResource(getResources(), R.drawable.focus_normal);
        failedIndicator = BitmapFactory.decodeResource(getResources(), R.drawable.focus_failed);
        completeIndicator = BitmapFactory.decodeResource(getResources(), R.drawable.focus_completed);

        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x / 5;

        normalIndicator = normalIndicator.createScaledBitmap(normalIndicator, width, width, false);
        failedIndicator = failedIndicator.createScaledBitmap(failedIndicator, width, width, false);
        completeIndicator = completeIndicator.createScaledBitmap(completeIndicator, width, width, false);

        this.setAlpha(0.5f);


        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer = new CountDownTimer(1500, 100) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        //setAlpha(Math.max(0, getAlpha() - 0.1f));
                    }

                    @Override
                    public void onFinish() {
                        setAlpha(0);
                    }
                };
            }
        });


        bitmap = normalIndicator;


        haveTouch = false;
    }


    public void setHaveTouch(boolean t, Rect tArea, FocusType type) {
        haveTouch = t;
        touchArea = tArea;
        this.type = type;

        if (type == FocusType.COMPLETE) {
            bitmap = completeIndicator;
            timer.start();
        } else if (type == FocusType.FAILED) {
            bitmap = failedIndicator;
            timer.start();
        } else {
            bitmap = normalIndicator;
            setAlpha(1f);
            timer.cancel();

        }

        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {


        if (haveTouch) {
            canvas.drawBitmap(bitmap, touchArea.left - bitmap.getWidth() / 2, touchArea.top - bitmap.getHeight() / 2, drawingPaint);

        } else {
            canvas.drawColor(Color.TRANSPARENT);
        }
    }

    private boolean isDraw = true;

    public void setEnableDrawIndicator(boolean isDraw) {
        this.isDraw = isDraw;

    }

}

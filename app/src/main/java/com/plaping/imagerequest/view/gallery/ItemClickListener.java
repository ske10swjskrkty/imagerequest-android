package com.plaping.imagerequest.view.gallery;

import android.view.View;

/**
 * Created by deknaew_bws on 11/15/15.
 */
public interface ItemClickListener {

    void onClick(View view, Object obj, int position);
}

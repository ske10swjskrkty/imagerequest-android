package com.plaping.imagerequest.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.BaseActivity;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Post;
import com.plaping.imagerequest.async.DownloadTask;
import com.plaping.imagerequest.utils.UrlUtils;

import java.io.File;

/**
 * Created by deknaew_bws on 4/18/16.
 */
public class ShareDialog extends Dialog {

    private ImageRequestApplication app;
    private TextView tvSecond;
    private ImageView imvAdsBanner;
    private RelativeLayout rloShare;
    private Context context;
    private DownloadTask downloadTask;

    private CountDownTimer countDownTimer;

    public ShareDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        downloadTask = new DownloadTask(context);

        setContentView(R.layout.view_dialog_share);
        app = (ImageRequestApplication) context.getApplicationContext();
        tvSecond = (TextView) findViewById(R.id.tv_second);
        imvAdsBanner = (ImageView) findViewById(R.id.imv_ads_banner);
        rloShare = (RelativeLayout) findViewById(R.id.rlo_share);
        rloShare.setVisibility(View.GONE);
        this.context = context;

        countDownTimer = new CountDownTimer(6000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                tvSecond.setText((millisUntilFinished / 1000) + "");


            }

            @Override
            public void onFinish() {
                tvSecond.setText("0");
                rloShare.setVisibility(View.VISIBLE);

            }
        };

    }


    public void show(final Post post) {
        super.show();
        countDownTimer.start();

        rloShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (post == null) {
                    Toast.makeText(context, "No Post Found!", Toast.LENGTH_SHORT).show();
                    return;
                }

//                downloadTask.execute(UrlUtils.BASE_IMG_URL + post.getImgUrl());
//                downloadTask.setDownloadCompleteListener(new DownloadTask.DownloadCompleteListener() {
//                    @Override
//                    public void onDownloadComplete(File file) {
//                        shareImage(file);
//                    }
//                });


                callShareIntent(UrlUtils.BASE_IMG_URL + post.getImgUrl());


                ((BaseActivity) context).showProgressDialog();
                app.getHttpService().sharePost(app.getAccessToken(), post.getId(), new ImageRequestHttpService.OnResponseCallback<String>() {
                    @Override
                    public void onResponse(boolean success, Throwable error, String data) {
                        ((BaseActivity) context).hideProgressDialog();

                        if (success) {
                            callShareIntent(UrlUtils.BASE_IMG_URL + post.getImgUrl());
                        } else {
                            Toast.makeText(context, "Share failed, Please try again later.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

    }


    private void callShareIntent(String url) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Share From ImageRequest");
        share.putExtra(Intent.EXTRA_TEXT, url);

        context.startActivity(Intent.createChooser(share, "Share Image!"));
        this.dismiss();
    }


    private void shareImage(File file) {


        Intent share = new Intent(Intent.ACTION_SEND);

        // If you want to share a png image only, you can do:
        // setType("image/png"); OR for jpeg: setType("image/jpeg");
        share.setType("image/*");

        // Make sure you put example png image named myImage.png in your
        // directory


        Uri uri = Uri.fromFile(file);
        share.putExtra(Intent.EXTRA_STREAM, uri);

        context.startActivity(Intent.createChooser(share, "Share Image!"));
    }

    @Override
    public void dismiss() {
        super.dismiss();
        countDownTimer.cancel();
    }
}

package com.plaping.imagerequest.view.general;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.SearchActivity;

/**
 * Created by deknaew_bws on 4/11/16.
 */
public class TagListItem extends RelativeLayout {

    private Context context;

    private TextView tvWord;
    private String tagText;

    public TagListItem(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public TagListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public TagListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    private void setupView() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_list_tag, this, true);
        tvWord = (TextView) findViewById(R.id.tv_tag);

    }

    public void setTagText(final String tagText) {
        tvWord.setText(tagText);

        tvWord.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SearchActivity.class);
                intent.putExtra("tag",tagText);
                context.startActivity(intent);
            }
        });
    }
}

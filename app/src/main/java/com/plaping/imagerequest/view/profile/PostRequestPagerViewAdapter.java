package com.plaping.imagerequest.view.profile;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.plaping.imagerequest.fragment.BaseFragment;
import com.plaping.imagerequest.fragment.user.PostFragment;
import com.plaping.imagerequest.fragment.user.RequestFragment;

/**
 * Created by deknaew_bws on 4/9/16.
 */
public class PostRequestPagerViewAdapter extends FragmentPagerAdapter {


    private BaseFragment[] fragments;

    public PostRequestPagerViewAdapter(FragmentManager fm) {
        super(fm);
        fragments = new BaseFragment[]{
                PostFragment.newInstance(),
                RequestFragment.newInstance()
        };
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return 2;
    }
}

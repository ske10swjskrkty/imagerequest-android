package com.plaping.imagerequest.view.popular;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.activities.UserActivity;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.utils.UrlUtils;

/**
 * Created by deknaew_bws on 3/31/16.
 */
public class PopularUserItem extends RelativeLayout {

    private Context context;
    private ImageView imvProfile;
    private TextView tvUsername;
    private TextView tvShareCount;

    public PopularUserItem(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public PopularUserItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public PopularUserItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    private void setupView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_popular_user, this);
        imvProfile = (ImageView) findViewById(R.id.imv_profile);
        tvUsername = (TextView) findViewById(R.id.edt_username);
        tvShareCount = (TextView) findViewById(R.id.tv_share_count);

    }

    public void fill(final User user) {

        tvUsername.setText(user.getUsername());
        tvShareCount.setText(user.getTotalShareCount() + " Total Shares");
        Glide.with(context)
                .load(UrlUtils.BASE_IMG_URL + user.getImgUrl())
                .into(imvProfile);

        OnClickListener listener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserActivity.class);
                intent.putExtra("user", user);
                context.startActivity(intent);
            }
        };

        imvProfile.setOnClickListener(listener);

    }
}

package com.plaping.imagerequest.view.textdecorate;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.FontStyle;


/**
 * Created by Bestiiz on 21/3/2559.
 */
public class FontListItem extends RelativeLayout {
    private Context context;
    private TextView tvNameFont;

    public FontListItem(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public FontListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public FontListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    private void setupView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_list_font, this, true);
        tvNameFont = (TextView) view.findViewById(R.id.tv_name_font);


    }

    public void fill(FontStyle fontStyle) {
        tvNameFont.setText(fontStyle.getNameFont());
        tvNameFont.setTypeface(fontStyle.getTypeface());


    }
}

package com.plaping.imagerequest.view.camera;

import android.content.Context;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class CameraView extends ViewGroup implements SurfaceHolder.Callback {

    private static final String TAG = "petpolar";

    private SurfaceView surfaceView;
    private SurfaceHolder holder;

    private Camera camera;
    private List<Camera.Size> sizes = new ArrayList<Camera.Size>();
    private Camera.Size previewSize;

    private boolean created;

    private int format;
    private int width;
    private int height;
    private DrawingView drawingView;
    private Rect focusRect;
    private Context context;

    public CameraView(Context context, SurfaceView surfaceView, DrawingView drawingView) {
        super(context);
        this.surfaceView = surfaceView;
        this.drawingView = drawingView;
        this.context = context;

        addView(surfaceView);

        surfaceView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if (camera != null) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        float x = event.getX();
                        float y = event.getY();
                        float touchMajor = event.getTouchMajor();
                        float touchMinor = event.getTouchMinor();

                        Rect touchRect = new Rect(
                                (int) (x - touchMajor / 2),
                                (int) (y - touchMinor / 2),
                                (int) (x + touchMajor / 2),
                                (int) (y + touchMinor / 2));

                        touchFocus(touchRect);
                    }
                }
                return true;
            }
        });

        holder = surfaceView.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        Log.d("Arm", "Camera View Size >> width : " + this.getWidth() + " || height :" + this.getHeight());
        Log.d("Arm", "Drawing View Size >> width : " + drawingView.getWidth() + " || height :" + drawingView.getHeight());
    }

    public void touchFocus(final Rect tfocusRect) {
        this.focusRect = tfocusRect;
//        camera.stopFaceDetection();

        //Convert from View's width and height to +/- 1000
        final Rect targetFocusRect = new Rect(
                tfocusRect.left * 2000 / this.getWidth() - 1000,
                tfocusRect.top * 2000 / this.getHeight() - 1000,
                tfocusRect.right * 2000 / this.getWidth() - 1000,
                tfocusRect.bottom * 2000 / this.getHeight() - 1000);

        final List<Camera.Area> focusList = new ArrayList<Camera.Area>();
        Camera.Area focusArea = new Camera.Area(targetFocusRect, 1000);
        focusList.add(focusArea);

        Camera.Parameters para = camera.getParameters();
        para.setFocusAreas(focusList);
        para.setMeteringAreas(focusList);
        camera.setParameters(para);


        try {
            camera.autoFocus(myAutoFocusCallback);

        } catch (Exception e) {
            e.printStackTrace();
        }


        drawingView.setHaveTouch(true, tfocusRect, FocusType.NORMAL);
        drawingView.invalidate();
    }

    Camera.AutoFocusCallback myAutoFocusCallback = new Camera.AutoFocusCallback() {

        @Override
        public void onAutoFocus(boolean success, Camera camera) {

            if (success) {
                camera.cancelAutoFocus();
                drawingView.setHaveTouch(true, focusRect, FocusType.COMPLETE);
            } else {
                drawingView.setHaveTouch(true, focusRect, FocusType.FAILED);
            }

            float focusDistances[] = new float[3];
            camera.getParameters().getFocusDistances(focusDistances);

        }
    };

    private int clamp(int x, int min, int max) {
        if (x > max) {
            return max;
        }
        if (x < min) {
            return min;
        }
        return x;
    }

    public void setCamera(Camera camera) {
        if (camera == null) return;

        if (this.camera == camera)
            return;

        this.camera = camera;
        camera.setDisplayOrientation(90);

        sizes = camera.getParameters().getSupportedPreviewSizes();
        for (Camera.Size size : sizes) {
            Log.d("POND2", "@@@@@" + size.width + "," + size.height);
        }


        if (created) {

            Log.d("CameraView", "CameraView when created");
            surfaceCreated(holder);
            invalidate();
            surfaceChanged(holder, this.format, this.width, this.height);
        }

        ((BaseActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Log.d("CameraView", "CameraView requested Layout");
                requestLayout();
            }
        });

    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (camera != null) {
                camera.setPreviewDisplay(holder);
                created = true;
            }
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (camera != null) {
            try {
                camera.stopPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (camera == null || previewSize == null) return;
        this.format = format;
        this.width = width;
        this.height = height;

        try {
            camera.stopPreview();

            Log.d("POND2", "@@@@@" + previewSize.width + "," + previewSize.height);

            Camera.Parameters parameters = camera.getParameters();
            parameters.setPreviewSize(previewSize.width, previewSize.height);
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

            requestLayout();

            try {
                camera.setParameters(parameters);
                Log.d("Arm", "CameraView : Set parameters finish");
            } catch (Exception e) {
                Log.d("Arm", "CameraView : " + e.toString());
                e.printStackTrace();
            }
            camera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);

        if (sizes != null) {
            previewSize = getOptimalPreviewSize(sizes, width, height);

            if (previewSize != null) {
                int sizeMax = Math.max(previewSize.width, previewSize.height);
                int sizeMin = Math.min(previewSize.width, previewSize.height);
                setMeasuredDimension(width, width * sizeMax / sizeMin);
            } else {
                setMeasuredDimension(width, height);
            }

        } else {
            setMeasuredDimension(width, height);
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            float x = event.getX();
            float y = event.getY();
            float touchMajor = event.getTouchMajor();
            float touchMinor = event.getTouchMinor();

            Rect touchRect = new Rect(
                    (int) (x - touchMajor / 2),
                    (int) (y - touchMinor / 2),
                    (int) (x + touchMajor / 2),
                    (int) (y + touchMinor / 2));


        }
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        if (changed && getChildCount() > 0) {
            final View child = getChildAt(0);

            final int width = r - l;
            final int height = b - t;

            int previewWidth = width;
            int previewHeight = height;
            if (previewSize != null) {
                previewWidth = previewSize.height;
                previewHeight = previewSize.width;
            }

            final int scaledChildHeight = previewHeight * width / previewWidth;
            child.layout(0, 0, width, scaledChildHeight);
        }
    }


}
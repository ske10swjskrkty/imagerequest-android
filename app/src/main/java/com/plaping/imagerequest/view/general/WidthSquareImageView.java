package com.plaping.imagerequest.view.general;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class WidthSquareImageView extends ImageView {

    public WidthSquareImageView(Context context) {
        super(context);
    }

    public WidthSquareImageView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        if (getLayoutParams() != null && w != h) {
            getLayoutParams().height = w;
            setLayoutParams(getLayoutParams());
        }
    }
}

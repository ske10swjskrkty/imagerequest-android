package com.plaping.imagerequest.view.sticker;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.plaping.imagerequest.utils.AutoResizeTextView;


/**
 * Created by cheungchingai on 6/15/15.
 */
public class StickerTextView extends StickerView {
    private Context context;
    private AutoResizeTextView tv_main;
    private boolean stroke = false;
    private float strokeWidth = 0.0f;
    private int strokeColor;



    public StickerTextView(Context context) {
        super(context);
        this.context = context;
    }

    public StickerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public StickerTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    @Override
    public View getMainView() {
        if (tv_main != null)
            return tv_main;

        tv_main = new AutoResizeTextView(getContext());
        //tv_main.setTextSize(22);
        //tv_main.setTextColor(Color.WHITE);
        tv_main.setGravity(Gravity.CENTER);
        tv_main.setTextSize(TypedValue.COMPLEX_UNIT_PX, 110);
        Log.d("sizetext", tv_main.getTextSize() + "");
        tv_main.setMinTextSize(20);
        //tv_main.setShadowLayer(10, 0, 0, Color.BLACK);
        tv_main.setMaxLines(1);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        params.gravity = Gravity.CENTER;
        tv_main.setLayoutParams(params);
        if (getImageViewFlip() != null)
            getImageViewFlip().setVisibility(View.GONE);
        return tv_main;
    }

    public void setText(String text) {
        if (tv_main != null) {
            tv_main.setText(text);
            getMainView().measure(0, 0);
//            tv_main.measure(0, 0);
//            Log.d("textviewsizeee", getMainView().getWidth() + " " + getMainView().getHeight());
//            Log.d("textviewsizeee" ,getMainView().getLayoutParams().width+" "+getMainView().getLayoutParams().height);
//            Log.d("textviewsizeee", tv_main.getLayoutParams().width + " " + tv_main.getLayoutParams().height);
//            Log.d("textviewsizeee", getMainView().getMeasuredWidth()+ " " + getMainView().getMeasuredHeight());
//            Log.d("textviewsizeee", tv_main.getMeasuredWidth()+ " " + tv_main.getMeasuredHeight());

            setSize(getMainView().getMeasuredWidth(), getMainView().getMeasuredHeight());
        }
    }

    public String getText() {
        if (tv_main != null)
            return tv_main.getText().toString();

        return null;
    }

    public static float pixelsToSp(Context context, float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px / scaledDensity;
    }

    @Override
    protected void onScaling(boolean scaleUp) {
        super.onScaling(scaleUp);
    }

    public void setTextColor(int color) {

        tv_main.setTextColor(color);
        Log.d("textviewsizeee", getMainView().getWidth() + " " + getMainView().getHeight());
        Log.d("textviewsizeee", getMainView().getLayoutParams().width + " " + getMainView().getLayoutParams().height);
        Log.d("textviewsizeee", tv_main.getLayoutParams().width + " " + tv_main.getLayoutParams().height);
        invalidate();

    }

    public void setStroke(boolean stroke) {
        this.stroke = stroke;
        tv_main.setStroke(stroke);
        invalidate();
    }

    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        tv_main.setStrokeWidth(strokeWidth);
        invalidate();
    }

    public void setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
        tv_main.setStrokeColor(strokeColor);

    }

    public boolean isStroke() {
        return stroke;
    }

    public float getStrokeWidth() {
        Log.d("StrokeWidth" , ""+(int)(tv_main.getStrokeWidth()/2));
        return (int)(tv_main.getStrokeWidth());
    }

    public int getStrokeColor() {
        return strokeColor;
    }

    public int getTextColor() {

        return tv_main.getTextColors().getDefaultColor();

    }

    public void setOpacity(int opacity) {
        tv_main.setAlpha(opacity / (float) 100);

    }

    public void setFont(Typeface typeface) {
        tv_main.setTypeface(typeface);
        //getMainView().measure(0, 0);
       // setSize(getMainView().getMeasuredWidth(), getMainView().getMeasuredHeight());
        //invalidate();

    }


}
package com.plaping.imagerequest.view;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.plaping.imagerequest.R;


/**
 * Created by deknaew_bws on 1/16/16.
 */
public class AlertDialog extends Dialog {

    private RelativeLayout rloCancel;
    private RelativeLayout rloOK;
    private TextView tvAlertHeader;
    private TextView tvAlertText;

    private String alertText;
    private String alertHeader;
    private View.OnClickListener okListener;
    private View.OnClickListener cancelListener;


    public AlertDialog(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.view_dialog_alert);
        rloCancel = (RelativeLayout) findViewById(R.id.rlo_cancel);
        rloOK = (RelativeLayout) findViewById(R.id.rlo_ok);
        tvAlertHeader = (TextView) findViewById(R.id.tv_alert_header);
        tvAlertText = (TextView) findViewById(R.id.tv_alert_text);
    }

    public void setTvAlertHeader(String text) {
        this.alertHeader = text;
    }

    public void setTvAlertText(String text) {
        this.alertText = text;
    }

    public void setOkButtonListener(final View.OnClickListener listener) {
        okListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listener != null) listener.onClick(v);
                dismiss();
            }
        };
    }

    public void setCancelButtonListener(final View.OnClickListener listener) {
        cancelListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listener != null) listener.onClick(v);
                dismiss();
            }
        };
    }

    @Override
    public void show() {
        super.show();
        if (alertText != null) tvAlertText.setText(alertText);
        if (alertHeader != null) tvAlertHeader.setText(alertHeader);
        rloCancel.setOnClickListener(cancelListener);
        rloOK.setOnClickListener(okListener);
    }
}

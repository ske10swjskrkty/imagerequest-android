package com.plaping.imagerequest.view.popular;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.api.model.custom.UserResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by deknaew_bws on 3/31/16.
 */
public class PopularUserListItemAdapter extends PagerAdapter {

    private Context context;
    private ImageRequestApplication app;
    private List<User> userList = new ArrayList<>();

    public PopularUserListItemAdapter(Context context) {

        this.context = context;
        app = (ImageRequestApplication) context.getApplicationContext();

    }

    @Override
    public int getCount() {

        return userList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        PopularUserItem popularUserItem = new PopularUserItem(context);
        container.addView(popularUserItem);
        popularUserItem.fill(userList.get(position));
        return popularUserItem;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((PopularUserItem) object);
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (View) object;
    }

    public void load(final Handler.Callback callback) {
        app.getHttpService().getHotUser(new ImageRequestHttpService.OnResponseCallback<UserResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, UserResponse data) {
                callback.handleMessage(new Message());
                if (success) {
                    userList.clear();
                    userList.addAll(Arrays.asList(data.getUsers()));
                    notifyDataSetChanged();
                }
            }
        });
    }
}

package com.plaping.imagerequest.view.comment;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.Comment;
import com.plaping.imagerequest.utils.UrlUtils;

/**
 * Created by deknaew_bws on 4/11/16.
 */
public class CommentListItem extends RelativeLayout {

    private Context context;
    private ImageView imvProfile;
    private TextView tvUsername;
    private TextView tvMessage;
    private TextView tvTime;


    public CommentListItem(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public CommentListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public CommentListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    public void fill(Comment comment) {
        if (comment != null) {
            tvUsername.setText(comment.getUser().getUsername());
            tvMessage.setText(comment.getMessage());
            Glide.with(context)
                    .load(UrlUtils.BASE_IMG_URL + comment.getUser().getImgUrl())
                    .into(imvProfile);


            Log.d("CommentDateCreated","Date : " + comment.getDateCreated());


            tvTime.setText(DateUtils.getRelativeTimeSpanString(comment.getDateCreated(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        }
    }

    private void setupView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_list_comment, this, true);
        imvProfile = (ImageView) findViewById(R.id.imv_profile);
        tvMessage = (TextView) findViewById(R.id.tv_message);
        tvUsername = (TextView) findViewById(R.id.edt_username);
        tvTime = (TextView) findViewById(R.id.tv_time);


    }

}

package com.plaping.imagerequest.view.general;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class HeightSquareImageView extends ImageView {

    public HeightSquareImageView(Context context) {
        super(context);
    }

    public HeightSquareImageView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(
                measureWidth(heightMeasureSpec),
                measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int heightMeasureSpec)
    {
        return MeasureSpec.getSize(heightMeasureSpec);
    }

    private int measureHeight(int heightMeasureSpec)
    {
        return MeasureSpec.getSize(heightMeasureSpec);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        if (getLayoutParams() != null && w != h) {
            getLayoutParams().width = h;
            setLayoutParams(getLayoutParams());
        }
    }
}

package com.plaping.imagerequest.view.photo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.plaping.imagerequest.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class CropView extends RelativeLayout {

    private Context context;
    private PhotoView photoView;
    private PhotoViewAttacher photoViewAttacher;
    private RelativeLayout rloEmpty;


    public CropView(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public CropView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public CropView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }


    public void setupView() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_crop, this, true);
        photoView = (PhotoView) findViewById(R.id.pv_photo);
        rloEmpty = (RelativeLayout) findViewById(R.id.rlo_empty);

        photoView.setAllowParentInterceptOnEdge(false);

        photoView.setMaximumScale(10);


        photoView.destroyDrawingCache();


    }


    public void setEmpty(boolean isEmpty) {
        if (isEmpty) {
            rloEmpty.setVisibility(VISIBLE);
        } else {
            rloEmpty.setVisibility(GONE);
        }
    }

    public void setDisplayImage(Bitmap bitmap) {
        Drawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
        photoView.setImageDrawable(bitmapDrawable);

    }

    public void setDisplayImage(Uri uri) {
        this.photoView.setImageURI(uri);
    }

    public void setDisplayImage(String path) {
        Log.d("PhotoActivity", "Display Image Path : " + path);
        Picasso.with(context)
                .load(new File(path))
                .config(Bitmap.Config.RGB_565)
                .placeholder(R.color.black)
                .resize(1500, 1500)
                .centerInside()
                .into(photoView);
    }

    public Bitmap getCroppedImageBitmap() {

        Log.d("CropView", "Before Cropped");

        Bitmap b = photoView.getVisibleRectangleBitmap().copy(Bitmap.Config.RGB_565, false);

        photoView.destroyDrawingCache();


        Log.d("CropView", "Hash of bitmap : " + b.hashCode());

        Log.d("CropView", "After Cropped");


//        photoView.destroyDrawingCache();

        return b;

    }

    public String getCroppedImagePath() {
        File file = new File(Environment.getExternalStorageDirectory() + "/save" + System.currentTimeMillis() + ".jpg");
        try {
            FileOutputStream fos = new FileOutputStream(file);
            photoView.getVisibleRectangleBitmap().compress(Bitmap.CompressFormat.JPEG, 100, fos);
            return file.getAbsolutePath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        }
    }
}

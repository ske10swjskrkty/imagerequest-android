package com.plaping.imagerequest.view.general;

import android.content.Context;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.plaping.imagerequest.view.photo.CropView;

/**
 * Created by deknaew_bws on 10/27/15.
 */
public class OutBoundCancelRecyclerView extends RecyclerView {
    public OutBoundCancelRecyclerView(Context context) {
        super(context);
    }

    public OutBoundCancelRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private float y = 0;

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        VerticalViewPager verticalViewPager = ((VerticalViewPager) (getParent().getParent().getParent()));
        CropView cropView = (CropView) verticalViewPager.getChildAt(0);


        if (ev.getAction() == MotionEvent.ACTION_UP) {
            Log.d("OutBoundCancelRecyclerView", "Up");
        }
//        cropView.setViewEnabled(true);
        else if (ev.getAction() == MotionEvent.ACTION_MOVE) {

            if (ev.getY() < 0 && y > 0) {
                Log.d("OutBoundCancelRecyclerView", "Move to other");

                long downTime = SystemClock.uptimeMillis();
                long eventTime = SystemClock.uptimeMillis() + 100;
                float x = ev.getRawX();
                float y = ev.getRawY() - 160;

                int metaState = 0;
                MotionEvent motionEvent = MotionEvent.obtain(
                        downTime,
                        eventTime,
                        MotionEvent.ACTION_DOWN,
                        x,
                        y,
                        metaState
                );



                verticalViewPager.dispatchTouchEvent(motionEvent);
            } else if (!OutBoundCancelRecyclerView.this.canScrollVertically(-10) && ((VerticalViewPager) (getParent().getParent().getParent())).getCurrentItem() == 1) {

                Log.d("OutBoundCancelRecyclerView", "Can't Scroll Vertically : -10");
                long downTime = SystemClock.uptimeMillis();
                long eventTime = SystemClock.uptimeMillis() + 100;
                float x = ev.getRawX();
                float y = ev.getRawY() - 160;

                int metaState = 0;
                MotionEvent motionEvent = MotionEvent.obtain(
                        downTime,
                        eventTime,
                        MotionEvent.ACTION_DOWN,
                        x,
                        y,
                        metaState
                );

                verticalViewPager.dispatchTouchEvent(motionEvent);

            }


            if (ev.getY() < 0) {
                y = ev.getY();
                return false;
            }
        }

        y = ev.getY();
        return super.onTouchEvent(ev);
    }


}

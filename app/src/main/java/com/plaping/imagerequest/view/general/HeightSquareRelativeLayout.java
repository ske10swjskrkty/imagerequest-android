package com.plaping.imagerequest.view.general;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by deknaew_bws on 1/3/16.
 */
public class HeightSquareRelativeLayout extends RelativeLayout {
    public HeightSquareRelativeLayout(Context context) {
        super(context);
    }

    public HeightSquareRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeightSquareRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    }
}

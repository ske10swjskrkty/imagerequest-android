package com.plaping.imagerequest.view.sticker;

import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;


import com.plaping.imagerequest.R;

import java.util.ArrayList;
import java.util.List;

public class ViewOnTouchListener implements View.OnTouchListener {
    private static List<SingleFingerView> viewList =
            new ArrayList<SingleFingerView>();

    Point pushPoint;
    int lastImgLeft;
    int lastImgTop;
    FrameLayout.LayoutParams viewLP;
    FrameLayout.LayoutParams pushBtnLP;
    FrameLayout.LayoutParams deleteBtnLP;
    int lastPushBtnLeft;
    int lastPushBtnTop;
    int lastDeleteBtnLeft;
    int lastDeleteBtnTop;
    private View mPushView;
    private View mDeleteView;
    private SingleFingerView parent;

    public ViewOnTouchListener(View mPushView, View mDeleteView, SingleFingerView parent) {
        this.mPushView = mPushView;
        this.mDeleteView = mDeleteView;
        this.parent = parent;
        viewList.add(parent);

    }

    public static void removeView(SingleFingerView view) {
        view.removeView(view);
    }

    public static void clearViews() {
        viewList.clear();
    }

    public static void clearSelection() {
        for (SingleFingerView v : viewList) {
            if (v != null) {
                v.mPushView.setVisibility(View.GONE);
                v.mDeleteView.setVisibility(View.GONE);
                v.mView.setImageResource(android.R.color.transparent);
            }
        }


    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                if (null == viewLP) {
                    viewLP = (FrameLayout.LayoutParams) view.getLayoutParams();
                }
                if (null == pushBtnLP) {
                    pushBtnLP = (FrameLayout.LayoutParams) mPushView.getLayoutParams();
                }
                if (null == deleteBtnLP) {
                    deleteBtnLP = (FrameLayout.LayoutParams) mDeleteView.getLayoutParams();
                }

                for (SingleFingerView v : viewList) {
                    if (v != null && v != parent) {
                        v.mPushView.setVisibility(View.GONE);
                        v.mDeleteView.setVisibility(View.GONE);
                        v.mView.setImageResource(android.R.color.transparent);
                    }
                }

                mPushView.setVisibility(View.VISIBLE);
                mDeleteView.setVisibility(View.VISIBLE);
                parent.bringToFront();
                parent.mView.setImageResource(R.drawable.sticker_border);

                pushPoint = getRawPoint(event);
                lastImgLeft = viewLP.leftMargin;
                lastImgTop = viewLP.topMargin;
                lastPushBtnLeft = pushBtnLP.leftMargin;
                lastPushBtnTop = pushBtnLP.topMargin;

                lastDeleteBtnLeft = deleteBtnLP.leftMargin;
                lastDeleteBtnTop = deleteBtnLP.topMargin;
                break;
            case MotionEvent.ACTION_MOVE:
                Point newPoint = getRawPoint(event);
                float moveX = newPoint.x - pushPoint.x;
                float moveY = newPoint.y - pushPoint.y;

                viewLP.leftMargin = (int) (lastImgLeft + moveX);
                viewLP.topMargin = (int) (lastImgTop + moveY);
                view.setLayoutParams(viewLP);

                pushBtnLP.leftMargin = (int) (lastPushBtnLeft + moveX);
                pushBtnLP.topMargin = (int) (lastPushBtnTop + moveY);
                mPushView.setLayoutParams(pushBtnLP);

                deleteBtnLP.leftMargin = (int) (lastDeleteBtnLeft + moveX);
                deleteBtnLP.topMargin = (int) (lastDeleteBtnTop + moveY);
                mDeleteView.setLayoutParams(deleteBtnLP);

                break;

        }
        return true;
    }


    private Point getRawPoint(MotionEvent event) {
        return new Point((int) event.getRawX(), (int) event.getRawY());
    }
}

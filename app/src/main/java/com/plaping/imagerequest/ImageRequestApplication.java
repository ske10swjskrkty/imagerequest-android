package com.plaping.imagerequest;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.FontStyle;
import com.plaping.imagerequest.managers.BitmapManager;
import com.plaping.imagerequest.managers.FontManager;
import com.plaping.imagerequest.managers.StickerManager;
import com.plaping.imagerequest.managers.UserManager;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by deknaew_bws on 3/31/16.
 */
public class ImageRequestApplication extends Application {


    private BitmapManager bitmapManager;
    private StickerManager stickerManager;
    private FontManager fontManager;
    private ImageRequestHttpService httpService;
    private UserManager userManager;

    private String accessToken;

    @Override
    public void onCreate() {
        super.onCreate();
        bitmapManager = new BitmapManager(this);
        stickerManager = new StickerManager(this);
        fontManager = new FontManager(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        userManager = new UserManager(this);

        fontManager.getFontStyleList().add(new FontStyle(Typeface.DEFAULT, "DEFAULT"));
        fontManager.getFontStyleList().add(new FontStyle(Typeface.DEFAULT_BOLD, "DEFAULT BOLD"));
        fontManager.getFontStyleList().add(new FontStyle(Typeface.MONOSPACE, "MONOSPACE"));
        fontManager.getFontStyleList().add(new FontStyle(Typeface.SANS_SERIF, "SANS SERIF"));
        fontManager.getFontStyleList().add(new FontStyle(Typeface.SERIF, "SERIF"));
        httpService = new ImageRequestHttpService(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public ImageRequestHttpService getHttpService() {
        return this.httpService;
    }

    public BitmapManager getBitmapManager() {
        return bitmapManager;
    }

    public void setBitmapManager(BitmapManager bitmapManager) {
        this.bitmapManager = bitmapManager;
    }

    public StickerManager getStickerManager() {
        return stickerManager;
    }

    public void setStickerManager(StickerManager stickerManager) {
        this.stickerManager = stickerManager;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    public FontManager getFontManager() {
        return fontManager;
    }

    public void setFontManager(FontManager fontManager) {
        this.fontManager = fontManager;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}

package com.plaping.imagerequest.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Request;
import com.plaping.imagerequest.gif.AnimatedGifEncoder;
import com.plaping.imagerequest.view.general.TagListItem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class GifEditActivity extends BaseActivity {

    ArrayList<String> filePaths = new ArrayList<>();
    private int delay = 500;
    private File file;
    private ImageView imvPreview;
    private ProgressBar pgProgress;
    private RelativeLayout rloCreateNew;
    private EditText edtDelay;
    private RelativeLayout rloAddTag;
    private EditText edtTag;
    private RelativeLayout rloPost;
    private EditText edtCaption;
    private LinearLayout lloTags;
    private ImageRequestApplication app;
    private List<String> tagList = new ArrayList<>();
    private Request request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif_edit);
        file = new File(Environment.getExternalStorageDirectory() + "/ImgReqGif_" + System.currentTimeMillis() + ".gif");
        imvPreview = (ImageView) findViewById(R.id.imv_preview);
        pgProgress = (ProgressBar) findViewById(R.id.pg_bar);
        rloCreateNew = (RelativeLayout) findViewById(R.id.rlo_create_new);
        edtDelay = (EditText) findViewById(R.id.edt_delay);
        rloAddTag = (RelativeLayout) findViewById(R.id.rlo_add_tag);
        edtTag = (EditText) findViewById(R.id.edt_tag);
        rloPost = (RelativeLayout) findViewById(R.id.rlo_post);
        edtCaption = (EditText) findViewById(R.id.edt_caption);
        lloTags = (LinearLayout) findViewById(R.id.llo_tags);
        app = (ImageRequestApplication) getApplicationContext();
        request = (Request) getIntent().getSerializableExtra("request");
        if (request != null)
            Log.d("GifApplyRequest", "GifEditActivity >> Request :" + request.getId());


        findViewById(R.id.rlo_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        showProgressDialog();

        new Thread(new Runnable() {
            @Override
            public void run() {
                decodeGif(delay);
                hideProgressDialog();


            }
        }).start();

        rloCreateNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        int delay = Integer.parseInt(edtDelay.getText().toString());
                        decodeGif(delay);
                        hideProgressDialog();


                    }
                }).start();
            }
        });

        if (request != null) {
            Log.d("RequestIntent", "Request still here");
        } else {
            Log.d("RequestIntent", "No request found");
        }


        rloAddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = edtTag.getText().toString();
                edtTag.setText("");
                if (!tag.trim().equals("")) {
                    TagListItem item = new TagListItem(GifEditActivity.this);
                    item.setTagText(tag);
                    tagList.add(tag);
                    lloTags.addView(item);
                }

            }
        });

        rloPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showProgressDialog();

                String message = edtCaption.getText().toString();
                String tag = "";

                for (int i = 0; i < tagList.size(); i++) {
                    tag += tagList.get(i);
                    if (i != tagList.size() - 1) {
                        tag += ",";
                    }
                }


                Log.d("PublishActivity", "Tag : " + tag);
                Log.d("PublishActivity", "Caption : " + message);


                if (request == null) {
                    app.getHttpService().postPost(app.getAccessToken(), message, tag, file, new ImageRequestHttpService.OnResponseCallback<String>() {
                        @Override
                        public void onResponse(boolean success, Throwable error, String data) {
                            hideProgressDialog();
                            if (success) {

                                sendBroadcast(new Intent("publicationFinished"));
                                finish();
                            } else {
                                Toast.makeText(GifEditActivity.this, "Post failed, please try again later.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {

                    app.getHttpService().postPostToRequest(request.getId(), app.getAccessToken(), message, tag, file, new ImageRequestHttpService.OnResponseCallback<String>() {
                        @Override
                        public void onResponse(boolean success, Throwable error, String data) {
                            hideProgressDialog();
                            if (success) {

                                sendBroadcast(new Intent("publicationFinished"));
                                finish();
                            } else {
                                Toast.makeText(GifEditActivity.this, "Post failed, please try again later.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            }
        });


    }

    private void decodeGif(int delay) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pgProgress.setVisibility(View.VISIBLE);
            }
        });
        filePaths = getIntent().getStringArrayListExtra("paths");
        if (filePaths == null) {
            finish();
            return;
        }
        AnimatedGifEncoder encoder = new AnimatedGifEncoder();


        try {
            FileOutputStream fos = new FileOutputStream(file);
            encoder.setDelay(delay);
            encoder.setRepeat(1000);
            encoder.start(fos);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (String path : filePaths) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap bitmap = BitmapFactory.decodeFile(path, options);
            encoder.addFrame(bitmap);

        }
        encoder.finish();


        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Glide.with(GifEditActivity.this)
                        .load(file.getAbsoluteFile())
                        .asGif()
                        .skipMemoryCache(true)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.drawable.imgreq_placeholder)
                        .listener(new RequestListener<File, GifDrawable>() {
                            @Override
                            public boolean onException(Exception e, File model, Target<GifDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GifDrawable resource, File model, Target<GifDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                pgProgress.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(imvPreview);

            }
        });
    }
}

package com.plaping.imagerequest.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Request;
import com.plaping.imagerequest.view.general.TagListItem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PublishActivity extends BaseActivity {

    private ImageView imvPhoto;
    private EditText edtCaption;
    private EditText edtTags;
    private RelativeLayout rloAddTag;
    private LinearLayout lloTags;
    private ImageRequestApplication app;
    private List<String> tagList = new ArrayList<>();
    private RelativeLayout rloPost;
    private Request request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish);
        app = (ImageRequestApplication) getApplicationContext();
        request = (Request) getIntent().getSerializableExtra("request");

        imvPhoto = (ImageView) findViewById(R.id.imv_photo);
        edtCaption = (EditText) findViewById(R.id.edt_caption);
        edtTags = (EditText) findViewById(R.id.edt_tags);
        rloAddTag = (RelativeLayout) findViewById(R.id.rlo_add);
        lloTags = (LinearLayout) findViewById(R.id.llo_tags);
        rloPost = (RelativeLayout) findViewById(R.id.rlo_post);
        findViewById(R.id.rlo_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (request != null) {
            Log.d("RequestIntent", "Request still here");
        } else {
            Log.d("RequestIntent", "No request found");
        }

        if (request != null)
            Log.d("ApplyRequest", "PublishActivity >> Request : " + request.getId());


        final Bitmap b = app.getBitmapManager().getBitmapFromSharedPrefernce("postphoto");
        imvPhoto.setImageBitmap(b);

        rloAddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = edtTags.getText().toString();
                edtTags.setText("");
                if (!tag.trim().equals("")) {
                    TagListItem item = new TagListItem(PublishActivity.this);
                    item.setTagText(tag);
                    tagList.add(tag);
                    lloTags.addView(item);
                }

            }
        });

        rloPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showProgressDialog();

                String message = edtCaption.getText().toString();
                String tag = "";

                for (int i = 0; i < tagList.size(); i++) {
                    tag += tagList.get(i);
                    if (i != tagList.size() - 1) {
                        tag += ",";
                    }
                }


                Log.d("PublishActivity", "Tag : " + tag);
                Log.d("PublishActivity", "Caption : " + message);


                File file = new File(Environment.getExternalStorageDirectory() + "/ImageRequest_" + System.currentTimeMillis() + ".png");
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    b.compress(Bitmap.CompressFormat.PNG, 100, fos);

                    if (request == null) {
                        app.getHttpService().postPost(app.getAccessToken(), message, tag, file, new ImageRequestHttpService.OnResponseCallback<String>() {
                            @Override
                            public void onResponse(boolean success, Throwable error, String data) {
                                hideProgressDialog();
                                if (success) {

                                    sendBroadcast(new Intent("publicationFinished"));
                                    finish();
                                } else {
                                    Toast.makeText(PublishActivity.this, "Post failed, please try again later.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    } else {

                        Log.d("ApplyRequest", "post to request : " + request.getId());

                        app.getHttpService().postPostToRequest(request.getId(), app.getAccessToken(), message, tag, file, new ImageRequestHttpService.OnResponseCallback<String>() {
                            @Override
                            public void onResponse(boolean success, Throwable error, String data) {
                                hideProgressDialog();
                                if (success) {

                                    sendBroadcast(new Intent("publicationFinished"));
                                    finish();
                                } else {
                                    Toast.makeText(PublishActivity.this, "Post failed, please try again later.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });


    }
}

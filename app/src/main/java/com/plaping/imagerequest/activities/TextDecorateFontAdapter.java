package com.plaping.imagerequest.activities;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.view.textdecorate.FontListItem;

/**
 * Created by Bestiiz on 20/3/2559.
 */
public class TextDecorateFontAdapter extends BaseAdapter{
    private Context context;
    private ImageRequestApplication app;

    public TextDecorateFontAdapter(Context context){
        this.context = context;
        this.app = (ImageRequestApplication) context.getApplicationContext();


    }
    @Override
    public int getCount() {
        return app.getFontManager().getFontStyleList().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FontListItem item = createListItem(convertView);
        item.fill(app.getFontManager().getFontStyleList().get(position));

        return item;
    }
    public FontListItem createListItem(View convertView){

        if(convertView == null || !(convertView instanceof FontListItem)){
            return new FontListItem(this.context);
        }

        return (FontListItem)convertView;

    }
}

package com.plaping.imagerequest.activities;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;

public class SearchActivity extends BaseActivity {

    private RecyclerView rcvSearch;
    private SearchActivityAdapter adapter;
    private EditText edtSearch;
    private CountDownTimer timer;
    private String keyword;
    private ImageRequestApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        edtSearch = (EditText) findViewById(R.id.edt_search);
        rcvSearch = (RecyclerView) findViewById(R.id.rcv_search);
        adapter = new SearchActivityAdapter(this);

        rcvSearch.setLayoutManager(new LinearLayoutManager(this));
        rcvSearch.setAdapter(adapter);
        app = (ImageRequestApplication) getApplicationContext();

        keyword = getIntent().getStringExtra("tag");


        timer = new CountDownTimer(500, 500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                adapter.load(edtSearch.getText().toString(), new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {


                        return false;
                    }
                });
            }
        };

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                timer.cancel();
                timer.start();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (keyword != null) {
            if (!keyword.trim().equals("")) {
                edtSearch.setText(keyword);
//                adapter.load(keyword, new Handler.Callback() {
//                    @Override
//                    public boolean handleMessage(Message msg) {
//
//                        return false;
//                    }
//                });
            }
        }


    }
}

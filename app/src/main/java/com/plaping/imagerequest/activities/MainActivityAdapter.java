package com.plaping.imagerequest.activities;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.fragment.FeedFragment;
import com.plaping.imagerequest.fragment.PopularFragment;
import com.plaping.imagerequest.fragment.UserFragment;

/**
 * Created by deknaew_bws on 3/31/16.
 */
public class MainActivityAdapter extends FragmentPagerAdapter {


    private Fragment[] fragments;
    private ImageRequestApplication app;

    public MainActivityAdapter(Context context, FragmentManager fm) {
        super(fm);
        app = (ImageRequestApplication) context.getApplicationContext();
        fragments = new Fragment[]{
                PopularFragment.newInstance(),
                FeedFragment.newInstance(),
                UserFragment.newInstance(app.getUserManager().getUser())
        };
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) return "Popular";
        if (position == 1) return "Feed";
        return "User";
    }
}

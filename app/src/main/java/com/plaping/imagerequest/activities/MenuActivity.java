package com.plaping.imagerequest.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.RelativeLayout;

import com.plaping.imagerequest.R;

public class MenuActivity extends BaseActivity {

    private FloatingActionButton fabMenu;
    private RelativeLayout rloContainer;
    private RelativeLayout rloNormal;
    private RelativeLayout rloGif;
    private RelativeLayout rloQuote;


    private BroadcastReceiver publicationFinishedReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        fabMenu = (FloatingActionButton) findViewById(R.id.fab_add);
        rloContainer = (RelativeLayout) findViewById(R.id.rlo_container);
        rloNormal = (RelativeLayout) findViewById(R.id.rlo_normal_image);
        rloGif = (RelativeLayout) findViewById(R.id.rlo_gif);
        rloQuote = (RelativeLayout) findViewById(R.id.rlo_quote);


        rloGif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuActivity.this, GifActivity.class));
            }
        });

        rloNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuActivity.this, PhotoActivity.class));
            }
        });

        publicationFinishedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        IntentFilter updatePagesFilter = new IntentFilter("publicationFinished");
        registerReceiver(publicationFinishedReceiver, updatePagesFilter);

        rloQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, QuoteActivity.class);
                startActivity(intent);
            }
        });


        Animation dialogAnimation = AnimationUtils.loadAnimation(this, R.anim.open_dialog);
        rloContainer.startAnimation(dialogAnimation);

        final Animation closeDialogAnimation = AnimationUtils.loadAnimation(this, R.anim.close_dialog);
        closeDialogAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rloContainer.setVisibility(View.GONE);
                finish();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        RotateAnimation rotateAnimation = new RotateAnimation(0, 45, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        final RotateAnimation rotateBackAnimation = new RotateAnimation(45, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateBackAnimation.setDuration(200);
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setDuration(200);
        rotateBackAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fabMenu.startAnimation(rotateAnimation);
        fabMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabMenu.startAnimation(rotateBackAnimation);
                rloContainer.startAnimation(closeDialogAnimation);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(publicationFinishedReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

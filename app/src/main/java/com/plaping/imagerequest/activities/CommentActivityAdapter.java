package com.plaping.imagerequest.activities;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.Toast;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.Comment;
import com.plaping.imagerequest.api.model.custom.CommentResponse;
import com.plaping.imagerequest.view.comment.CommentListItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by deknaew_bws on 4/17/16.
 */
public class CommentActivityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private ImageRequestApplication app;
    private String postId;
    private List<Comment> commentList = new ArrayList<>();

    public CommentActivityAdapter(Context context, String postId) {
        this.context = context;
        this.postId = postId;
        this.app = (ImageRequestApplication) context.getApplicationContext();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentViewHolder(new CommentListItem(context));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CommentViewHolder) {
            ((CommentViewHolder) holder).fill(commentList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    private class CommentViewHolder extends RecyclerView.ViewHolder {


        private CommentListItem item;

        public CommentViewHolder(CommentListItem itemView) {
            super(itemView);
            this.item = itemView;
        }

        private void fill(Comment comment) {
            item.fill(comment);
        }
    }

    public void comment(String message, String accessToken, File file, final Handler.Callback callback) {
        app.getHttpService().postCommentToPost(file, accessToken, postId, message, new ImageRequestHttpService.OnResponseCallback<String>() {
            @Override
            public void onResponse(boolean success, Throwable error, String data) {

                if (success) {
                    reload(callback);

                } else {
                    Toast.makeText(context, "Comment not success please try again later", Toast.LENGTH_SHORT).show();
                    callback.handleMessage(new Message());
                }
            }
        });


    }

    public void load(final Handler.Callback callback) {

        app.getHttpService().getComment(app.getAccessToken(), postId, new ImageRequestHttpService.OnResponseCallback<CommentResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, CommentResponse data) {

                callback.handleMessage(new Message());
                if (success) {
                    if (data.getComments() != null) {
                        commentList.addAll(Arrays.asList(data.getComments()));
                        notifyDataSetChanged();
                        Collections.reverse(commentList);
                    }

                }
            }
        });


    }

    public void reload(Handler.Callback callback) {
        commentList.clear();
        load(callback);

    }
}

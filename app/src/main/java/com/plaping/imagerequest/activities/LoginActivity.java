package com.plaping.imagerequest.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.User;


public class LoginActivity extends BaseActivity {

    private RelativeLayout rloContainer;
    private Animation scaleAnimation;
    private RelativeLayout rloSignIn;
    private EditText edtUsername;
    private EditText edtPassword;
    private ImageRequestApplication app;
    private RelativeLayout rloSignup;
    private BroadcastReceiver signUpCompletedBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("LoginActivity", "Created");

        setContentView(R.layout.activity_login);
        app = (ImageRequestApplication) getApplicationContext();
        rloContainer = (RelativeLayout) findViewById(R.id.rlo_container);
        rloSignIn = (RelativeLayout) findViewById(R.id.rlo_sign_in);
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        rloSignup = (RelativeLayout) findViewById(R.id.rlo_sign_up);
        signUpCompletedBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };

        IntentFilter filter = new IntentFilter("signUpCompleted");
        registerReceiver(signUpCompletedBroadcastReceiver, filter);


        rloSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                app.getHttpService().login(edtUsername.getText().toString(), edtPassword.getText().toString(), new ImageRequestHttpService.OnResponseCallback<String>() {
                    @Override
                    public void onResponse(boolean success, Throwable error, String data) {

                        hideProgressDialog();
                        if (success) {

                            Log.d("AccessToken", "Token : " + data);

                            SharedPreferences sp = getSharedPreferences("ImageRequest", MODE_PRIVATE);

                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString("accessToken", data);
                            editor.commit();
                            app.setAccessToken(data);
                            app.getHttpService().getMe(app.getAccessToken(), new ImageRequestHttpService.OnResponseCallback<User>() {
                                @Override
                                public void onResponse(boolean success, Throwable error, User data) {
                                    if (success) {
                                        app.getUserManager().setUser(data);
                                    } else {
                                        Toast.makeText(LoginActivity.this, "Failed to get user data, Please try again later.", Toast.LENGTH_SHORT).show();

                                    }
                                }
                            });


                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();

                        } else {
                            Toast.makeText(LoginActivity.this, "Sign in failed, Please try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        rloSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });


        scaleAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up);
        rloContainer.startAnimation(scaleAnimation);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(signUpCompletedBroadcastReceiver);
    }
}

package com.plaping.imagerequest.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.PostResponse;
import com.plaping.imagerequest.view.PostListItem;

public class PostActivity extends BaseActivity {

    private String postId;
    private PostListItem item;
    private ImageRequestApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        app = (ImageRequestApplication) getApplicationContext();
        postId = getIntent().getStringExtra("postid");
        if (postId == null) {
            finish();
            Toast.makeText(this, "Load post error, please try again later", Toast.LENGTH_SHORT).show();
            return;
        }
        findViewById(R.id.rlo_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        item = (PostListItem) findViewById(R.id.pli_post);
        showProgressDialog();

        app.getHttpService().getSinglePost(app.getAccessToken(), postId, new ImageRequestHttpService.OnResponseCallback<PostResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, PostResponse data) {
                hideProgressDialog();
                if (success) {
                    if (data.getPosts().length > 0) {
                        item.fill(data.getPosts()[0]);
                    } else {
                        finish();
                        Toast.makeText(PostActivity.this, "No Post Found!, please try again later", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });


    }
}

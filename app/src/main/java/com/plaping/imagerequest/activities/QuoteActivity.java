package com.plaping.imagerequest.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.Request;

public class QuoteActivity extends BaseActivity {

    private EditText edtQuote;
    private EditText edtCredit;
    private TextView tvQuote;
    private TextView tvCredit;
    private ImageRequestApplication app;
    private boolean isInAddEditMode;
    private Request request;
    private BroadcastReceiver publicationFinishedReceiver;
    private SeekBar sbAlpha;

    private RelativeLayout rloSelectImage;
    private ImageView imvBackground;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote);
        edtQuote = (EditText) findViewById(R.id.edt_quote);
        edtCredit = (EditText) findViewById(R.id.edt_credit);
        imvBackground = (ImageView) findViewById(R.id.imv_background);
        tvCredit = (TextView) findViewById(R.id.tv_credit);
        tvQuote = (TextView) findViewById(R.id.tv_quote);
        app = (ImageRequestApplication) getApplicationContext();
        request = (Request) getIntent().getSerializableExtra("request");
        isInAddEditMode = getIntent().getBooleanExtra("isInAddEditMode", false);
        rloSelectImage = (RelativeLayout) findViewById(R.id.rlo_select_image);
        sbAlpha = (SeekBar) findViewById(R.id.sb_alpha);
        sbAlpha.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                imvBackground.setAlpha((1.0f * progress) / (seekBar.getMax() * 1.0f));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        rloSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuoteActivity.this, PhotoActivity.class);
                intent.putExtra("isInAddEditMode", true);
                startActivityForResult(intent, 1);
            }
        });

        edtQuote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvQuote.setText(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtCredit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvCredit.setText("- " + s + " -");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        publicationFinishedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        IntentFilter updatePagesFilter = new IntentFilter("publicationFinished");
        registerReceiver(publicationFinishedReceiver, updatePagesFilter);


        findViewById(R.id.rlo_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showProgressDialog();


                new Thread(new Runnable() {
                    public void run() {

                        Bitmap squareBitmap = loadBitmapFromView(findViewById(R.id.rlo_all));

                        app.getBitmapManager().saveBitmapToSharedPreference(squareBitmap, "postphoto");

                        if (!isInAddEditMode) {

                            Intent intent = new Intent(QuoteActivity.this, PublishActivity.class);
//                            intent.putExtra("request", request);

                            startActivity(intent);
                        } else {

                            setResult(RESULT_OK);
                            finish();
                        }

                        hideProgressDialog();
                    }
                }).start();
            }
        });

    }

    private Bitmap loadBitmapFromView(View v) {

        Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.RGB_565);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);

        b = b.createScaledBitmap(b, 800, 800, false);
        return b;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(publicationFinishedReceiver);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Bitmap b = app.getBitmapManager().getBitmapFromSharedPrefernce("postphoto");
                imvBackground.setImageBitmap(b);
            }
        }
    }
}

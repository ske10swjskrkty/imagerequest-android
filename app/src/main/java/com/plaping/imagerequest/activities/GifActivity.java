package com.plaping.imagerequest.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.Request;
import com.plaping.imagerequest.view.camera.CameraView;
import com.plaping.imagerequest.view.camera.DrawingView;
import com.plaping.imagerequest.view.general.HeightSquareImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class GifActivity extends BaseActivity {

    private FrameLayout rloCameraView;
    private CameraView cameraView;
    private DrawingView drawingView;
    private SurfaceView surfaceView;
    private Camera camera;
    private RelativeLayout rloFlash;
    private RelativeLayout rloShutter;
    private RelativeLayout rloSwap;
    private Bitmap savedBitmap;
    private TextView tvFlash;
    private int cameraId;
    private ImageView imvFlash;
    private LinearLayout lloImage;
    private ArrayList<String> filePaths = new ArrayList<>();
    private RelativeLayout rloNext;
    private Request request;

    private BroadcastReceiver publicationFinishedBroadcastReceiver;
    private ScaleAnimation scaleAnimation;
    private boolean isInAddEditMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif);
        if (!checkCameraHardware(this)) {
            Toast.makeText(this, "No Camera Detected!", Toast.LENGTH_SHORT).show();
            this.finish();
        }

        request = (Request) getIntent().getSerializableExtra("request");

        if (request != null) Log.d("GifApplyRequest", "GifActivity >> Request :" + request.getId());

        publicationFinishedBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        IntentFilter filter = new IntentFilter("publicationFinished");
        registerReceiver(publicationFinishedBroadcastReceiver, filter);


        isInAddEditMode = getIntent().getBooleanExtra("isInAddEditMode", false);
        lloImage = (LinearLayout) findViewById(R.id.llo_image);
        rloNext = (RelativeLayout) findViewById(R.id.rlo_next);

        rloNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isInAddEditMode) {
                    Intent intent = new Intent();
                    intent.putStringArrayListExtra("paths", filePaths);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Intent intent = new Intent(GifActivity.this, GifEditActivity.class);
                    intent.putStringArrayListExtra("paths", filePaths);
                    intent.putExtra("request", request);
                    startActivity(intent);
                }
            }
        });

        scaleAnimation = new ScaleAnimation(1, -1, 1, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(200);
        surfaceView = new SurfaceView(this);
        tvFlash = (TextView) findViewById(R.id.tv_flash);
        tvFlash.setText("off");
        drawingView = new DrawingView(this);

        drawingView.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        ));

        cameraView = new CameraView(this, surfaceView, drawingView);
        cameraView.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        ));
        cameraView.setKeepScreenOn(true);
//
        rloCameraView = (FrameLayout) findViewById(R.id.rlo_camera);
        rloFlash = (RelativeLayout) findViewById(R.id.rlo_flash);
        rloShutter = (RelativeLayout) findViewById(R.id.rlo_shutter);
        rloSwap = (RelativeLayout) findViewById(R.id.rlo_swap);
        imvFlash = (ImageView) findViewById(R.id.imv_flash);

        rloShutter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                takePicture();

            }
        });

        rloSwap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.startAnimation(scaleAnimation);

                int camCount = Camera.getNumberOfCameras();
                if (camCount < 2) return;

                stopCamera();
                cameraId = (cameraId + 1) % 2;


                if (cameraId == 1) {
                    drawingView.setVisibility(View.GONE);
                    rloFlash.setAlpha(0.5f);

                } else {
                    drawingView.setVisibility(View.VISIBLE);
                    rloFlash.setAlpha(1f);
                }
                startCamera();
            }
        });


        rloFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (camera != null) {
                    Camera.Parameters p = cameraButtonSetup();
                    if (p != null) camera.setParameters(p);
                }
            }
        });


        rloCameraView.addView(cameraView);
        rloCameraView.addView(drawingView);

        cameraButtonSetup();
    }

    private Camera.Parameters cameraButtonSetup() {
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters.getFlashMode().equals(Camera.Parameters.FLASH_MODE_ON)) {

                //TODO edit the flash reaction when toggle the flash
                tvFlash.setText("off");
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            } else {
                tvFlash.setText("on");
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            }

            return parameters;
        }
        return null;
    }

    protected void stopCamera() {
        if (camera != null) {
            camera.stopPreview();
            cameraView.setCamera(null);
            camera.release();
            camera = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopCamera();
    }

    @Override
    public void onResume() {
        super.onResume();

        startCamera();

    }

    private Camera.Size chooseBestSize(List<Camera.Size> sizes) {

        DisplayMetrics metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float heightPixels = metrics.heightPixels * 1.0f;
        float widthPixels = metrics.widthPixels * 1.0f;

        for (Camera.Size s : sizes) {

//            if (s.equals(camera.getParameters().getPreviewSize())) {
//
//                Log.d("ChooseBestSize", "Size : " + s.width + " x " + s.height);
//                return s;
//            }
        }
        return sizes.get(sizes.size() - 1);
    }

    protected void takePicture() {
        try {
            MediaActionSound sound = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                sound = new MediaActionSound();
                sound.play(MediaActionSound.SHUTTER_CLICK);
            }

            Log.d("Post", "capture!");


            Camera.Parameters parameters = camera.getParameters();
            Camera.Size biggestSize = chooseBestSize(parameters.getSupportedPictureSizes());

            parameters.setPictureSize(biggestSize.width, biggestSize.height);
            camera.setParameters(parameters);
            Log.d("Post", "CameraActivity : Width : " + parameters.getPictureSize().width + " /// Height : " + parameters.getPictureSize().height + "");
            for (Camera.Size s : parameters.getSupportedPictureSizes()) {
                Log.d("Post", "CameraActivity : Picture size : " + s.width + " x " + s.height);
            }


            camera.takePicture(
                    null,
                    null,
                    new Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(final byte[] data, final Camera camera) {


                            new Thread(new Runnable() {
                                @Override
                                public void run() {


                                    savedBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

                                    Log.d("GifActivity", "Image Width : " + savedBitmap.getWidth() + " || Height : " + savedBitmap.getHeight());

                                    Matrix matrix = new Matrix();
                                    Bitmap squareBitmap = null;

                                    Log.d("GifActivity", "CameraId : " + cameraId);
                                    if (cameraId == 0) {

                                        matrix.postRotate(90);
                                        squareBitmap = savedBitmap.createBitmap(savedBitmap, 0, 0, savedBitmap.getHeight(), savedBitmap.getHeight(), matrix, false);
                                    } else if (cameraId == 1) {

                                        matrix.postScale(-1, 1);
                                        matrix.postRotate(90);
                                        squareBitmap = savedBitmap.createBitmap(savedBitmap, savedBitmap.getWidth() - savedBitmap.getHeight(), 0, savedBitmap.getHeight(), savedBitmap.getHeight(), matrix, false);
                                    }


                                    if (squareBitmap != savedBitmap) {
                                        savedBitmap.recycle();
                                    }


//                                    ImageRequestApplication app = (ImageRequestApplication) GifActivity.this.getApplicationContext();
//                                    app.getBitmapManager().saveBitmapToSharedPreference(squareBitmap, "postphoto");
//
//                                    Log.d("CameraFragment", "dismiss");

                                    final HeightSquareImageView imvPhoto = new HeightSquareImageView(GifActivity.this);
                                    imvPhoto.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                    imvPhoto.setImageBitmap(squareBitmap);


                                    GifActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            lloImage.addView(imvPhoto);
                                        }
                                    });

                                    File file = new File(Environment.getExternalStorageDirectory() + "/" + "GIF_" + System.currentTimeMillis() + ".jpg");
                                    try {
                                        FileOutputStream fos = new FileOutputStream(file);
                                        squareBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                                        filePaths.add(file.getAbsolutePath());
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();

                                    }


                                    GifActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            stopCamera();
                                            startCamera();
                                        }
                                    });


//                                    Intent intent = new Intent();
////                                    intent.putExtra("imagepath", pictureFile.getAbsolutePath());
//                                    GifActivity.this.setResult(Activity.RESULT_OK, intent);
//
//                                    if (GifActivity.this.getClass().getName().contains("CameraActivity")) {
//                                        GifActivity.this.finish();
//                                    }
//
//
//                                    if (!isInAddEditMode) {
//                                        Intent cropIntent = new Intent(GifActivity.this, FilterActivity.class);
//
//                                        Request request = (Request) GifActivity.this.getIntent().getSerializableExtra("request");
//                                        cropIntent.putExtra("request", request);
//
//                                        startActivity(cropIntent);
//                                    } else {
//                                        GifActivity.this.setResult(Activity.RESULT_OK);
//                                        GifActivity.this.finish();
//                                    }

                                }
                            }).start();
                        }
                    });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void startCamera() {
        Log.d("CameraActivity", "StartCamera");
        int numCams = Camera.getNumberOfCameras();
        if (numCams > 0) {

            camera = Camera.open(cameraId);
            Camera.Parameters param = camera.getParameters();
            camera.startPreview();
            cameraView.setCamera(camera);
        }
    }


    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(publicationFinishedBroadcastReceiver);
    }
}

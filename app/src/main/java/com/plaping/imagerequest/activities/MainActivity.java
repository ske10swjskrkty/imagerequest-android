package com.plaping.imagerequest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.utils.ScreenUtils;

public class MainActivity extends BaseActivity {

    private ViewPager vpMain;
    private MainActivityAdapter adapter;
    private FloatingActionButton fabAdd;

    private TabLayout tabLayout;
    private ImageRequestApplication app;
    private RelativeLayout rloHead;
    private RelativeLayout rloSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vpMain = (ViewPager) findViewById(R.id.vp_main);
        fabAdd = (FloatingActionButton) findViewById(R.id.fab_add);
        adapter = new MainActivityAdapter(this, getSupportFragmentManager());
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        rloHead = (RelativeLayout) findViewById(R.id.rlo_head);
        rloSearch = (RelativeLayout) findViewById(R.id.rlo_search);
        vpMain.setOffscreenPageLimit(4);

        rloSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SearchActivity.class));
            }
        });


        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MenuActivity.class));

            }
        });

        vpMain.setAdapter(adapter);
        tabLayout.setupWithViewPager(vpMain);


    }

    public float getTabHeight() {

        float num = 0;
        num += ScreenUtils.convertDpToPixel(60, this);
        num += ScreenUtils.convertDpToPixel(60, this);
        num -= getStatusBarHeight();

        Log.d("AllHeight", "tabLayout : " + tabLayout.getHeight());
        Log.d("AllHeight", "rloHead : " + rloHead.getHeight());
        return num;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}

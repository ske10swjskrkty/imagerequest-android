package com.plaping.imagerequest.activities;


import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.Request;
import com.plaping.imagerequest.filter.BaseFilter;
import com.plaping.imagerequest.filter.custom.Filter00;
import com.plaping.imagerequest.filter.custom.Filter01;
import com.plaping.imagerequest.filter.custom.Filter02;
import com.plaping.imagerequest.filter.custom.Filter03;
import com.plaping.imagerequest.filter.custom.Filter04;
import com.plaping.imagerequest.filter.custom.Filter05;
import com.plaping.imagerequest.filter.custom.Filter06;
import com.plaping.imagerequest.filter.custom.Filter07;
import com.plaping.imagerequest.filter.custom.Filter08;
import com.plaping.imagerequest.filter.custom.Filter09;
import com.plaping.imagerequest.filter.custom.Filter10;
import com.plaping.imagerequest.filter.custom.Filter11;
import com.plaping.imagerequest.filter.custom.Filter12;
import com.plaping.imagerequest.filter.custom.Filter13;
import com.plaping.imagerequest.filter.custom.Filter14;
import com.plaping.imagerequest.filter.custom.Filter15;
import com.plaping.imagerequest.utils.AutoResizeTextView;
import com.plaping.imagerequest.utils.BitmapUtils;
import com.plaping.imagerequest.utils.ImageUtil;
import com.plaping.imagerequest.view.dialog.TextDecorateDialog;
import com.plaping.imagerequest.view.filter.FilterItemView;
import com.plaping.imagerequest.view.filter.FrameItemView;
import com.plaping.imagerequest.view.general.ColorPickerView;
import com.plaping.imagerequest.view.sticker.SingleFingerView;
import com.plaping.imagerequest.view.sticker.StickerImageView;
import com.plaping.imagerequest.view.sticker.StickerTextView;
import com.plaping.imagerequest.view.sticker.StickerView;
import com.plaping.imagerequest.view.sticker.ViewOnTouchListener;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageSharpenFilter;

public class FilterActivity extends BaseActivity {

    private BroadcastReceiver publicationFinishedReceiver;
    private Request request;

    private static final String[] FILTER_NAMES = {
            "Default",
            "RoadVille",
            "Palio",
            "Brighti",
            "Blueday",
            "Vintage",
            "Lomonic",
            "SoSo",
            "Light II",
            "Oldi",
            "Kani",
            "Folki",
            "Atoma",
            "Lloyd",
            "Krypton",
            "Foma"
    };
    private TextView tvwTitle;
    private ImageButton btnCancel;
    private ImageButton btnConfirm;
    private ImageView imvFilteredImage;
    private ImageView imvFrame;
    private Bitmap originalImage;
    private Bitmap filteredImage;
    private FrameLayout floStickerBoard;

    private HorizontalScrollView hsvFilters;
    private HorizontalScrollView hsvFrames;
    private LinearLayout hsvStickers;

    private RelativeLayout rloFilter;
    private RelativeLayout rloFrame;
    private RelativeLayout rloSticker;
    private RelativeLayout rloTextDecorate;

    private ImageView imvFilterIcon;
    private ImageView imvFrameIcon;
    private ImageView imvStickerIcon;
    private ImageView imvTextDecorateIcon;

    private int selectedFrame = -1;

    private List<FilterItemView> filterItemViewList;
    private List<FrameItemView> frameItemViewList;
    private List<ImageView> stickerThumbList;

    private List<BaseFilter> filterList;
    private List<Integer> frameList;
    private List<List<Pair<Integer, Integer>>> stickerSetList;
    private List<SingleFingerView> stickerList;

    private ImageRequestApplication app;

    private int currentStickerSet;
    Bitmap savedImage;
    private FrameLayout floStickerText;

    private RelativeLayout rloImageDecorator;

    //--------------------Text Decorate-------------------------

    private RelativeLayout rloTextFont;
    private RelativeLayout rloTextColor;
    private RelativeLayout rloTextOpacity;
    private RelativeLayout rloTextStroke;

    private ImageView imvTextFontIcon;
    private ImageView imvTextColorIcon;
    private ImageView imvTextOpacityIcon;
    private ImageView imvTextStrokeIcon;

    private RelativeLayout rloListViewFont;
    private RelativeLayout rloColor;
    private RelativeLayout rloOpacity;
    private RelativeLayout rloStroke;

    private ListView lvFont;
    private TextDecorateFontAdapter textDecorateFontAdapter;


    private SeekBar sbOpacity;
    private TextView tvOpacity;

    private SeekBar sbWidthStroke;
    private RelativeLayout rloPreviewColorStroke;
    private TextView tvWidthStroke;


    private TabLayout tabLayout;
    private RelativeLayout rloColorStroke;
    private RelativeLayout rloPropotiesStroke;


    private ColorPickerView cpText;
    private StickerView currentStickerView;
    private ColorPickerView cpStroke;

    private int startTextColor;
    private int startStrokeColor;

    private TextDecorateDialog textDecorateDialog;
    private TextDecorateDialog editTextDecorateDialog;

    private ImageView imvRed;
    private ImageView imvGreen;
    private ImageView imvBlue;
    private ImageView imvBlack;
    private ImageView imvWhite;
    private ImageView imvYellow;

    private RelativeLayout rloTextDecorator;

    private void setButtonsNormal() {
        imvFilterIcon.setImageResource(R.drawable.btn_normal_filter);
        imvFrameIcon.setImageResource(R.drawable.btn_normal_frame);
        imvStickerIcon.setImageResource(R.drawable.btn_normal_sticker);
    }

    private void setButtonsNormalTextDecorate() {
        imvTextColorIcon.setImageResource(R.drawable.btn_normal_text_color);
        imvTextStrokeIcon.setImageResource(R.drawable.btn_normal_text_stroke);
        imvTextOpacityIcon.setImageResource(R.drawable.btn_normal_text_opacity);
        imvTextFontIcon.setImageResource(R.drawable.btn_normal_text_font);
    }

    private void setLayoutInvisible() {
        rloListViewFont.setVisibility(View.GONE);
        rloColor.setVisibility(View.GONE);
        rloOpacity.setVisibility(View.GONE);
        rloStroke.setVisibility(View.GONE);

    }

    private void hideToolbars() {
        hsvFilters.setVisibility(View.GONE);
        hsvFrames.setVisibility(View.GONE);
        hsvStickers.setVisibility(View.GONE);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        request = (Request) getIntent().getSerializableExtra("request");

        if (request != null)
            Log.d("ApplyRequest", "FilterActivity >> Request : " + request.getId());


        Log.d("onCreate", "FilterActivity");

        ViewOnTouchListener.clearViews();

        setContentView(R.layout.activity_filter);

        app = (ImageRequestApplication) getApplicationContext();
        app.getStickerManager().getStickerList().clear();
        app.getStickerManager().getStickerTextList().clear();

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tvwTitle = (TextView) findViewById(R.id.tvwTitle);
        rloImageDecorator = (RelativeLayout) findViewById(R.id.rloImageDecorator);

        floStickerText = (FrameLayout) findViewById(R.id.floStickerText);
        imvFilterIcon = (ImageView) findViewById(R.id.imvFilterIcon);
        imvFrameIcon = (ImageView) findViewById(R.id.imvFrameIcon);
        imvStickerIcon = (ImageView) findViewById(R.id.imvStickerIcon);

        hsvFilters = (HorizontalScrollView) findViewById(R.id.hsvFilters);
        hsvFrames = (HorizontalScrollView) findViewById(R.id.hsvFrames);
        hsvStickers = (LinearLayout) findViewById(R.id.hsvStickers);

        rloFilter = (RelativeLayout) findViewById(R.id.rloFilter);
        rloFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideToolbars();
                hsvFilters.setVisibility(View.VISIBLE);

                setButtonsNormal();
                imvFilterIcon.setImageResource(R.drawable.btn_selected_filter);
            }
        });

        rloFrame = (RelativeLayout) findViewById(R.id.rloFrame);
        rloFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideToolbars();
                hsvFrames.setVisibility(View.VISIBLE);
                setButtonsNormal();
                imvFrameIcon.setImageResource(R.drawable.btn_selected_frame);
            }
        });

        rloSticker = (RelativeLayout) findViewById(R.id.rloSticker);
        rloSticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideToolbars();
                hsvStickers.setVisibility(View.VISIBLE);

                setButtonsNormal();
                imvStickerIcon.setImageResource(R.drawable.btn_selected_sticker);
            }
        });

        rloTextDecorate = (RelativeLayout) findViewById(R.id.rloTextDecorate);
        rloTextDecorate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hiddenImageDecorator();
                showTextDecorator();
                showCreateTextDialog();

//                savePropertySticker();
//                saveClearPropertyStickerText();
//
//                final ProgressDialog dialog = new ProgressDialog(FilterActivity.this);
//                dialog.setMessage(getResources().getString(R.string.inProgress) + "...");
//                dialog.setCancelable(false);
//                dialog.show();
//
//                final Intent intent = new Intent(FilterActivity.this, TextDecorateActivity.class);
//                intent.putExtra("imvFrame", selectedFrame);
//                //intent.putExtra("stickerList" , (Serializable) stickerList);
//                // intent.putParcelableArrayListExtra("stickerList",stickerList);
//
//                new Thread(new Runnable() {
//                    public void run() {
//                       // prepareToTextDecorate();
//                        startActivity(intent);
//                        dialog.dismiss();
//                    }
//                }).start();


            }
        });

        // Stickers
        floStickerBoard = (FrameLayout) findViewById(R.id.floStickerBoard);
        floStickerBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelectionAll();
                hiddenTextDecorator();
                showImageDecorator();
            }
        });

        stickerThumbList = new ArrayList<ImageView>();
        stickerSetList = new ArrayList<List<Pair<Integer, Integer>>>();
        stickerList = new ArrayList<SingleFingerView>();
        currentStickerSet = 0;

        initializeStickerSets();

        LinearLayout lloStickerSets = (LinearLayout) findViewById(R.id.lloStickerSets);
        for (int i = 0; i < lloStickerSets.getChildCount(); i++) {
            ImageView stickerSetView = (ImageView) lloStickerSets.getChildAt(i);
            if (stickerSetView == null) continue;

            final int position = i;
            stickerSetView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectStickerSet(position);
                }
            });
        }

        LinearLayout lloStickers = (LinearLayout) findViewById(R.id.lloStickers);
        for (int i = 0; i <= lloStickers.getChildCount(); i++) {
            ImageView stickerView = (ImageView) lloStickers.getChildAt(i);
            if (stickerView == null) continue;

            final int position = i;
            stickerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectSticker(position);
                }
            });

            stickerThumbList.add(stickerView);
        }

        // Filter Buttons
        filterList = new ArrayList<BaseFilter>();
        filterItemViewList = new ArrayList<FilterItemView>();

        LinearLayout lloFilters = (LinearLayout) findViewById(R.id.lloFilters);
        int filterItemCount = lloFilters.getChildCount();

        for (int i = 0; i < filterItemCount; i++) {
            View v = lloFilters.getChildAt(i);
            FilterItemView filterItem = (FilterItemView) v;
            filterItem.setName(FILTER_NAMES[i]);
            filterItem.setOnFilterItemSelectListener(new FilterItemView.OnFilterItemSelectListener() {
                @Override
                public void onFilterItemSelect(FilterItemView sender) {
                    applyFilter(filterItemViewList.indexOf(sender));
                }
            });
            filterItemViewList.add(filterItem);
        }

        frameList = new ArrayList<Integer>();
        frameItemViewList = new ArrayList<FrameItemView>();

        LinearLayout lloFrames = (LinearLayout) findViewById(R.id.lloFrames);
        int frameItemCount = lloFrames.getChildCount();

        for (int i = 0; i < frameItemCount; i++) {
            View v = lloFrames.getChildAt(i);
            final FrameItemView frameItem = (FrameItemView) v;

            frameItem.setOnFilterItemSelectListener(new FrameItemView.OnFrameItemSelectListener() {
                @Override
                public void onFrameItemSelect(FrameItemView sender) {
                    applyFrame(frameItemViewList.indexOf(sender));
                }
            });
            frameItemViewList.add(frameItem);
        }

        btnCancel = (ImageButton) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnConfirm = (ImageButton) findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FilterActivity", ">>>>>>");

                ProgressDialog dialog = new ProgressDialog(FilterActivity.this);
                dialog.setMessage("Loading...");
                dialog.setCancelable(false);


                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(final DialogInterface dialog) {
                        Log.d("FilterActivity", "dialog showed");


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                resizeAndNavigate();
                                dialog.dismiss();
                            }
                        });


                    }
                });

                Log.d("FilterActivity", "dialog show");
                dialog.show();
            }
        });

        initializeFilters();
        initializeFrames();


        try {

            ImageRequestApplication app = (ImageRequestApplication) getApplicationContext();

            originalImage = app.getBitmapManager().getBitmapFromSharedPrefernce("postphoto");

            if (originalImage == null) {
                Toast.makeText(getApplicationContext(), "Image load failed", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                originalImage = BitmapUtils.getResizedBitmap(originalImage, 800, 800);
            }


            GPUImage gpuImage = new GPUImage(getApplicationContext());
            gpuImage.setFilter(new GPUImageSharpenFilter(0.23f));
            originalImage = gpuImage.getBitmapWithFilterApplied(originalImage);

            if (originalImage == null) {
                Log.d("Post", "Original Image is null!");
            } else {
                Log.d("Post", "Original Image Size : " + originalImage.getWidth() + " x " + originalImage.getHeight());
            }
        } catch (OutOfMemoryError e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
            finish();
        }

        imvFilteredImage = (ImageView) findViewById(R.id.imvFilteredImage);
        imvFilteredImage.setImageBitmap(originalImage);

        imvFrame = (ImageView) findViewById(R.id.imvFrame);

        publicationFinishedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        IntentFilter updatePagesFilter = new IntentFilter("publicationFinished");
        registerReceiver(publicationFinishedReceiver, updatePagesFilter);


        //applyFilter(0);
        filterItemViewList.get(0).setSelected(true);

        //----------------------------Text Decorator----------------------
        rloTextDecorator = (RelativeLayout) findViewById(R.id.rloTextDecorator);
        cpText = (ColorPickerView) findViewById(R.id.cp_text);
        cpStroke = (ColorPickerView) findViewById(R.id.cp_stroke);
        textDecorateDialog = new TextDecorateDialog(this);
        editTextDecorateDialog = new TextDecorateDialog(this);


        rloTextColor = (RelativeLayout) findViewById(R.id.rloTextColor);
        rloTextFont = (RelativeLayout) findViewById(R.id.rloTextFont);
        rloTextOpacity = (RelativeLayout) findViewById(R.id.rloTextOpacity);
        rloTextStroke = (RelativeLayout) findViewById(R.id.rloTextStroke);

        imvTextColorIcon = (ImageView) findViewById(R.id.imvTextColorIcon);
        imvTextFontIcon = (ImageView) findViewById(R.id.imvTextFontIcon);
        imvTextStrokeIcon = (ImageView) findViewById(R.id.imvTextStrokeIcon);
        imvTextOpacityIcon = (ImageView) findViewById(R.id.imvTextOpacityIcon);

        rloListViewFont = (RelativeLayout) findViewById(R.id.rloListViewFont);
        rloColor = (RelativeLayout) findViewById(R.id.rloColor);
        rloOpacity = (RelativeLayout) findViewById(R.id.rloOpacity);
        rloStroke = (RelativeLayout) findViewById(R.id.rloStroke);

        lvFont = (ListView) findViewById(R.id.listview_font);
        textDecorateFontAdapter = new TextDecorateFontAdapter(this);
        lvFont.setAdapter(textDecorateFontAdapter);
        lvFont.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((StickerTextView) currentStickerView).setFont(app.getFontManager().getFontStyleList().get(position).getTypeface());
            }
        });

        imvRed = (ImageView) findViewById(R.id.imv_red);
        GradientDrawable bgRedShape = (GradientDrawable) imvRed.getBackground();
        bgRedShape.setColor(Color.RED);
        imvRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cpText.setColor(Color.RED);
                ((StickerTextView) currentStickerView).setTextColor(cpText.getColor());
            }
        });

        imvGreen = (ImageView) findViewById(R.id.imv_green);
        GradientDrawable bgGreenShape = (GradientDrawable) imvGreen.getBackground();
        bgGreenShape.setColor(Color.GREEN);
        imvGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cpText.setColor(Color.GREEN);
                ((StickerTextView) currentStickerView).setTextColor(cpText.getColor());
            }
        });

        imvBlue = (ImageView) findViewById(R.id.imv_blue);
        GradientDrawable bgBlueShape = (GradientDrawable) imvBlue.getBackground();
        bgBlueShape.setColor(Color.BLUE);
        imvBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cpText.setColor(Color.BLUE);
                ((StickerTextView) currentStickerView).setTextColor(cpText.getColor());
            }
        });

        imvWhite = (ImageView) findViewById(R.id.imv_white);
        GradientDrawable bgWhiteShape = (GradientDrawable) imvWhite.getBackground();
        bgWhiteShape.setColor(Color.WHITE);
        imvWhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cpText.setColor(Color.WHITE);
                ((StickerTextView) currentStickerView).setTextColor(cpText.getColor());
            }
        });

        imvBlack = (ImageView) findViewById(R.id.imv_black);
        GradientDrawable bgBlackShape = (GradientDrawable) imvBlack.getBackground();
        bgBlackShape.setColor(Color.BLACK);
        imvBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cpText.setColor(Color.BLACK);
                ((StickerTextView) currentStickerView).setTextColor(cpText.getColor());
            }
        });

        imvYellow = (ImageView) findViewById(R.id.imv_yellow);
        GradientDrawable bgYellowShape = (GradientDrawable) imvYellow.getBackground();
        bgYellowShape.setColor(Color.YELLOW);
        imvYellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cpText.setColor(Color.YELLOW);
                ((StickerTextView) currentStickerView).setTextColor(cpText.getColor());
            }
        });


        sbOpacity = (SeekBar) findViewById(R.id.sb_opacity);
        sbOpacity.getThumb().setColorFilter(getResources().getColor(R.color.petpolargreen), PorterDuff.Mode.SRC_IN);
        sbOpacity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ((StickerTextView) currentStickerView).setOpacity(progress);
                tvOpacity.setText(progress + " %");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });
        tvOpacity = (TextView) findViewById(R.id.tv_opacity);

        sbWidthStroke = (SeekBar) findViewById(R.id.sb_width_stroke);
        sbWidthStroke.getThumb().setColorFilter(getResources().getColor(R.color.petpolargreen), PorterDuff.Mode.SRC_IN);
        tvWidthStroke = (TextView) findViewById(R.id.tv_width_stroke);
        sbWidthStroke.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvWidthStroke.setText(String.valueOf(progress) + " px");
                if (progress == 0) {
                    Log.d("stroke", "false , disable");
                    ((StickerTextView) currentStickerView).setStroke(false);


                } else {
                    Log.d("stroke", "true , enable");
                    ((StickerTextView) currentStickerView).setStroke(true);


                }
                ((StickerTextView) currentStickerView).setStrokeWidth(progress * 2);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        rloColorStroke = (RelativeLayout) findViewById(R.id.rlo_color_stroke);
        rloPropotiesStroke = (RelativeLayout) findViewById(R.id.rlo_propoties_stroke);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("PROPERTIES"));
        tabLayout.addTab(tabLayout.newTab().setText("COLOR"));
        tabLayout.setTabTextColors(getResources().getColorStateList(R.color.white));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    //Log.d("TextDecorateActivity" , "0");
                    rloColorStroke.setVisibility(View.GONE);
                    rloPropotiesStroke.setVisibility(View.VISIBLE);


                }
                if (tab.getPosition() == 1) {
                    // Log.d("TextDecorateActivity" , "1");
                    rloPropotiesStroke.setVisibility(View.GONE);
                    rloColorStroke.setVisibility(View.VISIBLE);


                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //tabLayout.setupWithViewPager(viewPager);


        rloTextFont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLayoutInvisible();
                rloListViewFont.setVisibility(View.VISIBLE);
                setButtonsNormalTextDecorate();
                imvTextFontIcon.setImageResource(R.drawable.btn_selected_text_font);

            }
        });

        rloTextColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLayoutInvisible();
                rloColor.setVisibility(View.VISIBLE);
                setButtonsNormalTextDecorate();
                imvTextColorIcon.setImageResource(R.drawable.btn_selected_text_color);

            }
        });

        rloTextOpacity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLayoutInvisible();
                rloOpacity.setVisibility(View.VISIBLE);
                setButtonsNormalTextDecorate();
                imvTextOpacityIcon.setImageResource(R.drawable.btn_selected_text_opacity);

            }
        });
        rloTextStroke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLayoutInvisible();
                rloStroke.setVisibility(View.VISIBLE);
                setButtonsNormalTextDecorate();
                imvTextStrokeIcon.setImageResource(R.drawable.btn_selected_text_stroke);

            }
        });
        startTextColor = Color.WHITE;
        startStrokeColor = Color.WHITE;
        cpText.setOnColorChangeListener(new ColorPickerView.OnColorChangedListener() {
            @Override
            public void colorChanged() {
                Log.d("listenercolor", "TextDecorate");
                int color = cpText.getColor();
                ((StickerTextView) currentStickerView).setTextColor(color);


            }
        });

//        cpText.setColor(Color.RED);


        cpStroke.setOnColorChangeListener(new ColorPickerView.OnColorChangedListener() {
            @Override
            public void colorChanged() {
                int color = cpStroke.getColor();
                ((StickerTextView) currentStickerView).setStrokeColor(color);
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
//        floStickerText.removeAllViewsInLayout();
//        floStickerBoard.removeAllViewsInLayout();
//        if (app.getStickerManager().getStickerTextList().size() != 0) {
//
//           // addStickerText();
//
//        }
//        if (app.getStickerManager().getStickerList().size() != 0) {
//
//           // addSticker();
//
//        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(publicationFinishedReceiver);

        super.onDestroy();
    }


    public void resizeAndNavigate() {
        clearSelectionAll();

        if (filteredImage == null) {
            savedImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
            Log.d("FilerActivity", "Originale Image Size : " + savedImage.getWidth() + " x " + savedImage.getHeight());

        } else {
            savedImage = filteredImage.copy(Bitmap.Config.ARGB_8888, true);
            Log.d("FilerActivity", "Filtered Image Size : " + savedImage.getWidth() + " x " + savedImage.getHeight());
        }

        Canvas canvas = new Canvas(savedImage);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);

        Bitmap stickerBitmap = ImageUtil.getBitmapFromView(floStickerBoard, android.R.color.transparent, null, null, null);

        canvas.drawBitmap(stickerBitmap,
                new Rect(0, 0, stickerBitmap.getWidth(), stickerBitmap.getHeight()),
                new Rect(0, 0, 800, 800),
                paint);
        stickerBitmap.recycle();
        Bitmap stickerTextBitmap = ImageUtil.getBitmapFromView(floStickerText, android.R.color.transparent, null, null, null);

        canvas.drawBitmap(stickerTextBitmap,
                new Rect(0, 0, stickerTextBitmap.getWidth(), stickerTextBitmap.getHeight()),
                new Rect(0, 0, 800, 800),
                paint);
        stickerBitmap.recycle();


        if (selectedFrame != -1) {
            // Out of mem error line below
            Bitmap frameBitmap = BitmapFactory.decodeResource(getResources(), selectedFrame);
            canvas.drawBitmap(frameBitmap,
                    new Rect(0, 0, frameBitmap.getWidth(), frameBitmap.getHeight()),
                    new Rect(0, 0, 800, 800),
                    paint);
            frameBitmap.recycle();
        }

        ImageRequestApplication app = (ImageRequestApplication) getApplicationContext();
        app.getBitmapManager().saveBitmapToSharedPreference(savedImage, "postphoto");

        Intent intent = new Intent(this, PublishActivity.class);
        intent.putExtra("request", request);
        startActivity(intent);
    }

    private void hiddenImageDecorator() {

        rloImageDecorator.setVisibility(View.INVISIBLE);

    }

    private void showImageDecorator() {
        rloImageDecorator.setVisibility(View.VISIBLE);
        tvwTitle.setText("Decorate");

    }

    private void hiddenTextDecorator() {

        rloTextDecorator.setVisibility(View.INVISIBLE);

    }


    private void showTextDecorator() {
        rloTextDecorator.setVisibility(View.VISIBLE);
        tvwTitle.setText("Decorate");

    }

    private void showCreateTextDialog() {


        textDecorateDialog.show();
        textDecorateDialog.setRloOKOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //createSticker
                if (textDecorateDialog.getText().equals("")) {
                    Toast.makeText(FilterActivity.this, "Empty", Toast.LENGTH_SHORT).show();
                } else {
                    //addText(textDecorateDialog.getText());
                    addStickerTextView(textDecorateDialog.getText());
                    textDecorateDialog.setText("");
                    textDecorateDialog.dismiss();

                }

            }
        });

        textDecorateDialog.setCancelable(false);
        textDecorateDialog.setCanceledOnTouchOutside(false);

        textDecorateDialog.setRloCancelOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                hiddenTextDecorator();
                showImageDecorator();
                textDecorateDialog.dismiss();

            }
        });
    }

    private void showEditTextDialog() {
        editTextDecorateDialog.show();
        editTextDecorateDialog.setText(((StickerTextView) currentStickerView).getText());
        editTextDecorateDialog.setRloOKOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextDecorateDialog.getText().equals("")) {
                    Toast.makeText(FilterActivity.this, "Empty", Toast.LENGTH_SHORT).show();
                } else {
                    ((StickerTextView) currentStickerView).setText(editTextDecorateDialog.getText());
                    editTextDecorateDialog.dismiss();

                }

            }
        });

        editTextDecorateDialog.setCancelable(false);
        editTextDecorateDialog.setCanceledOnTouchOutside(false);

        editTextDecorateDialog.setRloCancelOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextDecorateDialog.dismiss();

            }
        });


    }

    private void addStickerTextView(String text) {
        StickerTextView stickerTextView = new StickerTextView(this);
        stickerTextView.setText(text);
        stickerTextView.setStroke(false);
        stickerTextView.setTextColor(startTextColor);
        stickerTextView.setStrokeColor(startStrokeColor);
        stickerTextView.setStrokeWidth(0);
        stickerTextView.setFocus(true);
        currentStickerView = stickerTextView;
        float alpha = ((AutoResizeTextView) stickerTextView.getMainView()).getAlpha();
        sbOpacity.setProgress((int) alpha * 100);
        stickerTextView.setOnFocusStickerTextViewListener(new StickerView.onFocusStickerTextViewListener() {
            @Override
            public void onFocus(StickerTextView stickerTextView) {
                currentStickerView = stickerTextView;
                if (!stickerTextView.isFocus()) {
                    clearSelection();
                }
                cpText.setColor(stickerTextView.getTextColor());
                cpStroke.setColor(stickerTextView.getStrokeColor());
                float alpha = ((AutoResizeTextView) stickerTextView.getMainView()).getAlpha();
                sbOpacity.setProgress((int) (alpha * 100));
                sbWidthStroke.setProgress((int) stickerTextView.getStrokeWidth() / 2);
                hiddenImageDecorator();
                showTextDecorator();

            }
        });
        stickerTextView.setOnClickStickerTextViewAgainListener(new StickerView.onClickStickerTextViewAgainListener() {
            @Override
            public void onClick() {
                Log.d("onclickagain", "............");
                showEditTextDialog();
            }
        });

        stickerTextView.setOnDeleteStickerTexViewListener(new StickerView.onDeleteStickerTexViewListener() {
            @Override
            public void onDelete() {
                hiddenTextDecorator();
                showImageDecorator();
            }
        });
        cpText.setColor(stickerTextView.getTextColor());
        cpStroke.setColor(stickerTextView.getStrokeColor());
        sbWidthStroke.setProgress((int) stickerTextView.getStrokeWidth() / 2);
        clearSelection();
        floStickerText.addView(stickerTextView);
        hiddenImageDecorator();
        showTextDecorator();


    }


    private int getResourceIdByName(String name) {
        try {
            return getResources().getIdentifier(name, "drawable", "com.plaping.imagerequest");
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private void initializeStickerSets() {
        for (int i = 1; i <= 4; i++) {
            List<Pair<Integer, Integer>> stickerList = new ArrayList<Pair<Integer, Integer>>();
            stickerSetList.add(stickerList);


            for (int j = 1; j <= 10; j++) {
                String smName = String.format("small_sticker_%02d_%02d", i, j);
                String lgName = String.format("sticker_%02d_%02d", i, j);
                Pair stickerPair = Pair.create(getResourceIdByName(lgName), getResourceIdByName(smName));
                stickerList.add(stickerPair);

            }
        }
    }

    private void initializeFilters() {
        filterList.add(new Filter00(this));
        filterList.add(new Filter01(this));
        filterList.add(new Filter02(this));
        filterList.add(new Filter03(this));
        filterList.add(new Filter04(this));
        filterList.add(new Filter05(this));
        filterList.add(new Filter06(this));
        filterList.add(new Filter07(this));
        filterList.add(new Filter08(this));
        filterList.add(new Filter09(this));
        filterList.add(new Filter10(this));
        filterList.add(new Filter11(this));
        filterList.add(new Filter12(this));
        filterList.add(new Filter13(this));
        filterList.add(new Filter14(this));
        filterList.add(new Filter15(this));
    }

    private void initializeFrames() {
        for (int i = 1; i <= 13; i++) {
            String frameName = String.format("frame_%02d", i);
            frameList.add(getResourceIdByName(frameName));
        }
    }

    public void applyFilter(int index) {
        if (index == -1) return;

        if (filteredImage != null) filteredImage.recycle();
        if (originalImage == null)
            Toast.makeText(getApplicationContext(), "Original Image : null", Toast.LENGTH_SHORT).show();


        BaseFilter filter = filterList.get(index);

        filteredImage = filter.apply(originalImage);
        imvFilteredImage.setImageBitmap(filteredImage);
    }

    public void applyFrame(int index) {
        selectedFrame = frameList.get(index).intValue();
        if (selectedFrame == 0) return;

        imvFrame.setImageResource(selectedFrame);
    }

    private void selectStickerSet(int position) {
        currentStickerSet = position;
        int count = 0;
        for (ImageView stickerThumbView : stickerThumbList) {
            stickerThumbView.setImageResource(stickerSetList.get(position).get(count++).second);

        }
    }

    private void selectSticker(int position) {
        int stickerRes = stickerSetList
                .get(currentStickerSet)
                .get(position)
                .first;
        //SingleFingerView sfvSticker = new SingleFingerView(this, stickerRes);
        StickerImageView sfvSticker = new StickerImageView(this);
        sfvSticker.setImageResource(stickerRes);
        sfvSticker.setOnFocusStickerImageViewListener(new StickerView.onFocusStickerImageViewListener() {
            @Override
            public void onFocus(StickerImageView stickerImageView) {
                currentStickerView = (StickerImageView) stickerImageView;
                clearSelection();
                hiddenTextDecorator();
                showImageDecorator();
            }
        });

        currentStickerView = sfvSticker;
        clearSelection();
        floStickerBoard.addView(sfvSticker);
        //cloneFloStickerBoard.addView(sfvSticker);

    }

//    private void prepareToTextDecorate() {
//        if (filteredImage == null) {
//            savedImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
//
//
//        } else {
//            savedImage = filteredImage.copy(Bitmap.Config.ARGB_8888, true);
//
//        }
//        PetPolarApplication app = (PetPolarApplication) getApplicationContext();
//        app.getBitmapManager().saveBitmapToSharedPreference(savedImage, "beforetextdecorateImage");
//
//
//    }

    private void clearSelectionAll() {
        if (floStickerBoard.getChildCount() != 0) {
            for (int i = 0; i < floStickerBoard.getChildCount(); i++) {
                // ((StickerImageView) floStickerBoard.getChildAt(i)).setControlItemsHidden(true);
                ((StickerImageView) floStickerBoard.getChildAt(i)).clearSelectionAll();
            }

        }

        if (floStickerText.getChildCount() != 0) {
            for (int i = 0; i < floStickerText.getChildCount(); i++) {
                // ((StickerImageView) floStickerBoard.getChildAt(i)).setControlItemsHidden(true);
                ((StickerTextView) floStickerText.getChildAt(i)).clearSelectionAll();
            }

        }

        currentStickerView = null;


    }

    private void clearSelection() {
        if (floStickerBoard.getChildCount() != 0) {
            for (int i = 0; i < floStickerBoard.getChildCount(); i++) {
                // ((StickerImageView) floStickerBoard.getChildAt(i)).setControlItemsHidden(true);
                ((StickerImageView) floStickerBoard.getChildAt(i)).clearSelectionAll();
            }

        }

        if (floStickerText.getChildCount() != 0) {
            for (int i = 0; i < floStickerText.getChildCount(); i++) {
                // ((StickerImageView) floStickerBoard.getChildAt(i)).setControlItemsHidden(true);
                ((StickerTextView) floStickerText.getChildAt(i)).clearSelectionAll();
            }

        }
        //currentStickerView.setFocus(true);


    }

//    private void addStickerText() {
//
//        for (StickerTextValue tempObject : app.getStickerManager().getStickerTextList()) {
//            StickerTextView stickerTextView = new StickerTextView(this);
//            //Log.d("sticker res", tempObject.getDrawable() + "");
//            //stickerImageView.setImageResource(tempObject.getDrawable());
//            stickerTextView.setTextColor(tempObject.getTextColor());
//            stickerTextView.setText(tempObject.getText());
//            stickerTextView.setStroke(tempObject.isStroke());
//            stickerTextView.setStrokeWidth(tempObject.getWidthText());
//            stickerTextView.setStrokeColor(tempObject.getTextStrokeColor());
//            stickerTextView.getLayoutParams().width = tempObject.getWidth();
//            stickerTextView.getLayoutParams().height = tempObject.getHeight();
//            stickerTextView.setOpacity(tempObject.getOpacity());
//            stickerTextView.setTop(tempObject.getTop());
//            stickerTextView.setLeft(tempObject.getLeft());
//            stickerTextView.setX(tempObject.getX());
//            stickerTextView.setY(tempObject.getY());
//            stickerTextView.setRotation(tempObject.getRotation());
//            stickerTextView.setControlItemsHidden(true);
//            stickerTextView.setOnTouchListener(null);
//            stickerTextView.setFont(tempObject.getTypefaceFont());
//            stickerTextView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    ((StickerTextView) v).setFocus(true);
//                    savePropertySticker();
//                    savePropertyStickerText();
//                    final Intent intent = new Intent(FilterActivity.this, TextDecorateActivity.class);
//                    intent.putExtra("editText", true);
//                    intent.putExtra("imvFrame", selectedFrame);
//                    final ProgressDialog dialog = new ProgressDialog(FilterActivity.this);
//                    dialog.setMessage(getResources().getString(R.string.inProgress) + "...");
//                    dialog.setCancelable(false);
//                    dialog.show();
//                    new Thread(new Runnable() {
//                        public void run() {
//                            prepareToTextDecorate();
//                            intent.putExtra("editText", true);
//                            startActivity(intent);
//                            dialog.dismiss();
//
//                        }
//                    }).start();
//
//                }
//            });
//            stickerTextView.setOpacity(tempObject.getOpacity());
//            floStickerText.addView(stickerTextView);
//
//        }
//
//
//    }
//
//    private void addSticker() {
//
//        for (StickerImageValue tempObject : app.getStickerManager().getStickerList()) {
//            StickerImageView stickerImageView = new StickerImageView(this);
//            Log.d("sticker res", tempObject.getDrawable() + "");
//            stickerImageView.setImageResource(tempObject.getDrawable());
//            stickerImageView.getLayoutParams().width = tempObject.getWidth();
//            stickerImageView.getLayoutParams().height = tempObject.getHeight();
//            stickerImageView.setTop(tempObject.getTop());
//            stickerImageView.setLeft(tempObject.getLeft());
//            stickerImageView.setX(tempObject.getX());
//            stickerImageView.setY(tempObject.getY());
//            stickerImageView.setRotation(tempObject.getRotation());
//            if (tempObject.isOnFous()) {
//                stickerImageView.setControlItemsHidden(false);
//                stickerImageView.setFocus(true);
//
//            } else {
//
//                stickerImageView.setControlItemsHidden(true);
//                stickerImageView.setFocus(false);
//            }
//            //Log.d("position","x"+stickerImageView.getX()+",y"+stickerImageView.getY());
//            floStickerBoard.addView(stickerImageView);
//
//        }
//
//
//    }
//
//    private void savePropertySticker() {
//        app.getStickerManager().getStickerList().clear();
//        for (int i = 0; i < floStickerBoard.getChildCount(); i++) {
//            StickerImageView child = (StickerImageView) floStickerBoard.getChildAt(i);
//            StickerImageValue objectIntent = new StickerImageValue();
//            objectIntent.setX(child.getX());
//            objectIntent.setY(child.getY());
//            objectIntent.setWidth(child.getLayoutParams().width);
//            objectIntent.setHeight(child.getLayoutParams().height);
//            objectIntent.setTop(child.getTop());
//            objectIntent.setLeft(child.getLeft());
//            objectIntent.setRotation(child.getRotation());
//            objectIntent.setDrawable((Integer) child.getMainView().getTag());
//            objectIntent.setOnFous(child.isFocus());
//
//            app.getStickerManager().getStickerList().add(objectIntent);
//
//
//        }
//
//
//    }
//
//    private void savePropertyStickerText() {
//
//        app.getStickerManager().getStickerTextList().clear();
//        for (int i = 0; i < floStickerText.getChildCount(); i++) {
//            StickerTextView child = (StickerTextView) floStickerText.getChildAt(i);
//            StickerTextValue objectIntent = new StickerTextValue();
//            objectIntent.setTextColor(child.getTextColor());
//            objectIntent.setTextStrokeColor(child.getStrokeColor());
//            objectIntent.setWidthText(child.getStrokeWidth());
//            objectIntent.setStroke(child.isStroke());
//            objectIntent.setX(child.getX());
//            objectIntent.setY(child.getY());
//            objectIntent.setWidth(child.getLayoutParams().width);
//            objectIntent.setHeight(child.getLayoutParams().height);
//            objectIntent.setTop(child.getTop());
//            objectIntent.setLeft(child.getLeft());
//            objectIntent.setRotation(child.getRotation());
//            objectIntent.setText(child.getText());
//            objectIntent.setOpacity((int) (child.getMainView().getAlpha() * 100));
//            objectIntent.setOnFous(child.isFocus());
//            objectIntent.setTypefaceFont(((AutoResizeTextView) child.getMainView()).getTypeface());
//            app.getStickerManager().getStickerTextList().add(objectIntent);
//
//
//        }
//    }

//    private void saveClearPropertyStickerText() {
//
//        app.getStickerManager().getStickerTextList().clear();
//        for (int i = 0; i < floStickerText.getChildCount(); i++) {
//            StickerTextView child = (StickerTextView) floStickerText.getChildAt(i);
//            StickerTextValue objectIntent = new StickerTextValue();
//            objectIntent.setTextColor(child.getTextColor());
//            objectIntent.setTextStrokeColor(child.getStrokeColor());
//            objectIntent.setWidthText(child.getStrokeWidth());
//            objectIntent.setStroke(child.isStroke());
//            objectIntent.setX(child.getX());
//            objectIntent.setY(child.getY());
//            objectIntent.setWidth(child.getLayoutParams().width);
//            objectIntent.setHeight(child.getLayoutParams().height);
//            objectIntent.setTop(child.getTop());
//            objectIntent.setLeft(child.getLeft());
//            objectIntent.setRotation(child.getRotation());
//            objectIntent.setText(child.getText());
//            objectIntent.setOpacity((int) (child.getMainView().getAlpha() * 100));
//            objectIntent.setOnFous(false);
//            objectIntent.setTypefaceFont(((AutoResizeTextView) child.getMainView()).getTypeface());
//            app.getStickerManager().getStickerTextList().add(objectIntent);
//
//
//        }
//    }


    private int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;


    }


}



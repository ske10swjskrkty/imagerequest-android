package com.plaping.imagerequest.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;

public class RequestCommentActivity extends BaseActivity {

    private RelativeLayout rloOk;
    private RecyclerView rcvComment;
    private RequestCommentActivityAdapter adapter;
    private EditText edtMessage;
    private String requestId;
    private ImageRequestApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_comment);

        requestId = getIntent().getStringExtra("requestid");

        if (requestId == null) {
            finish();
            return;
        }

        findViewById(R.id.rlo_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        app = (ImageRequestApplication) getApplicationContext();

        adapter = new RequestCommentActivityAdapter(this, requestId);
        rcvComment = (RecyclerView) findViewById(R.id.rcv_comment);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        rcvComment.setLayoutManager(linearLayoutManager);
        rcvComment.setAdapter(adapter);
        rloOk = (RelativeLayout) findViewById(R.id.rlo_ok);
        edtMessage = (EditText) findViewById(R.id.edt_message);

        rloOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtMessage.getText().toString().trim().equals("")) {
                    Toast.makeText(RequestCommentActivity.this, "Comment is blank, put something into it!", Toast.LENGTH_SHORT).show();

                } else {

                    showProgressDialog();
                    adapter.comment(edtMessage.getText().toString(), app.getAccessToken(), null, new Handler.Callback() {
                        @Override
                        public boolean handleMessage(Message msg) {
                            rcvComment.scrollTo(0, 0);
                            edtMessage.setText("");
                            hideProgressDialog();

                            return false;
                        }
                    });
                }
            }
        });

        showProgressDialog();
        adapter.load(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                hideProgressDialog();
                return false;
            }
        });
    }
}

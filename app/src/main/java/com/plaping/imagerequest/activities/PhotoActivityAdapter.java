package com.plaping.imagerequest.activities;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.plaping.imagerequest.fragment.CameraFragment;
import com.plaping.imagerequest.fragment.GalleryFragment;

/**
 * Created by deknaew_bws on 10/27/15.
 */
public class PhotoActivityAdapter extends FragmentPagerAdapter {


    private Context context;
    private Fragment[] fragments;

    public PhotoActivityAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        fragments = new Fragment[]{
                new GalleryFragment(),
                new CameraFragment()
        };
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return 2;
    }
}

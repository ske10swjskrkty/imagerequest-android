package com.plaping.imagerequest.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.Photo;
import com.plaping.imagerequest.api.model.custom.Request;
import com.plaping.imagerequest.fragment.GalleryFragment;
import com.plaping.imagerequest.utils.GalleryUtils;
import com.plaping.imagerequest.view.AlertDialog;
import com.plaping.imagerequest.view.photo.PhotoViewViewPager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PhotoActivity extends BaseActivity {

    private PhotoViewViewPager vpMain;
    private PhotoActivityAdapter adapter;

    private RelativeLayout rloGallery;
    private RelativeLayout rloCamera;

    private ArrayAdapter<String> spArrayAdapter;
    private List<String> albumsNameList = new ArrayList<String>();
    private Spinner spAlbumName;

    private TextView tvCamera;
    private RelativeLayout rloNext;
    private RelativeLayout rloSpinner;
    private RelativeLayout rloBack;
    private BroadcastReceiver publicationFinishedReceiver;

    private boolean isInAddEditMode = false;
    private AlertDialog alertDialog;
    private Request request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        request = (Request) getIntent().getSerializableExtra("request");
        if(request!=null)Log.d("ApplyRequest", "PhotoActivity >> Request : " + request.getId());

        this.isInAddEditMode = getIntent().getBooleanExtra("isInAddEditMode", false);

        Log.d("PhotoActivity", "Is In Add Edit Mode : " + isInAddEditMode);
        alertDialog = new AlertDialog(this);
        alertDialog.setTvAlertText("PetPolar require to access reading external storage permission\nPlease accept this permission to access the photo storage");
        alertDialog.setCancelButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setOkButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (permissionCheck == PackageManager.PERMISSION_GRANTED && cameraPermission == PackageManager.PERMISSION_GRANTED) {

            initView();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                alertDialog.show();


            } else {


                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 2);

            }
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 2: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    initView();

                } else {

                    //TODO handle when user denied the permission


                    finish();
                }
                return;
            }

        }
    }

    public void initView() {
        vpMain = (PhotoViewViewPager) findViewById(R.id.vp_main);
        rloCamera = (RelativeLayout) findViewById(R.id.rlo_camera);
        rloGallery = (RelativeLayout) findViewById(R.id.rlo_gallery);
        spAlbumName = (Spinner) findViewById(R.id.sp_albumname);
        tvCamera = (TextView) findViewById(R.id.tv_camera);
        rloNext = (RelativeLayout) findViewById(R.id.rlo_next);
        rloSpinner = (RelativeLayout) findViewById(R.id.rlo_spinner);
        rloBack = (RelativeLayout) findViewById(R.id.rlo_back);
        rloBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        publicationFinishedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        IntentFilter updatePagesFilter = new IntentFilter("publicationFinished");
        registerReceiver(publicationFinishedReceiver, updatePagesFilter);

        albumsNameList.add(0, "Gallery");
        final List<Photo> photoList = GalleryUtils.getImagesPath(this);

        Collections.reverse(photoList);

        for (Photo photo : photoList) {
            if (!albumsNameList.contains(photo.getAlbums())) {
                albumsNameList.add(photo.getAlbums());
            }
        }

        spArrayAdapter = new ArrayAdapter<String>(this, R.layout.view_spinner_list_item, R.id.tv_albumsname, albumsNameList);
        spAlbumName.setAdapter(spArrayAdapter);

        spAlbumName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((GalleryFragment) adapter.getItem(0)).setCurrentGalleryAlbum(albumsNameList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rloNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Log.d("CropView", "Press Next");


                final Bitmap squareBitmap = ((GalleryFragment) adapter.getItem(0)).getBitmap();
                Log.d("CropView", "Crop finished");
                final ImageRequestApplication app = (ImageRequestApplication) getApplicationContext();
                final ProgressDialog dialog = new ProgressDialog(PhotoActivity.this);
                dialog.setMessage("Loading");
                dialog.setCancelable(false);
                dialog.show();


                Log.d("CropView", "Save to share preference");

                new Thread(new Runnable() {
                    public void run() {

                        app.getBitmapManager().saveBitmapToSharedPreference(squareBitmap, "postphoto");

                        if (!isInAddEditMode) {

                            Intent intent = new Intent(PhotoActivity.this, FilterActivity.class);
                            intent.putExtra("request", request);
                            startActivity(intent);
                        } else {

                            setResult(RESULT_OK);
                            finish();
                        }
                        dialog.dismiss();
                    }
                }).start();


//                finish();
            }
        });

        rloSpinner.setVisibility(View.VISIBLE);
        rloNext.setVisibility(View.VISIBLE);
        tvCamera.setVisibility(View.GONE);

        adapter = new PhotoActivityAdapter(PhotoActivity.this.getSupportFragmentManager(), PhotoActivity.this);
        vpMain.setAdapter(adapter);

        rloCamera.setBackgroundColor(Color.parseColor("#3b3b3b"));
        rloGallery.setBackgroundColor(Color.parseColor("#1d1d1d"));

        rloCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vpMain.setCurrentItem(1, true);
            }
        });

        rloGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vpMain.setCurrentItem(0, true);
            }
        });

        vpMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    rloCamera.setBackgroundColor(Color.parseColor("#3b3b3b"));
                    rloGallery.setBackgroundColor(Color.parseColor("#1d1d1d"));

                    rloSpinner.setVisibility(View.VISIBLE);
                    rloNext.setVisibility(View.VISIBLE);
                    tvCamera.setVisibility(View.GONE);
                } else {
                    rloCamera.setBackgroundColor(Color.parseColor("#1d1d1d"));
                    rloGallery.setBackgroundColor(Color.parseColor("#3b3b3b"));

                    rloSpinner.setVisibility(View.GONE);
                    rloNext.setVisibility(View.GONE);
                    tvCamera.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (photoList.size() == 0) {
            rloNext.setVisibility(View.GONE);

        }
    }


    public boolean isInAddEditMode() {
        return this.isInAddEditMode;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (vpMain.getCurrentItem() == 0) {
            ((GalleryFragment) adapter.getItem(0)).onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(publicationFinishedReceiver);
    }

}

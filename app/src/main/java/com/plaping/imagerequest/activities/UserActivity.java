package com.plaping.imagerequest.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.fragment.UserFragment;

public class UserActivity extends BaseActivity {

    private RelativeLayout rloBack;

    private UserFragment userFragment;

    private TextView tvUsername;
    private ImageRequestApplication app;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        tvUsername = (TextView) findViewById(R.id.edt_username);

        User user = (User) getIntent().getSerializableExtra("user");
        if (user == null) {

            finish();

            return;
        }

        Log.d("UserActivity", user.toString());

        showProgressDialog();

        app = (ImageRequestApplication) getApplicationContext();
        app.getHttpService().getUser(user.getId(), new ImageRequestHttpService.OnResponseCallback<User>() {
            @Override
            public void onResponse(boolean success, Throwable error, User data) {
                hideProgressDialog();
                if (success) {
                    userFragment = UserFragment.newInstance(data);
                    tvUsername.setText(data.getUsername());

                    rloBack = (RelativeLayout) findViewById(R.id.rlo_back);

                    rloBack.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, userFragment)
                            .commit();
                } else {

                    Toast.makeText(UserActivity.this, "Load user failed, please try again later", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });


    }
}

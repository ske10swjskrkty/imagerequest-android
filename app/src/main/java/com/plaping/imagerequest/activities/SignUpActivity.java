package com.plaping.imagerequest.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.gif.AnimatedGifEncoder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpActivity extends BaseActivity {

    private ImageView imvProfile;
    private EditText edtUsername;
    private EditText edtPassword;
    private EditText edtEmail;
    private File file;
    private int PHOTO_REQUEST_CODE = 0;
    private int GIF_REQUEST_CODE = 1;
    private ImageRequestApplication app;
    private RelativeLayout rloSignUp;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        app = (ImageRequestApplication) getApplicationContext();

        imvProfile = (ImageView) findViewById(R.id.imv_profile);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        rloSignUp = (RelativeLayout) findViewById(R.id.rlo_sign_up);


        imvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.show();

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        builder.setTitle("Choose type")
                .setItems(R.array.image_choose, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {

                            Intent intent = new Intent(SignUpActivity.this, GifActivity.class);
                            intent.putExtra("isInAddEditMode", true);
                            SignUpActivity.this.startActivityForResult(intent, GIF_REQUEST_CODE);
                        } else if (which == 1) {
                            Intent intent = new Intent(SignUpActivity.this, PhotoActivity.class);
                            intent.putExtra("isInAddEditMode", true);
                            SignUpActivity.this.startActivityForResult(intent, PHOTO_REQUEST_CODE);


                        }
                    }
                });
        alertDialog = builder.create();

        rloSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();
                String email = edtEmail.getText().toString();
                if (!isUsernameCorrect(username)) {
                    Toast.makeText(SignUpActivity.this, "Username incorrect!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isEmailCorrect(email)) {
                    Toast.makeText(SignUpActivity.this, "E-mail incorrect!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (file == null) {
                    Toast.makeText(SignUpActivity.this, "Please insert your profile picture.", Toast.LENGTH_SHORT).show();
                    return;
                }

                showProgressDialog();
                app.getHttpService().register(username, password, email, new ImageRequestHttpService.OnResponseCallback<String>() {
                    @Override
                    public void onResponse(boolean success, Throwable error, String data) {

                        if (success) {
                            final String accessToken = data;
                            Log.d("UserNew", "AccessToken : " + data);
                            if (file != null) {

                                app.getHttpService().updateUserProfilePicture(accessToken, file, new ImageRequestHttpService.OnResponseCallback<String>() {
                                    @Override
                                    public void onResponse(boolean success, Throwable error, String data) {
                                        if (success) {
                                            app.getHttpService().getMe(accessToken, new ImageRequestHttpService.OnResponseCallback<User>() {
                                                @Override
                                                public void onResponse(boolean success, Throwable error, User data) {
                                                    if (success) {
                                                        app.getUserManager().setUser(data);
                                                        hideProgressDialog();
                                                        SharedPreferences sp = getSharedPreferences("ImageRequest", MODE_PRIVATE);

                                                        SharedPreferences.Editor editor = sp.edit();
                                                        editor.putString("accessToken", accessToken);
                                                        editor.commit();
                                                        startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                                                        sendBroadcast(new Intent("signUpCompleted"));
                                                        finish();
                                                    } else {

                                                        Log.d("FailedLog", "Failed to get user");
                                                        hideProgressDialog();
                                                        Toast.makeText(SignUpActivity.this, "Sign Up Failed, Please try again later", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                        } else {
                                            Log.d("FailedLog", "Failed to update profile pic");
                                            hideProgressDialog();
                                            Toast.makeText(SignUpActivity.this, "Sign Up Failed, Please try again later", Toast.LENGTH_SHORT).show();

                                        }
                                    }
                                });

                            } else {
                                Log.d("FailedLog", "Failed to register");

                                hideProgressDialog();
                                Toast.makeText(SignUpActivity.this, "Sign Up Failed, Please try again later", Toast.LENGTH_SHORT).show();


                            }
                        }
                    }
                });


            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PHOTO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                file = new File(Environment.getExternalStorageDirectory() + "/profile.jpg");
                Bitmap bitmap = app.getBitmapManager().getBitmapFromSharedPrefernce("postphoto");
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                    imvProfile.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == GIF_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                file = new File(Environment.getExternalStorageDirectory() + "/profile.jpg");
                showProgressDialog();
                final ArrayList<String> filePaths = data.getStringArrayListExtra("paths");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        decodeGif(200, filePaths);
                        hideProgressDialog();
                    }
                }).start();

            }

        }
    }


    private boolean isUsernameCorrect(String username) {

        if (username.contains(" ")) {
            return false;
        }

        return true;
    }

    private boolean isEmailCorrect(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void decodeGif(int delay, ArrayList<String> filePaths) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
        if (filePaths == null) {
            finish();
            return;
        }
        AnimatedGifEncoder encoder = new AnimatedGifEncoder();

        try {
            FileOutputStream fos = new FileOutputStream(file);
            encoder.setDelay(delay);
            encoder.setRepeat(1000);
            encoder.start(fos);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (String path : filePaths) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap bitmap = BitmapFactory.decodeFile(path, options);
            encoder.addFrame(bitmap);

        }
        encoder.finish();


        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Glide.with(SignUpActivity.this)
                        .load(file.getAbsoluteFile())
                        .asGif()
                        .skipMemoryCache(true)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.drawable.imgreq_placeholder)
                        .listener(new RequestListener<File, GifDrawable>() {
                            @Override
                            public boolean onException(Exception e, File model, Target<GifDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GifDrawable resource, File model, Target<GifDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .into(imvProfile);

            }
        });
    }
}

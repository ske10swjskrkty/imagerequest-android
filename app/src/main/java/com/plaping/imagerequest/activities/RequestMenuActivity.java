package com.plaping.imagerequest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.Request;

public class RequestMenuActivity extends BaseActivity {


    private RelativeLayout rloNormalImage;
    private Request request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_menu);


        request = (Request) getIntent().getSerializableExtra("request");

        if (request == null) {
            finish();
            return;

        }


        rloNormalImage = (RelativeLayout) findViewById(R.id.rlo_normal_image);
        rloNormalImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RequestMenuActivity.this, PhotoActivity.class);
                intent.putExtra("request", request);
                startActivity(intent);
            }
        });

    }
}

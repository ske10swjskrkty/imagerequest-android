package com.plaping.imagerequest.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.gif.AnimatedGifEncoder;
import com.plaping.imagerequest.utils.UrlUtils;
import com.plaping.imagerequest.view.general.TagListItem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RequestActivity extends BaseActivity {

    private ImageView imvProfile;
    private TextView tvUsername;
    private User user;
    private RelativeLayout rloAddImage;

    private int PHOTO_REQUEST_CODE = 1;
    private int GIF_REQUEST_CODE = 2;
    private AlertDialog dialog;
    private ImageView imvPhoto;
    private File file;
    private Bitmap b;

    private EditText edtCaption;
    private EditText edtTags;
    private RelativeLayout rloAddTag;
    private LinearLayout lloTags;
    private ImageRequestApplication app;
    private List<String> tagList = new ArrayList<>();
    private RelativeLayout rloPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        imvProfile = (ImageView) findViewById(R.id.imv_profile);
        tvUsername = (TextView) findViewById(R.id.edt_username);
        imvPhoto = (ImageView) findViewById(R.id.imv_photo);
        edtCaption = (EditText) findViewById(R.id.edt_caption);
        edtTags = (EditText) findViewById(R.id.edt_tags);
        rloAddTag = (RelativeLayout) findViewById(R.id.rlo_add);
        lloTags = (LinearLayout) findViewById(R.id.llo_tags);
        rloPost = (RelativeLayout) findViewById(R.id.rlo_post);
        user = (User) getIntent().getSerializableExtra("user");
        file = new File(Environment.getExternalStorageDirectory() + "/ImgReqGif_" + System.currentTimeMillis() + ".gif");

        app = (ImageRequestApplication) getApplicationContext();
        rloAddImage = (RelativeLayout) findViewById(R.id.rlo_add_image);
        if (user == null) {
            finish();
            return;
        }

        Glide.with(this)
                .load(UrlUtils.BASE_IMG_URL + user.getImgUrl())
                .into(imvProfile);

        tvUsername.setText("" + user.getUsername());
        findViewById(R.id.rlo_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        rloAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RequestActivity.this, PhotoActivity.class);
                intent.putExtra("isInAddEditMode", true);
                startActivityForResult(intent, PHOTO_REQUEST_CODE);


            }
        });

        rloAddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = edtTags.getText().toString();
                edtTags.setText("");
                if (!tag.trim().equals("")) {
                    TagListItem item = new TagListItem(RequestActivity.this);
                    item.setTagText(tag);
                    tagList.add(tag);
                    lloTags.addView(item);
                }

            }
        });
        rloPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showProgressDialog();

                String message = edtCaption.getText().toString();
                String tag = "";

                for (int i = 0; i < tagList.size(); i++) {
                    tag += tagList.get(i);
                    if (i != tagList.size() - 1) {
                        tag += ",";
                    }
                }


                Log.d("RequestActivity", "Tag : " + tag);
                Log.d("RequestActivity", "Caption : " + message);


                File file = null;
                try {
                    if (b != null) {
                        file = new File(Environment.getExternalStorageDirectory() + "/Request_" + System.currentTimeMillis() + ".jpg");
                        FileOutputStream fos = new FileOutputStream(file);
                        b.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    }

                    app.getHttpService().postRequest(app.getAccessToken(), user.getId(), message, tag, file, new ImageRequestHttpService.OnResponseCallback<String>() {
                        @Override
                        public void onResponse(boolean success, Throwable error, String data) {
                            hideProgressDialog();
                            if (success) {

//                                sendBroadcast(new Intent("publicationFinished"));
                                finish();
                            } else {
                                Toast.makeText(RequestActivity.this, "Post failed, please try again later.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GIF_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                List<Path> paths = (List<Path>) data.getSerializableExtra("paths");
            }
        }
        if (requestCode == PHOTO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                b = app.getBitmapManager().getBitmapFromSharedPrefernce("postphoto");
                imvPhoto.setVisibility(View.VISIBLE);
                imvPhoto.setImageBitmap(b);
//                List<Path> paths = (List<Path>) data.getSerializableExtra("paths");
            }
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

    private void decodeGif(int delay, List<String> filePaths) {

        if (filePaths == null) {
            finish();
            return;
        }
        AnimatedGifEncoder encoder = new AnimatedGifEncoder();


        try {
            FileOutputStream fos = new FileOutputStream(file);
            encoder.setDelay(delay);
            encoder.setRepeat(1000);
            encoder.start(fos);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (String path : filePaths) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap bitmap = BitmapFactory.decodeFile(path, options);
            encoder.addFrame(bitmap);

        }
        encoder.finish();


        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Glide.with(RequestActivity.this)
                        .load(file.getAbsoluteFile())
                        .asGif()
                        .skipMemoryCache(true)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.drawable.imgreq_placeholder)
                        .listener(new RequestListener<File, GifDrawable>() {
                            @Override
                            public boolean onException(Exception e, File model, Target<GifDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GifDrawable resource, File model, Target<GifDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .into(imvPhoto);

            }
        });
    }

}
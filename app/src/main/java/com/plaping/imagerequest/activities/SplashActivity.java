package com.plaping.imagerequest.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import com.plaping.imagerequest.ImageRequestApplication;
import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.httpservice.ImageRequestHttpService;
import com.plaping.imagerequest.api.model.custom.User;

public class SplashActivity extends BaseActivity {

    private SharedPreferences sp;
    private SharedPreferences.Editor spEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new CountDownTimer(1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (isLoggedIn()) {
                    final ImageRequestApplication app = (ImageRequestApplication) getApplicationContext();
                    String accessToken = sp.getString("accessToken", "");
                    app.getHttpService().getMe(accessToken, new ImageRequestHttpService.OnResponseCallback<User>() {
                        @Override
                        public void onResponse(boolean success, Throwable error, User data) {
                            if(success){

                                app.getUserManager().setUser(data);
                                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                                finish();

                            }else{
                                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                                finish();

                            }
                        }
                    });


                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }


            }
        }.start();
    }


    private boolean isLoggedIn() {


        sp = getSharedPreferences("ImageRequest", MODE_PRIVATE);
        spEditor = sp.edit();

        String accessToken = sp.getString("accessToken", "");

        Log.d("AccessToken", "AT : " + accessToken);
        ImageRequestApplication app = (ImageRequestApplication) getApplicationContext();
        app.setAccessToken(accessToken);

        return !accessToken.equals("");
    }
}

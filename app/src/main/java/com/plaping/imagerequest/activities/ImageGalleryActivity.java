package com.plaping.imagerequest.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.api.model.custom.Photo;
import com.plaping.imagerequest.utils.GalleryUtils;
import com.plaping.imagerequest.view.AlertDialog;
import com.plaping.imagerequest.view.gallery.GalleryViewAdapter;
import com.plaping.imagerequest.view.gallery.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class ImageGalleryActivity extends BaseActivity {


    private RecyclerView rcvImage;
    private GalleryViewAdapter adapter;
    private List<String> albumsNameList = new ArrayList<String>();
    private Spinner spAlbumName;
    private ArrayAdapter<String> spArrayAdapter;
    private ImageView imvBack;

    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery);

        alertDialog = new AlertDialog(this);
        alertDialog.setTvAlertText("PetPolar require to access reading external storage permission\nPlease accept this permission to access the photo storage");
        alertDialog.setCancelButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setOkButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {

            initView();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                alertDialog.show();
            } else {


                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 2: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    initView();

                } else {

                    //TODO handle when user denied the permission


                    finish();
                }
                return;
            }

        }
    }


    private void initView() {
        List<Photo> photos = GalleryUtils.getImagesPath(this);

        imvBack = (ImageView) findViewById(R.id.imv_back);
        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spAlbumName = (Spinner) findViewById(R.id.sp_albumsname);
        for (Photo photo : photos) {
            if (!albumsNameList.contains(photo.getAlbums())) {
                albumsNameList.add(photo.getAlbums());
            }
        }

        Log.d("ImageGalleryActivity", "Albums name : " + albumsNameList.toString());

        albumsNameList.add(0, "Gallery");
        adapter = new GalleryViewAdapter(this, photos);

        adapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, Object obj, int position) {
                Intent intent = new Intent();
                intent.putExtra("imagepath", adapter.getShowPhotoList().get(position).getPath());
                ImageGalleryActivity.this.setResult(Activity.RESULT_OK, intent);
                ImageGalleryActivity.this.finish();
            }
        });

        spArrayAdapter = new ArrayAdapter<String>(this, R.layout.view_spinner_list_item, R.id.tv_albumsname, albumsNameList);
        spAlbumName.setAdapter(spArrayAdapter);
        spAlbumName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setAlbumName(albumsNameList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rcvImage = (RecyclerView) findViewById(R.id.rcv_imagegrid);
        rcvImage.setLayoutManager(new GridLayoutManager(this, 3));
        rcvImage.setHasFixedSize(true);


        rcvImage.setAdapter(adapter);

    }
}
package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;

public class GaussianVerticalBlurFilter extends Filter {

	private final String fshaderHeader = "const lowp int GAUSSIAN_SAMPLES = 9;							\n"
									   + "varying highp vec2 blurCoordinates[GAUSSIAN_SAMPLES];			\n";
	private final String fshader = "lowp vec4 sum = vec4(0.0);											\n"
								 + "sum += texture2D(s_texture, blurCoordinates[0]) * 0.05;				\n"
								 + "sum += texture2D(s_texture, blurCoordinates[1]) * 0.09;				\n"
								 + "sum += texture2D(s_texture, blurCoordinates[2]) * 0.12;				\n"
								 + "sum += texture2D(s_texture, blurCoordinates[3]) * 0.15;				\n"
								 + "sum += texture2D(s_texture, blurCoordinates[4]) * 0.18;				\n"
								 + "sum += texture2D(s_texture, blurCoordinates[5]) * 0.15;				\n"
								 + "sum += texture2D(s_texture, blurCoordinates[6]) * 0.12;				\n"
								 + "sum += texture2D(s_texture, blurCoordinates[7]) * 0.09;				\n"
								 + "sum += texture2D(s_texture, blurCoordinates[8]) * 0.05;				\n"
								 + "textureColor = sum;													\n";
	
	private final String vshaderHeader = "const lowp int GAUSSIAN_SAMPLES = 9;							\n"
									   + "uniform highp float texelWidthOffset;							\n"
									   + "uniform highp float texelHeightOffset;						\n"
									   + "uniform highp float blurSize;									\n"
									   + "varying highp vec2 blurCoordinates[GAUSSIAN_SAMPLES];			\n";
	
	private final String vshader = "int multiplier = 0;																	\n"
								 + "highp vec2 blurStep;																\n"
								 + "highp vec2 singleStepOffset = vec2(0.0, 0.001) * %.3f;								\n"
								 + "for (lowp int i = 0; i < GAUSSIAN_SAMPLES; i++) {									\n"
								 + "   multiplier = (i - ((GAUSSIAN_SAMPLES - 1) / 2));									\n"
								 + "   blurStep = float(multiplier) * singleStepOffset;									\n"
								 + "   blurCoordinates[i] = v_texCoord + blurStep;										\n"
 								 + "}																					\n";
	private float mBlurSize;
	
	public GaussianVerticalBlurFilter(Bitmap bitmap) {
		super(bitmap);
		mBlurSize = 2.0f;
	}
	
	public void setBlurSize(float blurSize)
	{
		mBlurSize = blurSize;
	}

	@Override
	public String getName() {
		return "blur";
	}

	@Override
	public String getFShader() {
		return fshader;
	}

	@Override
	public String getVShader()
	{
		return String.format(vshader, mBlurSize);
	}

	@Override
	public String getFShaderHeader() {
		return fshaderHeader;
	}

	@Override
	public String getVShaderHeader() {
		return vshaderHeader;
	}
	
}


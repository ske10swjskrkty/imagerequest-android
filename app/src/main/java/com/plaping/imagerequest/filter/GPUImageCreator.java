package com.plaping.imagerequest.filter;

import android.content.Context;
import android.graphics.Bitmap;

import java.util.List;


public class GPUImageCreator {

	private PixelBuffer mPixelBuffer;
	private GPUImageRenderer mRenderer;
	private Context mContext;
	private Bitmap mBaseImage;
	
	public GPUImageCreator(Context context, Bitmap baseImage, int width, int height)
	{
		mRenderer = new GPUImageRenderer(context);
		mRenderer.setBaseImage(baseImage);

		mContext = context;
		mBaseImage = baseImage;

		mPixelBuffer = new PixelBuffer(width, height);
	}
	
	public void setBaseImage(Bitmap baseImage)
	{
		mRenderer.setBaseImage(baseImage);
	}
	
	public List<Filter> getFilterList()
	{
		return mRenderer.getFilterList();
	}
	
	public Bitmap getOutput()
	{
		mPixelBuffer.setRenderer(mRenderer);
		//TimeSpenderLog.getInstance().appendKeyTime("setRenderer");
		return mPixelBuffer.getBitmap();
	}
	
}

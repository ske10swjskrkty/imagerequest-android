package com.plaping.imagerequest.filter.custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.plaping.imagerequest.R;
import com.plaping.imagerequest.filter.BaseFilter;
import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageLookupFilter;

public class Filter05 extends BaseFilter {

    public Filter05(Context context) {
        super(context);
    }

    @Override
    public String getName() {
        return "Filter #01";
    }

    @Override
    public Bitmap apply(Bitmap image) {
        GPUImageLookupFilter filter = new GPUImageLookupFilter();
        filter.setBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.clut_05));
        GPUImage gpuImage = new GPUImage(getContext());
        gpuImage.setFilter(filter);
        return gpuImage.getBitmapWithFilterApplied(image);
    }
}

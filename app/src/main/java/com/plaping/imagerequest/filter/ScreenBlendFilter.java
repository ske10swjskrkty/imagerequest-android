package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;

public class ScreenBlendFilter extends Filter {

	private final String fshader = "mediump vec4 textureColor2 = texture2D(layer_texture, v_texCoord);							\n"
								 + "mediump vec4 whiteColor = vec4(1.0);														\n"
								 + "textureColor = whiteColor - ((whiteColor - textureColor2) * (whiteColor - textureColor));	\n";
	
	public ScreenBlendFilter(Bitmap bitmap)
	{
		super(bitmap);
	}

	@Override
	public String getName()
	{
		return String.format("layer_texture%d", getUnique());
	}
	
	@Override
	public String getFShader()
	{
		String shader = fshader;
		shader = shader.replace("whiteColor", String.format("whiteColor%d", getUnique()));
		shader = shader.replace("textureColor2", String.format("textureColor%d_2", getUnique()));
		shader = shader.replace("layer_texture", getName());
		return shader;
	}

	@Override
	public String getVShader()
	{
		return "";
	}

	@Override
	public String getFShaderHeader() {
		return "";
	}

	@Override
	public String getVShaderHeader() {
		return "";
	}

}

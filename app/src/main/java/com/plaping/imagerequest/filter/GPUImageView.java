package com.plaping.imagerequest.filter;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import java.util.List;


public class GPUImageView extends GLSurfaceView {

	private Context mContext;
	private GPUImageRenderer mRenderer;
	
	public GPUImageView(Context context) {
		super(context);
		mContext = context;
		
		setEGLContextClientVersion(2);
		setRenderer(mRenderer = new GPUImageRenderer(mContext));
	}
	
	public GPUImageView(Context context, AttributeSet attr)
	{
		super(context, attr);
		mContext = context;
		
		setEGLContextClientVersion(2);
		setRenderer(mRenderer = new GPUImageRenderer(mContext));
	}
	
	public void setBaseImage(Bitmap baseImage)
	{
		mRenderer.setBaseImage(baseImage);
	}
	
	public List<Filter> getFilterList()
	{
		return mRenderer.getFilterList();
	}
	
	public GPUImageRenderer getRenderer()
	{
		return mRenderer;
	}
	
}

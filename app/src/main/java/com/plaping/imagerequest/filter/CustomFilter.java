package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;

public class CustomFilter extends Filter {

	public CustomFilter(Bitmap bitmap) {
		super(bitmap);
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public String getFShader() {
		return null;
	}

	@Override
	public String getVShader()
	{
		return "";
	}

	@Override
	public String getFShaderHeader() {
		return "";
	}

	@Override
	public String getVShaderHeader() {
		return "";
	}

}

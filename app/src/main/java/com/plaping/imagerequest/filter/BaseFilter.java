package com.plaping.imagerequest.filter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public abstract class BaseFilter {

    protected static Bitmap.Config GL_BITMAP_CONFIG = Bitmap.Config.ALPHA_8;

    protected Context context;

    public BaseFilter(Context context) {
        this.context = context;
    }

    protected Context getContext() {
        return this.context;
    }

    protected Bitmap loadBitmap(int resId, Bitmap.Config config)
    {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        options.inDither=false;                     //Disable Dithering mode
        options.inPurgeable=true;

        Bitmap loadedBitmap = BitmapFactory.decodeResource(getContext().getResources(), resId, options);

        return loadedBitmap;
    }

    public abstract String getName();
    public abstract Bitmap apply(Bitmap bitmap);
}

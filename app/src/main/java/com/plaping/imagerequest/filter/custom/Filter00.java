package com.plaping.imagerequest.filter.custom;

import android.content.Context;
import android.graphics.Bitmap;

import com.plaping.imagerequest.filter.BaseFilter;


public class Filter00 extends BaseFilter {

    public Filter00(Context context) {
        super(context);
    }

    @Override
    public String getName() {
        return "Filter #01";
    }

    @Override
    public Bitmap apply(Bitmap image) {
        return image.copy(Bitmap.Config.ARGB_8888, true);
    }
}

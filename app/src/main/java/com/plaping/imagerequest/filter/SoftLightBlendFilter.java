package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;

public class SoftLightBlendFilter extends Filter {

	public SoftLightBlendFilter(Bitmap bitmap) {
		super(bitmap);
	}

	private final String fshader = "mediump vec4 base = textureColor;																														\n"
								 + "mediump vec4 overlay = texture2D(layer_texture, v_texCoord);																							\n"
								 + "textureColor = base * (overlay.a * (base / base.a) + (2.0 * overlay * (1.0 - (base / base.a)))) + overlay * (1.0 - base.a) + base * (1.0 - overlay.a);	\n";
	
	@Override
	public String getName() {
		return String.format("layer_texture%d", getUnique());
	}

	@Override
	public String getFShader() {
		String shader = fshader;
		shader = shader.replace("base", String.format("base%d", getUnique()));
		shader = shader.replace("overlay", String.format("overlay%d", getUnique()));
		shader = shader.replace("layer_texture", getName());
		return shader;
	}

	@Override
	public String getVShader() {
		return "";
	}

	@Override
	public String getFShaderHeader() {
		return "";
	}

	@Override
	public String getVShaderHeader() {
		return "";
	}

}

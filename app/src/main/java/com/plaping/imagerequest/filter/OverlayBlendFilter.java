package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;

public class OverlayBlendFilter extends Filter {

	public OverlayBlendFilter(Bitmap bitmap) {
		super(bitmap);
	}

	private final String fshader = "mediump vec4 base = textureColor;																											\n"
								 + "mediump vec4 overlay = texture2D(layer_texture, v_texCoord);																				\n"
								 + "mediump float ra;																															\n"
								 + "if (2.0 * base.r < base.a) {																												\n"
								 + "    ra = 2.0 * overlay.r * base.r + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);												\n"
								 + "} else {																																	\n"
								 + "    ra = overlay.a * base.a - 2.0 * (base.a - base.r) * (overlay.a - overlay.r) + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);	\n"
								 + "}																																			\n"
								 + "mediump float ga;																															\n"
								 + "if (2.0 * base.g < base.a) {																												\n"
								 + "    ga = 2.0 * overlay.g * base.g + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);												\n"
								 + "} else {																																	\n"
								 + "    ga = overlay.a * base.a - 2.0 * (base.a - base.g) * (overlay.a - overlay.g) + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);	\n"
								 + "}																																			\n"
								 + "mediump float ba;																															\n"
								 + "if (2.0 * base.b < base.a) {																												\n"
								 + "    ba = 2.0 * overlay.b * base.b + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);												\n"
								 + "} else {																																	\n"
								 + "    ba = overlay.a * base.a - 2.0 * (base.a - base.b) * (overlay.a - overlay.b) + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);	\n"
								 + "}																																			\n"
								 + "textureColor = vec4(ra, ga, ba, 1.0);																										\n";
	
	@Override
	public String getName() {
		return String.format("layer_texture%d", getUnique());
	}

	@Override
	public String getFShader() {
		String shader = fshader;
		shader = shader.replace("base", String.format("base%d", getUnique()));
		shader = shader.replace("overlay", String.format("overlay%d", getUnique()));
		shader = shader.replace("ra = ", String.format("ra%d = ", getUnique()));
		shader = shader.replace("ga = ", String.format("ga%d = ", getUnique()));
		shader = shader.replace("ba = ", String.format("ba%d = ", getUnique()));
		shader = shader.replace("ra,", String.format("ra%d,", getUnique()));
		shader = shader.replace("ga,", String.format("ga%d,", getUnique()));
		shader = shader.replace("ba,", String.format("ba%d,", getUnique()));
		shader = shader.replace("mediump float ra", String.format("mediump float ra%d", getUnique()));
		shader = shader.replace("mediump float ga", String.format("mediump float ga%d", getUnique()));
		shader = shader.replace("mediump float ba", String.format("mediump float ba%d", getUnique()));
		shader = shader.replace("layer_texture", getName());
		return shader;
	}

	@Override
	public String getVShader() {
		return "";
	}

	@Override
	public String getFShaderHeader() {
		return "";
	}

	@Override
	public String getVShaderHeader() {
		return "";
	}

}

package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;

public class MultiplyBlendFilter extends Filter {

	private final String fshader = "mediump vec4 overlayer = texture2D(layer_texture, v_texCoord);														\n"
								 + "textureColor = overlayer * textureColor + overlayer * (1.0 - textureColor.a) + textureColor * (1.0 - overlayer.a);	\n";
	
	public MultiplyBlendFilter(Bitmap bitmap) {
		super(bitmap);
	}

	@Override
	public String getName() {
		return String.format("layer_texture%d", getUnique());
	}

	@Override
	public String getFShader() {
		String shader = fshader;
		shader = shader.replace("overlayer", String.format("overlayer%d", getUnique()));
		shader = shader.replace("layer_texture", getName());
		return shader;
	}

	@Override
	public String getVShader()
	{
		return "";
	}

	@Override
	public String getFShaderHeader() {
		return "";
	}

	@Override
	public String getVShaderHeader() {
		return "";
	}

}

package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;

public class ColorBurnBlendFilter extends Filter {

	private final String fshader = "mediump vec4 blendColor = texture2D(layer_texture, v_texCoord);	\n"
								 + "mediump vec4 whiteColor = vec4(1.0,1.0,1.0,1.0);				\n"
			 					 + "float burnR = 1.0 - (1.0 - textureColor.r) / blendColor.r;		\n"
								 + "float burnG = 1.0 - (1.0 - textureColor.g) / blendColor.g;		\n"
								 + "float burnB = 1.0 - (1.0 - textureColor.b) / blendColor.b;		\n"
								 + "float burnA = 1.0 - (1.0 - textureColor.a) / blendColor.a;		\n"
								 + "burnR = burnR < 0.0 ? 0.0 : burnR; 								\n"
								 + "burnR = burnR > 1.0 ? 1.0 : burnR; 								\n"
								 + "burnG = burnG < 0.0 ? 0.0 : burnG; 								\n"
								 + "burnG = burnG > 1.0 ? 1.0 : burnG; 								\n"
								 + "burnB = burnB < 0.0 ? 0.0 : burnB; 								\n"
								 + "burnB = burnB > 1.0 ? 1.0 : burnB; 								\n"
								 + "burnA = burnA < 0.0 ? 0.0 : burnA; 								\n"
								 + "burnA = burnA > 1.0 ? 1.0 : burnA; 								\n"
								 + "textureColor = vec4(burnR,burnG,burnB,burnA); 					\n";

	public ColorBurnBlendFilter(Bitmap bitmap) {
		super(bitmap);
	}
	
	@Override
	public String getName() {
		return String.format("layer_texture%d", getUnique());
	}
	
	@Override
	public String getFShader() {
		String shader = fshader;
		shader = shader.replace("whiteColor", String.format("whiteColor%d", getUnique()));
		shader = shader.replace("blendColor", String.format("blendColor%d", getUnique()));
		shader = shader.replace("burnR", String.format("burnR%d", getUnique()));
		shader = shader.replace("burnG", String.format("burnG%d", getUnique()));
		shader = shader.replace("burnB", String.format("burnB%d", getUnique()));
		shader = shader.replace("burnA", String.format("burnA%d", getUnique()));
		shader = shader.replace("layer_texture", getName());
		return shader;
	}

	@Override
	public String getVShader()
	{
		return "";
	}

	@Override
	public String getFShaderHeader() {
		return "";
	}

	@Override
	public String getVShaderHeader() {
		return "";
	}

}

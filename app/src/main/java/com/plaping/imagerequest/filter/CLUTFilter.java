package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;

public class CLUTFilter extends Filter {

	private final String fshader = "mediump float blueColor = textureColor.b * 63.0; 									\n"
								 + "mediump vec2 quad1;																	\n"
								 + "quad1.y = floor(floor(blueColor) / 8.0);											\n"
								 + "quad1.x = floor(blueColor) - (quad1.y * 8.0);										\n" 
								 + "mediump vec2 quad2; 																\n"
								 + "quad2.y = floor(ceil(blueColor) / 8.0); 											\n"
								 + "quad2.x = ceil(blueColor) - (quad2.y * 8.0); 										\n" 
								 + "highp vec2 texPos1; 																\n"
								 + "texPos1.x = (quad1.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.r); \n"
								 + "texPos1.y = (quad1.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.g); \n"  
								 + "highp vec2 texPos2; 																\n"
								 + "texPos2.x = (quad2.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.r); \n"
								 + "texPos2.y = (quad2.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * textureColor.g); \n" 
								 + "mediump vec4 newColor1 = texture2D(lut_texture, texPos1); 							\n"
								 + "mediump vec4 newColor2 = texture2D(lut_texture, texPos2); 							\n"  
								 + "mediump vec4 newColor = mix(newColor1, newColor2, fract(blueColor)); 				\n"
								 + "textureColor = vec4(newColor2.rgb, textureColor.w); 								\n";
								 ;
	public CLUTFilter(Bitmap bitmap)
	{
		super(bitmap);
	}
	
	@Override
	public String getName()
	{
		return String.format("lut_texture%d", getUnique());
	}

	@Override
	public String getFShader()
	{
		String shader = fshader;
		shader = shader.replace("blueColor", String.format("blueColor%d", getUnique()));
		shader = shader.replace("quad1", String.format("quad%d_1", getUnique()));
		shader = shader.replace("quad2", String.format("quad%d_2", getUnique()));
		shader = shader.replace("texPos1", String.format("texPos%d_1", getUnique()));
		shader = shader.replace("texPos2", String.format("texPos%d_2", getUnique()));
		shader = shader.replace("newColor1", String.format("newColor%d_1", getUnique()));
		shader = shader.replace("newColor2", String.format("newColor%d_2", getUnique()));
		shader = shader.replace("newColor", String.format("newColor%d", getUnique()));
		shader = shader.replace("lut_texture", getName());
		return shader;
	}

	@Override
	public String getVShader()
	{
		return "";
	}

	@Override
	public String getFShaderHeader() {
		return "";
	}

	@Override
	public String getVShaderHeader() {
		return "";
	}

}

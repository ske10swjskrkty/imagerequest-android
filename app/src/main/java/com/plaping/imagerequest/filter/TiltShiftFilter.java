package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;

public class TiltShiftFilter extends Filter {

	private final String fshaderHeader = "uniform highp float topFocusLevel;						\n"
									   + "uniform highp float bottomFocusLevel;						\n"
									   + "uniform highp float focusFallOffRate;						\n";
	
	private final String fshader = "mediump vec4 blurredImageColor = texture2D(layer_texture, v_texCoord);												\n"
								 + "lowp float blurIntensity = 1.0 - smoothstep(%.3f - %.3f, %.3f, v_texCoord.y);										\n"
								 + "blurIntensity += smoothstep(%.3f, %.3f + %.3f, v_texCoord.y);														\n"
								 + "textureColor = mix(textureColor, blurredImageColor, blurIntensity);													\n";

	private float mTopLevel;
	private float mBottomLevel;
	private float mFallOffRate;
	
	public TiltShiftFilter(Bitmap bitmap) {
		super(bitmap);
		mTopLevel = 0.4f;
		mBottomLevel = 0.6f;
		mFallOffRate = 0.2f;
	}
	
	public void setTopLevel(float topLevel)
	{
		mTopLevel = topLevel;
	}
	
	public void setBottomLevel(float bottomLevel)
	{
		mBottomLevel = bottomLevel;
	}
	
	public void setFallOffRate(float fallOffRate)
	{
		mFallOffRate = fallOffRate;
	}
	
	@Override
	public String getName() {
		return "layer_texture"; //String.format("layer_texture%d", getUnique());
	}
	
	@Override
	public String getFShader() {
		return String.format(fshader, mTopLevel, mFallOffRate, mTopLevel, mBottomLevel, mBottomLevel, mFallOffRate);
	}

	@Override
	public String getVShader()
	{
		return "";
	}

	@Override
	public String getFShaderHeader() {
		return fshaderHeader;
	}

	@Override
	public String getVShaderHeader() {
		return "";
	}

}

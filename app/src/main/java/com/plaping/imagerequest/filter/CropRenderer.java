package com.plaping.imagerequest.filter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class CropRenderer implements GLSurfaceView.Renderer {
	
	Context context;
	// Shader Program
	private int mProgramObject;
    
    // Attribute locations
    private int mPositionLoc;
    private int mTexCoordLoc;
    
    // Sampler location
    private int mSamplerLoc;
    
    // Texture handle
    private int[] mTextureId = new int[1];
    
    private Bitmap mBitmap;
    private Rect mCropRect;
    private int mRotation;
    private int mBgColor;
    
    // Additional member variables
    private int mWidth;
    private int mHeight;
    private FloatBuffer mVertices;
    private ShortBuffer mIndices;

	
    private float[] mVerticesData =
    { 
            -1.0f, 1.0f, 0.0f, // Position 0
            0.0f, 1.0f, // TexCoord 0
            -1.0f, -1.0f, 0.0f, // Position 1
            0.0f, 0.0f, // TexCoord 1
            1.0f, -1.0f, 0.0f, // Position 2
            1.0f, 0.0f, // TexCoord 2
            1.0f, 1.0f, 0.0f, // Position 3
            1.0f, 1.0f, // TexCoord 3
    };

    private short[] mIndicesData =
    { 
            3, 2, 0, 2, 1, 0 
    };
    
	public CropRenderer(Context context)
	{
		this.context = context;
		
		mVertices = ByteBuffer.allocateDirect(mVerticesData.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mVertices.put(mVerticesData).position(0);
        mIndices = ByteBuffer.allocateDirect(mIndicesData.length * 2)
                .order(ByteOrder.nativeOrder()).asShortBuffer();
        mIndices.put(mIndicesData).position(0);
	}
	
	public void destroy()
	{
		GLES20.glDeleteTextures(1, mTextureId, 0);
		if (mBitmap != null)
		{
			mBitmap.recycle();
			mBitmap = null;
		}
	}
	
	public void setBitmap(Bitmap bitmap, Rect cropRect, int rotation, int bgColor)
	{
		destroy();
		
		mBitmap = bitmap;
		mCropRect = cropRect;
		mRotation = rotation;
		mBgColor = bgColor;
		
		while (mRotation < 0)
			mRotation += 360;
		
		mRotation = mRotation % 360;
	}
	
	public void initialize()
	{
		if (mBitmap == null)
			return;
		
		Log.d("LAB", "Initializing");

		// Generate texture from bitmap
		
        mVerticesData[0  + 3] = (float)(mCropRect.left + 0.0) / mBitmap.getWidth();
        mVerticesData[5  + 3] = (float)(mCropRect.left + 0.0) / mBitmap.getWidth();
        mVerticesData[10 + 3] = (float)(mCropRect.right + 0.0) / mBitmap.getWidth();
        mVerticesData[15 + 3] = (float)(mCropRect.right + 0.0) / mBitmap.getWidth();

        mVerticesData[0  + 4] = (float)(mCropRect.bottom + 0.0) / mBitmap.getHeight();
        mVerticesData[5  + 4] = (float)(mCropRect.top + 0.0) / mBitmap.getHeight();
        mVerticesData[10 + 4] = (float)(mCropRect.top + 0.0) / mBitmap.getHeight();
        mVerticesData[15 + 4] = (float)(mCropRect.bottom + 0.0) / mBitmap.getHeight();
        
        for (int i = 0; i < 4; i++)
        {
	        if (mVerticesData[i * 5 + 3] < 0)
	        	mVerticesData[i * 5 + 3] = 0;
	        else if (mVerticesData[i * 5 + 3] > 1)
	        	mVerticesData[i * 5 + 3] = 1;

	        if (mVerticesData[i * 5 + 4] < 0)
	        	mVerticesData[i * 5 + 4] = 0;
	        else if (mVerticesData[i * 5 + 4] > 1)
	        	mVerticesData[i * 5 + 4] = 1;
        }
        
    	float left   = (0.0f - mCropRect.left) * 2 / mCropRect.width() - 1.0f;
    	if (mCropRect.left >= 0)
    		left = -1;
    	mVerticesData[0] = mVerticesData[5] = left;
    	float right  = 1.0f - (mCropRect.right - mBitmap.getWidth()) * 2.0f / mCropRect.width();
    	if (mCropRect.right <= mBitmap.getWidth())
    		right = 1;
    	mVerticesData[10] = mVerticesData[15] = right;
    	float top    = (0.0f - mCropRect.top) * 2 / mCropRect.height() - 1.0f;
    	if (mCropRect.top >= 0)
    		top = -1;
    	mVerticesData[6] = mVerticesData[11] = top;
    	float bottom = 1.0f - (mCropRect.bottom - mBitmap.getHeight()) * 2.0f / mCropRect.height();
    	if (mCropRect.bottom <= mBitmap.getHeight())
    		bottom = 1;
    	mVerticesData[1] = mVerticesData[16] = bottom;

        float tmp = 0;
        int loopCount = 0;
        switch (mRotation)
        {
	        case 0:
	        	loopCount = 0;
	        	break;
	        case 90:
	        	loopCount = 3;
	        	break;
	        case 180:
	        	loopCount = 2;
	        	break;
	        case 270:
	        	loopCount = 1;
	        	break;
        }
        
        for (int i = 0; i < loopCount; i++)
        {
            // Rotate
            tmp = mVerticesData[0 + 3];
            mVerticesData[0  + 3] = mVerticesData[5  + 3];
            mVerticesData[5  + 3] = mVerticesData[10 + 3];
            mVerticesData[10 + 3] = mVerticesData[15 + 3];
            mVerticesData[15 + 3] = tmp;

            tmp = mVerticesData[0 + 4];
            mVerticesData[0  + 4] = mVerticesData[5  + 4];
            mVerticesData[5  + 4] = mVerticesData[10 + 4];
            mVerticesData[10 + 4] = mVerticesData[15 + 4];
            mVerticesData[15 + 4] = tmp;        	
        }
        
        mVertices = ByteBuffer.allocateDirect(mVerticesData.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mVertices.put(mVerticesData).position(0);
        
        // Bind to the texture in OpenGL
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureId[0]);
        //filter.setTextureId(mTextureId);
 
        // Set filtering
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, 
        		GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, 
                GLES20.GL_CLAMP_TO_EDGE);

        // Load the bitmap into the bound texture.
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, mBitmap, 0);
        
        // Recycle the bitmap, since its data has been loaded into OpenGL.
        mBitmap.recycle();
        
        mBitmap = null;
	}

    protected Bitmap loadBitmap(int resId, Bitmap.Config config)
    {
    	Point size = getBitmapResolution(resId);
    	
    	int mul = 1;
    	int minSize = Math.min(size.x, size.y);
    	while (minSize > 800 * 2)
    	{
    		minSize = minSize >> 1;
    		mul = mul << 1;
    	}
    	
    	mul = 1;
    	
    	final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        options.inPreferredConfig = config;
        //options.inSampleSize = mul;
        
        // Read in the resource
        return BitmapFactory.decodeResource(context.getResources(), resId, options);
    }
    
    protected Point getBitmapResolution(int resId)
    {
    	final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), resId, options);
        return new Point(options.outWidth, options.outHeight);
    }
	
	public void onDrawFrame(GL10 gl) 
	{
		// Set the viewport
        GLES20.glViewport(0, 0, mWidth, mHeight);

        // Clear the color buffer
        if (mBgColor == Color.WHITE)
            GLES20.glClearColor(1, 1, 1, 1);
        else
        	GLES20.glClearColor(0, 0, 0, 1);
        
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        // Use the program object
        GLES20.glUseProgram(mProgramObject);

        // Load the vertex position
        mVertices.position(0);
        GLES20.glVertexAttribPointer ( mPositionLoc, 3, GLES20.GL_FLOAT, 
                                       false, 
                                       5 * 4, mVertices );
        // Load the texture coordinate
        mVertices.position(3);
        GLES20.glVertexAttribPointer ( mTexCoordLoc, 2, GLES20.GL_FLOAT,
                                       false, 
                                       5 * 4, 
                                       mVertices );
               
        GLES20.glEnableVertexAttribArray ( mPositionLoc );
        GLES20.glEnableVertexAttribArray ( mTexCoordLoc );
       
        // Bind the texture
        GLES20.glActiveTexture (GLES20.GL_TEXTURE0);
        GLES20.glBindTexture (GLES20.GL_TEXTURE_2D, mTextureId[0]);
        GLES20.glUniform1i (mSamplerLoc, 0);
        
        GLES20.glDrawElements ( GLES20.GL_TRIANGLES, 6, GLES20.GL_UNSIGNED_SHORT, mIndices);
	}

	public void onSurfaceChanged(GL10 gl, int width, int height) 
	{
		//gl.glViewport(0, 0, width, height);
		mWidth = width;
		mHeight = height;
	}

	public void onSurfaceCreated(GL10 gl, EGLConfig config) 
	{
		StringBuffer vHeader = new StringBuffer();
		StringBuffer fHeader = new StringBuffer();
		StringBuffer sampler = new StringBuffer();
		StringBuffer vscript = new StringBuffer();
		StringBuffer fscript = new StringBuffer();

		String vShaderStr = ""
				  + "attribute vec4 a_position;   										\n"
				  + "attribute vec2 a_texCoord;   										\n"
				  + "%s																	\n"	
				  + "varying vec2 v_texCoord;     										\n"
				  + "void main()                  										\n"
				  + "{                            										\n"
				  + "   gl_Position = a_position; 										\n"
				  + "   v_texCoord = a_texCoord;  										\n"
				  + "%s																	\n"
				  + "}                            										\n";

		String fShaderStr = ""
	  			  + "#ifdef GL_FRAGMENT_PRECISION_HIGH									\n"
				  + "precision highp float;												\n"
				  + "#else																\n"
				  + "precision mediump float;											\n"
				  + "#endif  															\n"
				  + "varying vec2 v_texCoord;											\n"
				  + "uniform sampler2D s_texture;										\n"
				  + "%s																	\n"
				  + "%s																	\n"	
				  + "void main()														\n"
				  + "{																	\n"
				  + "mediump vec4 textureColor = texture2D( s_texture, v_texCoord );    \n"
				  + "%s																	\n"
				  + "gl_FragColor = textureColor;										\n"
				  + "}                                                   				\n";
		
		vShaderStr = String.format(vShaderStr, vHeader, vscript);
		fShaderStr = String.format(fShaderStr, fHeader, sampler, fscript);
		//System.out.println(fShaderStr);
		// Load the shaders and get a linked program object
		mProgramObject = ESShader.loadProgram(vShaderStr, fShaderStr);
		
		// Get the attribute locations
		mPositionLoc = GLES20.glGetAttribLocation(mProgramObject, "a_position");
		mTexCoordLoc = GLES20.glGetAttribLocation(mProgramObject, "a_texCoord" );
		
		// Get the sampler location
		mSamplerLoc = GLES20.glGetUniformLocation ( mProgramObject, "s_texture" );
		
		initialize();
	}

}

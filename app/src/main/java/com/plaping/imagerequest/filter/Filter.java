package com.plaping.imagerequest.filter;

import android.graphics.Bitmap;
import android.opengl.GLES20;

import java.util.Random;

public abstract class Filter {

	private int mTextureId;
	private int mTexLocId;
	private int mUnique;
	private Bitmap mBitmap;
	
	public Filter(Bitmap bitmap)
	{
		mBitmap = bitmap;
		mUnique = new Random().nextInt(1000000000);
	}
	
	public abstract String getName();
	public abstract String getFShader();
	public abstract String getFShaderHeader();
	public abstract String getVShader();
	public abstract String getVShaderHeader();
	
	public void setBitmap(Bitmap bitmap)
	{
		mBitmap = bitmap;
	}
	
	public Bitmap getBitmap()
	{
		return mBitmap;
	}
	
	public void setTextureId(int textureId)
	{
		mTextureId = textureId;
	}
	
	public int getTextureId()
	{
		return mTextureId;
	}
	
	public void setProgram(int program)
	{
		mTexLocId = GLES20.glGetUniformLocation(program, getName());
	}
	
	public int getTexLocId()
	{
		return mTexLocId;
	}
	
	protected int getUnique()
	{
		return mUnique;
	}
	
}

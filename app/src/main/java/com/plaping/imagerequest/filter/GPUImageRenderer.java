package com.plaping.imagerequest.filter;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.util.Log;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GPUImageRenderer implements GLSurfaceView.Renderer {

	private Context mContext;
	
	// Shader Program
	private int mProgramObject;
    
    // Attribute locations
    private int mPositionLoc;
    private int mTexCoordLoc;
    
    // Sampler location
    private int mSamplerLoc;
    
    // Texture handle
    private int mTextureId;
    
    // Additional member variables
    private int mWidth;
    private int mHeight;
    private FloatBuffer mVertices;
    private ShortBuffer mIndices;
    
    private GL10 mGl;
    
//    private String mFilterStrength = "1.0";
    
    private final float[] mVerticesData =
    { 
            -1.0f, 1.0f, 0.0f, // Position 0
            0.0f, 1.0f, // TexCoord 0
            -1.0f, -1.0f, 0.0f, // Position 1
            0.0f, 0.0f, // TexCoord 1
            1.0f, -1.0f, 0.0f, // Position 2
            1.0f, 0.0f, // TexCoord 2
            1.0f, 1.0f, 0.0f, // Position 3
            1.0f, 1.0f, // TexCoord 3
    };

    private final short[] mIndicesData =
    { 
            3, 2, 0, 2, 1, 0 
    };
    
    private int[] mTextureIds;
    private ArrayList<Filter> mFilterList;
    private Bitmap mBaseImage;
	
	protected GPUImageRenderer(Context context)
	{
		mContext = context;
		
		mVertices = ByteBuffer.allocateDirect(mVerticesData.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mVertices.put(mVerticesData).position(0);
        mIndices = ByteBuffer.allocateDirect(mIndicesData.length * 2)
                .order(ByteOrder.nativeOrder()).asShortBuffer();
        mIndices.put(mIndicesData).position(0);
		
		Class<GLES20> gles20Class= GLES20.class;
		
		mFilterList = new ArrayList<Filter>();
		
		mTextureIds = new int[32];
		for (int i=0; i<mTextureIds.length; i++)
		{
			try {
				Field textureField = gles20Class.getField("GL_TEXTURE"+i);
				mTextureIds[i] = textureField.getInt(null);
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean mDebugging = false;
	public void setDebugging(boolean debugging)
	{
		mDebugging = debugging;
	}
	
	@Override
	public void onDrawFrame(GL10 glUnused) {
		mGl = glUnused;
		if (mDebugging)
			Log.d("POND2","onDrawFrame " + hashCode());
		
		// Set the viewport
        GLES20.glViewport(0, 0, mWidth, mHeight);

        // Clear the color buffer
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        // Use the program object
        GLES20.glUseProgram(mProgramObject);

        // Load the vertex position
        mVertices.position(0);
        GLES20.glVertexAttribPointer ( mPositionLoc, 3, GLES20.GL_FLOAT, 
                                       false, 
                                       5 * 4, mVertices );
        // Load the texture coordinate
        mVertices.position(3);
        GLES20.glVertexAttribPointer ( mTexCoordLoc, 2, GLES20.GL_FLOAT,
                                       false, 
                                       5 * 4, 
                                       mVertices );
               
        GLES20.glEnableVertexAttribArray ( mPositionLoc );
        GLES20.glEnableVertexAttribArray ( mTexCoordLoc );
       
        // Bind the texture
        GLES20.glActiveTexture (GLES20.GL_TEXTURE0);
        GLES20.glBindTexture (GLES20.GL_TEXTURE_2D, mTextureId);
        GLES20.glUniform1i (mSamplerLoc, 0);
        
        int filterCount = mFilterList.size();
        for (int i=1; i<=filterCount; i++)
        {
        	Filter filter = mFilterList.get(i-1);
	        GLES20.glActiveTexture (mTextureIds[i]);
	        GLES20.glBindTexture (GLES20.GL_TEXTURE_2D, filter.getTextureId());
	        GLES20.glUniform1i (filter.getTexLocId(), i);
        }
        
//        GLES20.glActiveTexture (mTextureId);
//        GLES20.glBindTexture (GLES20.GL_TEXTURE_2D, mTextureId);
//        GLES20.glUniform1i (mSamplerLoc, 0);
        
        GLES20.glDrawElements ( GLES20.GL_TRIANGLES, 6, GLES20.GL_UNSIGNED_SHORT, mIndices);
	}

	@Override
	public void onSurfaceChanged(GL10 glUnused, int width, int height) {
		mWidth = width;
        mHeight = height;
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig arg1) {
		if (mDebugging)
			Log.d("POND2", "onSurfaceCreated: " + hashCode());
		
		long start = System.currentTimeMillis();
		
		StringBuffer vHeader = new StringBuffer();
		StringBuffer fHeader = new StringBuffer();
		StringBuffer sampler = new StringBuffer();
		StringBuffer vscript = new StringBuffer();
		StringBuffer fscript = new StringBuffer();
		
		final int filterCount = mFilterList.size();
		for (int i=0; i<filterCount; i++)
		{
			  Filter filter = mFilterList.get(i);
			  sampler.append(String.format("uniform sampler2D %s;\n", filter.getName()));
			  vscript.append(filter.getVShader());
			  fscript.append(filter.getFShader());
			  fHeader.append(filter.getFShaderHeader());
			  vHeader.append(filter.getVShaderHeader());
		}
	      
		String vShaderStr = ""
						  + "attribute vec4 a_position;   										\n"
						  + "attribute vec2 a_texCoord;   										\n"
						  + "%s																	\n"	
						  + "varying vec2 v_texCoord;     										\n"
						  + "void main()                  										\n"
						  + "{                            										\n"
						  + "   gl_Position = a_position; 										\n"
						  + "   v_texCoord = a_texCoord;  										\n"
						  + "%s																	\n"
						  + "}                            										\n";
		
		String fShaderStr = ""
    		  			  + "#ifdef GL_FRAGMENT_PRECISION_HIGH									\n"
						  + "precision highp float;												\n"
						  + "#else																\n"
						  + "precision mediump float;											\n"
						  + "#endif  															\n"
						  + "varying vec2 v_texCoord;											\n"
						  + "uniform sampler2D s_texture;										\n"
						  + "%s																	\n"
						  + "%s																	\n"	
						  + "void main()														\n"
						  + "{																	\n"
						  + "mediump vec4 textureColor = texture2D( s_texture, v_texCoord );    \n"
						  + "%s																	\n"
						  + "gl_FragColor = textureColor;										\n"
//						  + "gl_FragColor = vec4(textureColor.x,textureColor.y,textureColor.z,%s);\n"
						  + "}                                                   				\n";

	  vShaderStr = String.format(vShaderStr, vHeader, vscript);
    fShaderStr = String.format(fShaderStr, fHeader, sampler, fscript);
//      fShaderStr = String.format(fShaderStr, fHeader, sampler, fscript, mFilterStrength);
      //System.out.println(fShaderStr);
      // Load the shaders and get a linked program object
      mProgramObject = ESShader.loadProgram(vShaderStr, fShaderStr);

      // Get the attribute locations
      mPositionLoc = GLES20.glGetAttribLocation(mProgramObject, "a_position");
      mTexCoordLoc = GLES20.glGetAttribLocation(mProgramObject, "a_texCoord" );
      
      // Get the sampler location
      mSamplerLoc = GLES20.glGetUniformLocation ( mProgramObject, "s_texture" );
      
      for (int i=0; i<filterCount; i++)
      {
    	  Filter filter = mFilterList.get(i);
    	  filter.setProgram(mProgramObject);
      }
      
      // Load the texture
      createSimpleTexture2D();

//      GLES20.glDisable(GLES20.GL_CULL_FACE);
//      
//      // No depth testing
//      GLES20.glDisable(GLES20.GL_DEPTH_TEST);
//      GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
      
//      Log.d("POND2", "init time = " + (System.currentTimeMillis() - start));
      
      GLES20.glEnable(GLES20.GL_TEXTURE_2D);
      GLES20.glEnable(GLES20.GL_BLEND);
      GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA,GLES20.GL_ONE_MINUS_SRC_ALPHA); //I've seen this on most tutorials
      GLES20.glDisable(GLES20.GL_DEPTH_TEST); //it's a 2D game
	}
	
	public void setBaseImage(Bitmap baseImage)
	{
		mBaseImage = baseImage;
	}
	
	public Bitmap getBaseImage()
	{
		return mBaseImage;
	}
	
	public Bitmap getResultImage()
	{
		return null;
	}
	
	public ArrayList<Filter> getFilterList()
	{
		return mFilterList;
	}
	
	//
    // Create a simple 2x2 texture image with four different colors
    //
    private int[] createSimpleTexture2D()
    {    
    	final int filterCount = mFilterList.size();
    	final int[] textureHandle = new int[filterCount+1];
        
        GLES20.glGenTextures(filterCount+1, textureHandle, 0);
     
        if (textureHandle[0] != 0)
        {
        	{
	            // Bind to the texture in OpenGL
	            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);
	     
	            // Set filtering
	            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
	            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
	            
	            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, 
	            		GLES20.GL_CLAMP_TO_EDGE);
	            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, 
	                    GLES20.GL_CLAMP_TO_EDGE);
	
//	            Log.d("POND2", "emma = " + mBaseImage);
	            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, mBaseImage, 0);
	            
	            mTextureId = textureHandle[0];
        	}
        	
        	for (int i=1; i<=filterCount; i++)
        	{
        		Filter filter = mFilterList.get(i-1);
	     
	            // Read in the resource
	            final Bitmap bitmap = filter.getBitmap();
	            
	            // Bind to the texture in OpenGL
	            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[i]);
	            filter.setTextureId(textureHandle[i]);
	     
	            // Set filtering
	            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
	            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
	            
	            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, 
	            		GLES20.GL_CLAMP_TO_EDGE);
	            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, 
	                    GLES20.GL_CLAMP_TO_EDGE);
	
	            // Load the bitmap into the bound texture.
	            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
	     
	            // Recycle the bitmap, since its data has been loaded into OpenGL.
	            bitmap.recycle();
	        }
        }
     
        if (textureHandle[0] == 0)
        {
            throw new RuntimeException("Error loading texture.");
        }
     
        return textureHandle;
    }
    
    public GL10 getGL()
    {
    	return mGl;
    }

//	public void setmFilterStrength(String mFilterStrength) {
//		this.mFilterStrength = mFilterStrength;
//	}

}

package com.plaping.imagerequest.api.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;

/* Fix this too! */
public class JsonDtoConverter {

    public static <T> T convert(JSONObject aJson, Class<T> aDataClass)
    {
        T dataDto = null;

        try {
            dataDto = (T)aDataClass.newInstance();
            Field[] fields = aDataClass.getDeclaredFields();
            for(Field field : fields)
            {
                if(field.getAnnotations().length == 0)
                    continue;

                Annotation annotation = field.getAnnotations()[0];
                //System.out.println(field.getName()+": "+field.getAnnotations()[0].annotationType()+": ");

                Class<? extends Annotation> annotationClass = field.getAnnotations()[0].annotationType();
                try {

                    Method method = null;
                    if(annotationClass == JSONArrayValue.class)
                    {
                        try
                        {
                            method = annotationClass.getMethod("field", null);
                            String key = (String)method.invoke(annotation, null);
                            JSONArray jsonArray = aJson.getJSONArray(key);
                            final int jsonArraySize = jsonArray.length();

                            Class<? extends Object> fieldType = field.getType().getComponentType();
                            Object fieldArray = Array.newInstance(fieldType, jsonArray.length());

                            for(int i=0;i<jsonArraySize;i++)
                            {
                                Object child = jsonArray.get(i);
                                if(child instanceof JSONArray)
                                {

                                }
                                else
                                {
                                    setArrayData(fieldArray, i, jsonArray);
                                }
                            }

//							//System.out.println(fieldArray);
//							String fieldName = field.getName();
//							String setterMethodName = String.format("set%s%s",
//									(fieldName.charAt(0)+"").toUpperCase(),
//									fieldName.substring(1));
//							Method setterMethod = dataClass.getMethod(setterMethodName,fieldArray.getClass());
//							setterMethod.invoke(dataDto, fieldArray);

                            AccessibleObject.setAccessible(new AccessibleObject[]{field}, true);
                            field.set(dataDto, fieldArray);
                            AccessibleObject.setAccessible(new AccessibleObject[]{field}, false);

                        }catch(JSONException jse)
                        {

                        }
                    }
                    else if(annotationClass == JSONMultipleValue.class)
                    {
                        method = annotationClass.getMethod("fields", null);
                        String[] keys = (String[])method.invoke(annotation, null);

                        Class fieldType = field.getType();
                        Class memberType = fieldType.getComponentType();
                        Object fieldArray = Array.newInstance(memberType, keys.length);

                        for(int i=0;i<keys.length;i++)
                        {
                            Object data = createData(memberType, aJson, keys[i]);
                            if(data != null)
                            {
                                Array.set(fieldArray, i, data);
                            }
                        }

//						String fieldName = field.getName();
//						String setterMethodName = String.format("set%s%s",
//								(fieldName.charAt(0)+"").toUpperCase(),
//								fieldName.substring(1));
//						//System.out.println("$$%^^^"+field.getType());
//						Method setterMethod = dataClass.getMethod(setterMethodName,field.getType());
//						setterMethod.invoke(dataDto, fieldArray);

                        AccessibleObject.setAccessible(new AccessibleObject[]{field}, true);
                        field.set(dataDto, fieldArray);
                        AccessibleObject.setAccessible(new AccessibleObject[]{field}, false);
                    }
                    else if(annotationClass == JSONValue.class)
                    {



                        method = annotationClass.getMethod("field", null);
                        String key = (String)method.invoke(annotation, null);

                        //if (key.equals("welcome_gift")) {
                        //Log.d("", "key = "+ key);
                        //Log.d("", "field = "+ field);
                        //Log.d("", "dataDto" + dataDto);
                        //}

                        fillData(field,aJson,key,dataDto);
                    }

                } catch (SecurityException e) {
                    //if(AppConstants.ENABLE_COMMON_LOG)
                    //e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    //if(AppConstants.ENABLE_COMMON_LOG)
                    //e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    //if(AppConstants.ENABLE_COMMON_LOG)
                    //e.printStackTrace();
                } catch (InvocationTargetException e) {
                    //if(AppConstants.ENABLE_COMMON_LOG)
                    //e.printStackTrace();
                }

            }

//			Annotation[] annotations = dataClass.getAnnotations();
//			//System.out.println(annotations.length);
//			for(Annotation annotation : annotations){
//				//System.out.println("*"+annotation.annotationType());
//			}


        } catch (InstantiationException e) {
            //if(AppConstants.ENABLE_COMMON_LOG)
            //e.printStackTrace();
        } catch (IllegalAccessException e) {
            //if(AppConstants.ENABLE_COMMON_LOG)
            //e.printStackTrace();
        }

        if (dataDto != null && aJson != null && dataDto instanceof DataDTO)
        {
            //((DataDTO)dataDto).setOriginalJSON(aJson.toString());
        }

        return dataDto;
    }

    private static <T> void setArrayData(Object aArray, int aIndex, JSONArray aJsonArray)
    {
        Class<? extends Object> type = aArray.getClass().getComponentType();
        String typeName = type.toString();
        //System.out.println("------>"+typeName);
        try {

            if(int.class.equals(type))
            {
                //System.out.println("------>"+aJsonArray.get(aIndex));
                Array.setInt(aArray, aIndex, aJsonArray.getInt(aIndex));
            }
            else if(long.class.equals(type))
            {
                Array.setLong(aArray, aIndex, aJsonArray.getLong(aIndex));
            }
            else if(double.class.equals(type))
            {
                Array.setDouble(aArray, aIndex, aJsonArray.getDouble(aIndex));
            }
            else if(boolean.class.equals(type))
            {
                Array.setBoolean(aArray, aIndex, aJsonArray.getBoolean(aIndex));
            }
            else if(String.class.equals(type))
            {
                Array.set(aArray, aIndex, aJsonArray.getString(aIndex));
            }
            else if(DataDTO.class.equals(type.getSuperclass()))
            {
                //System.out.println("$$$$$"+type+","+aJsonArray.getJSONObject(aIndex));
                JSONObject json = aJsonArray.getJSONObject(aIndex);
                Object dataDto = convert(json, type);
                Array.set(aArray, aIndex, dataDto);
                //System.out.println(dataDto);
            }
            else
            {
                Array.set(aArray, aIndex, new Object());// jsonArray.get(index));
            }

        } catch (IllegalArgumentException e) {
            //if(AppConstants.ENABLE_COMMON_LOG)
            //e.printStackTrace();
        } catch (JSONException e) {
            //if(AppConstants.ENABLE_COMMON_LOG)
            //e.printStackTrace();
        } catch (SecurityException e) {
            //if(AppConstants.ENABLE_COMMON_LOG)
            //e.printStackTrace();
        }
    }

    private static <T> void fillData(Field aField, JSONObject aJson, String key, T aDataDto)
    {
        Class type = aField.getType();
        String typeName = type.toString();

        try {
            if(key.indexOf(".") != -1)
            {
                String[] subkeys = key.split("\\.");
                //System.out.println("!!!!!!!!"+key+","+subkeys.length);
                for(int i=0;i<subkeys.length-1;i++)
                {
                    aJson = aJson.optJSONObject(subkeys[i]);
                    //System.out.println("!!!!!!!!"+subkeys[i]);

                    if(aJson == null){
                        //System.out.println("NULLNULLNULL");
                        return;
                    }
                }

                key = subkeys[subkeys.length-1];
            }


            String fieldName = aField.getName();
            String setterMethodName = String.format("set%s%s",
                    (fieldName.charAt(0)+"").toUpperCase(),
                    fieldName.substring(1));

            Method setterMethod = null;
            Class<? extends Object> dataClass = aDataDto.getClass();
            Class<? extends Object> paramClass = null;
            boolean isDataDTO = false;
            //System.out.println("****"+aDataDto.getClass()+"::"+type);

            if(aField.getType() == java.util.Date.class)
            {
//				setterMethod = dataClass.getMethod(setterMethodName, type);
//				long timestamp = json.optLong(key)*1000;
//				setterMethod.invoke(dataDto, new Date(timestamp));

                long timestamp = aJson.optLong(key)*1000;
                AccessibleObject.setAccessible(new AccessibleObject[]{aField}, true);
                aField.set(aDataDto, new Date(timestamp));
                AccessibleObject.setAccessible(new AccessibleObject[]{aField}, false);
            }
            else if(DataDTO.class.equals(type.getSuperclass()))
            {
//				setterMethod = dataClass.getMethod(setterMethodName, type);
//				if (key.equals("welcome_gift")) {
//					System.out.println("$$$$$"+type+","+aJson.optJSONObject(key));
//				}

//				setterMethod.invoke(dataDto, convert(json.optJSONObject(key),type));

//				if (aJson.optJSONObject(key) != null)
//				{
                AccessibleObject.setAccessible(new AccessibleObject[]{aField}, true);
                if (aJson.optJSONObject(key) != null) {
                    aField.set(aDataDto, convert(aJson.optJSONObject(key),type));
                }
                AccessibleObject.setAccessible(new AccessibleObject[]{aField}, false);
//				}
            }
            else
            {
//				setterMethod = dataClass.getMethod(setterMethodName, type);
//				Object value = json.opt(key);
//
//				if(value != null)
//				{
//					setterMethod.invoke(dataDto, value);
//				}

                AccessibleObject.setAccessible(new AccessibleObject[]{aField}, true);
                if(aJson != null && key != null && aJson.opt(key) != null)// && aJson.opt(key).getClass().equals(aField.getDeclaringClass()))
                {
                    aField.set(aDataDto, aJson.opt(key));
                }
                AccessibleObject.setAccessible(new AccessibleObject[]{aField}, false);
            }

        } catch (IllegalArgumentException e) {
            //System.out.println("ZZZ"+key+e);
            //if(AppConstants.ENABLE_COMMON_LOG)
            //e.printStackTrace();
        } catch (IllegalAccessException e) {
            //if(AppConstants.ENABLE_COMMON_LOG)
            //e.printStackTrace();
        } catch (SecurityException e) {
            //if(AppConstants.ENABLE_COMMON_LOG)
            //e.printStackTrace();
        }
    }

    private static <T> Object createData(Class aType, JSONObject aJson, String aKey)
    {
        Object value = null;
        try {
            if(aKey.indexOf(".") != -1)
            {
                String[] subkeys = aKey.split("\\.");
                //System.out.println("!!!!!!!!"+aKey+","+subkeys.length);
                for(int i=0;i<subkeys.length-1;i++)
                {
                    aJson = aJson.optJSONObject(subkeys[i]);
                    //System.out.println("!!!!!!!!"+subkeys[i]);

                    if(aJson == null){
                        //System.out.println("NULLNULLNULL");
                        return null;
                    }
                }

                aKey = subkeys[subkeys.length-1];
            }



            if(aType == java.util.Date.class)
            {
                value = new Date(aJson.optLong(aKey)*1000);
            }
            if(DataDTO.class.equals(aType.getSuperclass()))
            {
                value = convert(aJson.optJSONObject(aKey), aType);
            }
            else
            {
                value = aJson.opt(aKey);
            }

        } catch (IllegalArgumentException e) {
            //if(AppConstants.ENABLE_COMMON_LOG)
            //e.printStackTrace();
        }

        return value;
    }
}

package com.plaping.imagerequest.api.model.custom;

import com.plaping.imagerequest.api.model.DataDTO;
import com.plaping.imagerequest.api.model.JSONArrayValue;

/**
 * Created by deknaew_bws on 4/17/16.
 */
public class RequestResponse extends DataDTO {

    @JSONArrayValue(field = "requests")
    private Request[] requests;

    public Request[] getRequests() {
        return requests;
    }

    public void setRequests(Request[] requests) {
        this.requests = requests;
    }
}

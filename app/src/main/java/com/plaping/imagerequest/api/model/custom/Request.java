package com.plaping.imagerequest.api.model.custom;

import com.plaping.imagerequest.api.model.DataDTO;
import com.plaping.imagerequest.api.model.JSONArrayValue;
import com.plaping.imagerequest.api.model.JSONValue;

/**
 * Created by deknaew_bws on 4/16/16.
 */
public class Request extends DataDTO {

    @JSONValue(field = "toUser")
    private User toUser;

    @JSONValue(field = "owner")
    private User fromUser;

    @JSONValue(field = "image")
    private String imgUrl;

    @JSONArrayValue(field = "tags")
    private String[] tags;


    @JSONValue(field = "message")
    private String message;

    @JSONValue(field = "createAt")
    private long dateCreated;

    @JSONValue(field = "id")
    private String id;

    @JSONValue(field = "choosenPost")
    private String referPostId;

    @JSONValue(field = "isOpen")
    private boolean isOpen;

    @JSONValue(field = "isLiked")
    private boolean isLiked;

    @JSONValue(field = "likes")
    private int likeCount;

    @JSONValue(field = "comments")
    private int commentCount;


    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public String getReferPostId() {
        return referPostId;
    }

    public void setReferPostId(String referPostId) {
        this.referPostId = referPostId;
    }
}

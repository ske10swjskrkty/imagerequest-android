package com.plaping.imagerequest.api.model.custom;

import android.graphics.Typeface;

/**
 * Created by Bestiiz on 19/3/2559.
 */
public class StickerTextValue {
    private float x;
    private float y;
    private int width;
    private int height;
    private float rotation;
    private String text;
    private int top;
    private int textColor;
    private int textStrokeColor;
    private float widthText;
    private boolean stroke;
    private int opacity;
    private boolean onFous;
    private Typeface typefaceFont;

    public Typeface getTypefaceFont() {
        return typefaceFont;
    }

    public void setTypefaceFont(Typeface typefaceFont) {
        this.typefaceFont = typefaceFont;
    }

    public boolean isOnFous() {
        return onFous;
    }

    public void setOnFous(boolean onFous) {
        this.onFous = onFous;
    }

    public int getOpacity() {
        return opacity;
    }

    public void setOpacity(int opacity) {
        this.opacity = opacity;
    }

    public boolean isStroke() {
        return stroke;
    }

    public void setStroke(boolean stroke) {
        this.stroke = stroke;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getTextStrokeColor() {
        return textStrokeColor;
    }

    public void setTextStrokeColor(int textStrokeColor) {
        this.textStrokeColor = textStrokeColor;
    }

    public float getWidthText() {
        return widthText;
    }

    public void setWidthText(float widthText) {
        this.widthText = widthText;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    private int left;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

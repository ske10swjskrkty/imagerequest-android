package com.plaping.imagerequest.api.model.custom;

import com.plaping.imagerequest.api.model.DataDTO;
import com.plaping.imagerequest.api.model.JSONArrayValue;

/**
 * Created by deknaew_bws on 4/16/16.
 */
public class PostResponse extends DataDTO {

    @JSONArrayValue(field = "posts")
    private Post[] posts;

    public Post[] getPosts() {
        return posts;
    }

    public void setPosts(Post[] posts) {
        this.posts = posts;
    }
}

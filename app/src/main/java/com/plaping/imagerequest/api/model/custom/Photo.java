package com.plaping.imagerequest.api.model.custom;

/**
 * Created by deknaew_bws on 10/20/15.
 */
public class Photo {

    private String path;
    private String albums;

    public Photo(String path, String albums) {
        this.path = path;
        this.albums = albums;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAlbums() {
        return albums;
    }

    public void setAlbums(String albums) {
        this.albums = albums;
    }
}

package com.plaping.imagerequest.api.model.custom;

import com.plaping.imagerequest.api.model.DataDTO;
import com.plaping.imagerequest.api.model.JSONArrayValue;
import com.plaping.imagerequest.api.model.JSONValue;

/**
 * Created by deknaew_bws on 4/17/16.
 */
public class CommentResponse extends DataDTO {

    @JSONArrayValue(field = "comments")
    private Comment[] comments;

    public Comment[] getComments() {
        return comments;
    }

    public void setComments(Comment[] comments) {
        this.comments = comments;
    }
}

package com.plaping.imagerequest.api.model.custom;

import com.plaping.imagerequest.api.model.DataDTO;
import com.plaping.imagerequest.api.model.JSONValue;

/**
 * Created by deknaew_bws on 4/15/16.
 */


public class User extends DataDTO {

    @JSONValue(field = "username")
    private String username;

    @JSONValue(field = "id")
    private String id;

    @JSONValue(field = "email")
    private String email;

    @JSONValue(field = "totalShares")
    private int totalShareCount;

    @JSONValue(field = "totalPosts")
    private int postCount;

    @JSONValue(field = "totalCredits")
    private int totalCredits;

    @JSONValue(field = "inComeRequest")
    private int requestCount;

    @JSONValue(field = "request")
    private int myRequestCount;

    @JSONValue(field = "picture")
    private String imgUrl;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTotalShareCount() {
        return totalShareCount;
    }

    public void setTotalShareCount(int totalShareCount) {
        this.totalShareCount = totalShareCount;
    }

    public int getPostCount() {
        return postCount;
    }

    public void setPostCount(int postCount) {
        this.postCount = postCount;
    }

    public int getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    public int getMyRequestCount() {
        return myRequestCount;
    }

    public void setMyRequestCount(int myRequestCount) {
        this.myRequestCount = myRequestCount;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getTotalCredits() {
        return totalCredits;
    }

    public void setTotalCredits(int totalCredits) {
        this.totalCredits = totalCredits;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", totalShareCount=" + totalShareCount +
                ", postCount=" + postCount +
                ", totalCredits=" + totalCredits +
                ", requestCount=" + requestCount +
                ", myRequestCount=" + myRequestCount +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }
}

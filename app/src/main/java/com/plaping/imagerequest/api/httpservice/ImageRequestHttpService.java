package com.plaping.imagerequest.api.httpservice;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.plaping.imagerequest.api.model.JsonDtoConverter;
import com.plaping.imagerequest.api.model.custom.CommentResponse;
import com.plaping.imagerequest.api.model.custom.PostResponse;
import com.plaping.imagerequest.api.model.custom.RequestResponse;
import com.plaping.imagerequest.api.model.custom.User;
import com.plaping.imagerequest.api.model.custom.UserResponse;
import com.plaping.imagerequest.api.request.CustomMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

/**
 * Created by deknaew_bws on 4/15/16.
 */
public class ImageRequestHttpService {

    private Context context;
    private static final String BASE_API_URL = "http://ske10.noip.me/api/";
    private RequestQueue requestQueue;

    public void search(String word, String accessToken, final OnResponseCallback<PostResponse> callback) {
        String url = BASE_API_URL + "post/search";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getMe", "response : " + response);

                try {
                    JSONObject json = new JSONObject(response);
                    PostResponse post = JsonDtoConverter.convert(json, PostResponse.class);
                    if (post != null) {
                        callback.onResponse(true, null, post);
                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("tags", word);

        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void getPopularFeed(String accessToken, final OnResponseCallback<PostResponse> callback) {
        String url = BASE_API_URL + "post/hot";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getpopularFeed", "response : " + response);

                try {
                    JSONObject json = new JSONObject(response);
                    PostResponse post = JsonDtoConverter.convert(json, PostResponse.class);
                    if (post != null) {
                        callback.onResponse(true, null, post);
                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void postRequest(String accessToken, String id, String message, String tag, File file, final OnResponseCallback<String> callback) {
        String url = BASE_API_URL + "request/new/" + id;

        CustomMultipartRequest request = new CustomMultipartRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("error")) {
                    callback.onResponse(false, null, response);

                } else {
                    callback.onResponse(true, null, response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("message", message);
        params.put("tags", tag);
        params.put("to", id);

        request.setParams(params);
        if (file != null) request.addFile("image", file.getAbsolutePath());

        requestQueue.add(request);
    }

    public void getHotUser(final OnResponseCallback<UserResponse> callback) {
        String url = BASE_API_URL + "user/hot";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getpopulaUser", "response : " + response);

                try {
                    JSONObject json = new JSONObject(response);
                    UserResponse post = JsonDtoConverter.convert(json, UserResponse.class);
                    if (post != null) {
                        callback.onResponse(true, null, post);
                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);

    }


    public interface OnResponseCallback<T> {
        public void onResponse(boolean success, Throwable error, T data);
    }

    public ImageRequestHttpService(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
    }

    public void getComment(String accessToken, String postId, final OnResponseCallback<CommentResponse> callback) {

        String url = BASE_API_URL + "post/" + postId + "/comment";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject json = new JSONObject(response);

                    Log.d("getComment", "Response : " + response);
                    CommentResponse c = JsonDtoConverter.convert(json, CommentResponse.class);
                    callback.onResponse(true, null, c);

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("time", System.currentTimeMillis() + "");
        params.put("access_token", accessToken);
        request.setParams(params);
        requestQueue.add(request);
    }

    public void getRequestComment(String accessToken, String requestId, final OnResponseCallback<CommentResponse> callback) {

        String url = BASE_API_URL + "request/" + requestId + "/comment";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject json = new JSONObject(response);

                    Log.d("getComment", "Response : " + response);
                    CommentResponse c = JsonDtoConverter.convert(json, CommentResponse.class);
                    callback.onResponse(true, null, c);

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("time", System.currentTimeMillis() + "");
        params.put("access_token", accessToken);
        request.setParams(params);
        requestQueue.add(request);
    }


    public void postCommentToRequest(File file, String accessToken, String requestID, String message, final OnResponseCallback<String> callback) {
        String url = BASE_API_URL + "request/" + requestID + "/comment";

        CustomMultipartRequest request = new CustomMultipartRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(true, null, response);
                Log.d("PostComment", "Response : " + response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });
        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("requestId", requestID);
        params.put("message", message);

        params.put("time", System.currentTimeMillis() + "");

        if (file != null) {
            request.addFile("image", file.getAbsolutePath());
        }

        request.setParams(params);
        requestQueue.add(request);

    }

    public void postCommentToPost(File file, String accessToken, String postId, String message, final OnResponseCallback<String> callback) {
        String url = BASE_API_URL + "post/" + postId + "/comment";

        CustomMultipartRequest request = new CustomMultipartRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(true, null, response);
                Log.d("PostComment", "Response : " + response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });
        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("pid", postId);
        params.put("message", message);

        params.put("time", System.currentTimeMillis() + "");

        if (file != null) {
            request.addFile("image", file.getAbsolutePath());
        }

        request.setParams(params);
        requestQueue.add(request);

    }

    public void login(String username, String password, final OnResponseCallback<String> callback) {

        String url = BASE_API_URL + "auth/";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject json = new JSONObject(response);
                    String accessToken = json.getString("access_token");
                    if (accessToken != null) {
                        callback.onResponse(true, null, accessToken);
                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);

    }

    public void updateUserProfilePicture(String accessToken, File file, final OnResponseCallback<String> callback) {

        String url = BASE_API_URL + "user/picture?access_token=" + accessToken;

        CustomMultipartRequest request = new CustomMultipartRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                callback.onResponse(true, null, response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        request.setParams(params);
        request.addFile("image", file.getAbsolutePath());

        requestQueue.add(request);

    }

    public void register(String username, String password, String email, final OnResponseCallback<String> callback) {

        String url = BASE_API_URL + "user/new";
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject json = new JSONObject(response);
                    User user = JsonDtoConverter.convert(json, User.class);

                    Log.d("NewUser", "Json : " + user.getId());
                    String accessToken = user.getId();
                    if (accessToken != null) {
                        callback.onResponse(true, null, accessToken);
                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        params.put("email", email);

        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);

    }


    public void getMe(String accessToken, final OnResponseCallback<User> callback) {
        String url = BASE_API_URL + "user/";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("getMe", "response : " + response);

                try {
                    JSONObject json = new JSONObject(response);
                    User user = JsonDtoConverter.convert(json, User.class);
                    if (user != null) {
                        callback.onResponse(true, null, user);
                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void getUser(String userId, final OnResponseCallback<User> callback) {
        String url = BASE_API_URL + "user/";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("getMe", "response : " + response);

                try {
                    JSONObject json = new JSONObject(response);
                    User user = JsonDtoConverter.convert(json, User.class);
                    if (user != null) {
                        callback.onResponse(true, null, user);
                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", userId);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void getPosts(String accessToken, String userId, final OnResponseCallback<PostResponse> callback) {
        String url = BASE_API_URL + "post/" + userId;
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getPost", "response : " + response);

                try {
                    JSONObject json = new JSONObject(response);
                    PostResponse post = JsonDtoConverter.convert(json, PostResponse.class);
                    if (post != null) {
                        callback.onResponse(true, null, post);

                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
//        params.put("uid", userId);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }


    public void sharePost(String accessToken, String postId, final OnResponseCallback<String> callback) {
        String url = BASE_API_URL + "post/" + postId + "/share";
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("sharePost", "response : " + response);

                callback.onResponse(true, null, response);

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("pid", postId);

        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void getSinglePost(String accessToken, String postId, final OnResponseCallback<PostResponse> callback) {
        String url = BASE_API_URL + "post/get/" + postId;
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getPostSingle", "response : " + response);

                try {
                    JSONObject json = new JSONObject(response);
                    PostResponse post = JsonDtoConverter.convert(json, PostResponse.class);
                    if (post != null) {
                        callback.onResponse(true, null, post);

                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
//        params.put("uid", userId);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void postPost(String accessToken, String caption, String tag, File file, final OnResponseCallback<String> callback) {

        String url = BASE_API_URL + "post/new";

        CustomMultipartRequest request = new CustomMultipartRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("PostNewResponse", response);
                callback.onResponse(true, null, response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("message", caption);
        params.put("tags", tag);
        request.setParams(params);
        request.addFile("image", file.getAbsolutePath());
        requestQueue.add(request);

    }

    public void postPostToRequest(String requestId, String accessToken, String caption, String tag, File file, final OnResponseCallback<String> callback) {

        String url = BASE_API_URL + "post/new/" + requestId;

        CustomMultipartRequest request = new CustomMultipartRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                callback.onResponse(true, null, response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("message", caption);
        params.put("tags", tag);
        request.setParams(params);
        request.addFile("image", file.getAbsolutePath());

        requestQueue.add(request);

    }

    public void getFeed(String accessToken, final OnResponseCallback<PostResponse> callback) {
        String url = BASE_API_URL + "post/feed";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getMe", "response : " + response);

                try {
                    JSONObject json = new JSONObject(response);
                    PostResponse post = JsonDtoConverter.convert(json, PostResponse.class);
                    if (post != null) {
                        callback.onResponse(true, null, post);
                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }


    public void getRequestFeed(String accessToken, final OnResponseCallback<RequestResponse> callback) {
        String url = BASE_API_URL + "request/feed";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getRequestFeed", "response : " + response);

                try {
                    JSONObject json = new JSONObject(response);
                    RequestResponse requests = JsonDtoConverter.convert(json, RequestResponse.class);
                    if (requests != null) {
                        callback.onResponse(true, null, requests);
                    } else {
                        callback.onResponse(false, null, null);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void getRequestToOthers(String accessToken, String userId, final OnResponseCallback<com.plaping.imagerequest.api.model.custom.RequestResponse> callback) {
        String url = BASE_API_URL + "request/" + userId;
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("getRequestToOthers", ">>>" + response);
                try {
                    JSONObject json = new JSONObject(response);
                    com.plaping.imagerequest.api.model.custom.RequestResponse req = JsonDtoConverter.convert(json, com.plaping.imagerequest.api.model.custom.RequestResponse.class);
                    callback.onResponse(true, null, req);


                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);

    }


    public void getRequestToMe(String accessToken, String userId, final OnResponseCallback<com.plaping.imagerequest.api.model.custom.RequestResponse> callback) {
        String url = BASE_API_URL + "request/income/" + userId;
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    com.plaping.imagerequest.api.model.custom.RequestResponse req = JsonDtoConverter.convert(json, com.plaping.imagerequest.api.model.custom.RequestResponse.class);
                    callback.onResponse(true, null, req);


                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onResponse(false, e, null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);

    }

    public void likeRequest(String accessToken, String requestId, final OnResponseCallback<String> callback) {
        String url = BASE_API_URL + "request/" + requestId + "/like";
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(true, null, null);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("requestId", requestId);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void unLikeRequest(String accessToken, String requestId, final OnResponseCallback<String> callback) {
        String url = BASE_API_URL + "request/" + requestId + "/like?access_token=" + accessToken;
        StringRequest request = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                callback.onResponse(true, null, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("requestId", requestId);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void likePost(String accessToken, String postId, final OnResponseCallback<String> callback) {
        String url = BASE_API_URL + "post/" + postId + "/like";
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(true, null, null);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("pid", postId);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }

    public void unLikePost(String accessToken, String postId, final OnResponseCallback<String> callback) {
        String url = BASE_API_URL + "post/" + postId + "/like?access_token=" + accessToken;
        StringRequest request = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                callback.onResponse(true, null, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onResponse(false, error, null);
                error.printStackTrace();
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("pid", postId);
        params.put("time", System.currentTimeMillis() + "");

        request.setParams(params);
        requestQueue.add(request);
    }
}

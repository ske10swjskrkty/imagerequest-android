package com.plaping.imagerequest.api.model.custom;

import com.plaping.imagerequest.api.model.DataDTO;
import com.plaping.imagerequest.api.model.JSONArrayValue;

/**
 * Created by deknaew_bws on 4/20/16.
 */
public class UserResponse extends DataDTO {

    @JSONArrayValue(field = "users")
    private User[] users;

    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }
}

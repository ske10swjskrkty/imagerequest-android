package com.plaping.imagerequest.api.model;

import java.io.Serializable;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Calendar;

public class DataDTO implements Serializable, Cloneable
{

	private static final long serialVersionUID = -9157891425074728975L;
	private Calendar mTimeStamp;
	private String originalJSON;
	
	public String getOriginalJSON() {
		return originalJSON;
	}

	public void setOriginalJSON(String originalJSON) {
		this.originalJSON = originalJSON;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toString()
	{	
		StringBuilder detailBuilder = new StringBuilder();
		Field[] fields = getClass().getDeclaredFields();
		for(Field field : fields)
		{
			String fieldName = field.getName();
			String getterMethodName = null;
			
			Class fieldType = field.getType();
			
			if(boolean.class.equals(fieldType))
			{
				getterMethodName = String.format("is%s%s",
						(fieldName.charAt(0)+"").toUpperCase(),
						fieldName.substring(1));
			}
			else
			{
				getterMethodName = String.format("get%s%s",
						(fieldName.charAt(0)+"").toUpperCase(),
						fieldName.substring(1));
			}
			
			Method getterMethod;
			try {
//				getterMethod = getClass().getMethod(getterMethodName, null);
//				Object value = getterMethod.invoke(this, null);
				
				AccessibleObject.setAccessible(new AccessibleObject[]{field}, true);
				Object value = field.get(this);
				AccessibleObject.setAccessible(new AccessibleObject[]{field}, false);
				
				if(value != null)
				{
					if(fieldType.isArray())
					{
						final int arrayLength = Array.getLength(value);
						detailBuilder.append(fieldName+"("+arrayLength+") = [\n");
						
						for(int i=0;i<arrayLength;i++)
						{
							detailBuilder.append("("+i+") => ");
							detailBuilder.append(Array.get(value, i));
							detailBuilder.append("\n");
						}
						detailBuilder.append("]\n");
					}
					else
					{
						detailBuilder.append(fieldName+"="+value+"\n");
					}
				}
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return detailBuilder.toString();
	}

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
	
	// mod implement
	public void setTimeStamp() {
		mTimeStamp = Calendar.getInstance();
	}
	
	public Calendar getTimeStamp() {
		return mTimeStamp;
	}
}

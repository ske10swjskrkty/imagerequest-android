package com.plaping.imagerequest.api.model.custom;

import com.plaping.imagerequest.api.model.DataDTO;
import com.plaping.imagerequest.api.model.JSONValue;

/**
 * Created by deknaew_bws on 4/17/16.
 */
public class Comment extends DataDTO {

    @JSONValue(field = "createAt")
    private long dateCreated;

    @JSONValue(field = "owner")
    private User user;

    @JSONValue(field = "image")
    private String imgUrl;

    @JSONValue(field = "message")
    private String message;

    @JSONValue(field = "id")
    private String id;

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

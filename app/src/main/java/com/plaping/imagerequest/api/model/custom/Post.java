package com.plaping.imagerequest.api.model.custom;

import com.plaping.imagerequest.api.model.DataDTO;
import com.plaping.imagerequest.api.model.JSONArrayValue;
import com.plaping.imagerequest.api.model.JSONValue;

/**
 * Created by deknaew_bws on 4/16/16.
 */
public class Post extends DataDTO {

    @JSONValue(field = "id")
    private String id;

    @JSONValue(field = "owner")
    private User user;

    @JSONValue(field = "createAt")
    private long dateCreated;

    @JSONArrayValue(field = "tags")
    private String[] tags;

    @JSONValue(field = "message")
    private String caption;

    @JSONValue(field = "image")
    private String imgUrl;

    @JSONValue(field = "isLiked")
    private boolean isLiked;


    @JSONValue(field = "likes")
    private int likeCount;

    @JSONValue(field = "comments")
    private int commentCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }
}

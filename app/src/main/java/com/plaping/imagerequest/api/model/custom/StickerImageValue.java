package com.plaping.imagerequest.api.model.custom;

/**
 * Created by Bestiiz on 17/3/2559.
 */
public class StickerImageValue {
    private float x;
    private float y;
    private int width;
    private int height;
    private float rotation;
    private int drawable;
    private int top;
    private boolean onFous;

    public boolean isOnFous() {
        return onFous;
    }

    public void setOnFous(boolean onFous) {
        this.onFous = onFous;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    private int left;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }
}

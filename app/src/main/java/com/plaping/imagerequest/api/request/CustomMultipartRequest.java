package com.plaping.imagerequest.api.request;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.request.MultiPartRequest;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by deknaew_bws on 4/16/16.
 */
public class CustomMultipartRequest extends MultiPartRequest<String> {
    public CustomMultipartRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    private Map<String, String> params = new HashMap<>();

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    public void setParams(Map<String, String> params) {
        if (params == null) return;
        this.params = params;

        Set<String> paramNames = params.keySet();
        for (String paramName : paramNames) {
            addMultipartParam(paramName, "text/plain", params.get(paramName));
        }
    }
}

package com.plaping.imagerequest.api.model.custom;

import android.graphics.Typeface;

/**
 * Created by Bestiiz on 21/3/2559.
 */
public class FontStyle {
    private Typeface typeface;
    private String nameFont;

    public FontStyle(Typeface typeface , String nameFont){
        this.typeface = typeface;
        this.nameFont = nameFont;

    }

    public Typeface getTypeface() {
        return typeface;
    }

    public void setTypeface(Typeface typeface) {
        this.typeface = typeface;
    }

    public String getNameFont() {
        return nameFont;
    }

    public void setNameFont(String nameFont) {
        this.nameFont = nameFont;
    }
}

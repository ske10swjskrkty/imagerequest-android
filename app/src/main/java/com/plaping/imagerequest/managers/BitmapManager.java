package com.plaping.imagerequest.managers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by deknaew_bws on 11/26/15.
 */
public class BitmapManager {

    private Context context;
    private File tempFolder;
    private SharedPreferences bitmapSharedPreference;
    private File savedFolder;
    private SharedPreferences.Editor edit;

    public BitmapManager(Context context) {

        bitmapSharedPreference = context.getSharedPreferences("sp_bitmap", Context.MODE_PRIVATE);
        edit = bitmapSharedPreference.edit();

        this.context = context;


//        Log.d("BitmapManager", "Cache Directory : " + context.getCacheDir().getAbsolutePath());
//        Log.d("BitmapManager", "Saved Folder Directory : " + context.getDir("data", Context.MODE_APPEND).getAbsolutePath());
        String dir = context.getCacheDir() + File.separator + context.getPackageName();
        savedFolder = new File(Environment.getExternalStorageDirectory() + File.separator + "PetPolar");

        if (!savedFolder.exists()) {
            savedFolder.mkdir();
        }

        tempFolder = new File(context.getCacheDir().getAbsolutePath() + File.separator + "pp_temp");

        if (!tempFolder.exists()) {
            tempFolder.mkdir();
        }
    }

    private Bitmap bitmap;

    public void saveBitmapToSharedPreference(Bitmap bitmap, String key) {

//        this.bitmap = bitmap;

        Bitmap realImage = bitmap;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        realImage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();

        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);


        if (bitmapSharedPreference.contains(key)) {
            edit.remove(key);
        }
        edit.putString(key, encodedImage);
        edit.commit();
    }

    public Bitmap getBitmapFromSharedPrefernce(String key) {

        String previouslyEncodedImage = bitmapSharedPreference.getString(key, "");

        if (previouslyEncodedImage.equals("")) return null;

        byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);

        return bitmap;
    }

    public void clearTemporaryFile() {

        Log.d("BitmapManager", "Delete All Files");
        deleteAllFile(tempFolder);

//        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+ Environment.getExternalStorageDirectory())));

        MediaScannerConnection.scanFile(context, new String[]{tempFolder.getAbsolutePath()}, null, null);
        edit.clear();
    }

    private void deleteAllFile(File fileOrDirectory) {

        Log.d("BitmapManager", "Directory to delete : " + fileOrDirectory.getName());

        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteAllFile(child);

        fileOrDirectory.delete();
        MediaScannerConnection.scanFile(context, new String[]{fileOrDirectory.getAbsolutePath()}, null, null);


//        context.getContentResolver().delete(MediaStore.Images.Media.getContentUri(fileOrDirectory.getAbsolutePath()),null,null);


    }

    public File saveBitmapToFile(Bitmap bitmap, String fileName) {

        boolean isComplete = false;

        File file = new File(savedFolder.getAbsolutePath() + File.separator + fileName + ".jpg");
        try {
            FileOutputStream fos = new FileOutputStream(file);
            isComplete = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file.getAbsoluteFile()));
        context.sendBroadcast(mediaScanIntent);

        return file;

    }

    public boolean saveByteArrayToTemporaryFolder(byte[] data, String fileName) {

        String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "pp_temp";
        File dirFile = new File(dir);
        if (!dirFile.exists()) {
            dirFile.mkdir();
        }

        File pictureFile = new File(dirFile.getAbsolutePath() + "/" + new Date().getTime() + ".png");

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data, 0, data.length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }


        return true;
    }


    public boolean saveBitmapToTemporaryFile(Bitmap bitmap, String fileName) {

        boolean isComplete = false;

        File file = new File(tempFolder.getAbsolutePath() + File.separator + fileName);
        try {
            FileOutputStream fos = context.openFileOutput(file.getAbsolutePath(), Context.MODE_PRIVATE);
            isComplete = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return isComplete;

    }


}

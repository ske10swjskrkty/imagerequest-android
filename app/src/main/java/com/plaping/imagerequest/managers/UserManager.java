package com.plaping.imagerequest.managers;

import android.content.Context;

import com.plaping.imagerequest.api.model.custom.User;

/**
 * Created by deknaew_bws on 4/15/16.
 */
public class UserManager {

    private Context context;
    private User user;

    public UserManager(Context context) {
        this.context = context;

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

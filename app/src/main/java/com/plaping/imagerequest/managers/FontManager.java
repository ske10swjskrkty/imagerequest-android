package com.plaping.imagerequest.managers;

import android.content.Context;
import android.util.Log;


import com.plaping.imagerequest.api.model.custom.FontStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bestiiz on 21/3/2559.
 */
public class FontManager {
    private List<FontStyle> fontStyleList;
    private Context context;

    public FontManager(Context context) {
        this.context = context;
        fontStyleList = new ArrayList<FontStyle>();

    }

    public List<FontStyle> getFontStyleList() {

        Log.d("FontList","Font List Size : " + fontStyleList.size());
        return fontStyleList;
    }

    public void setFontStyleList(List<FontStyle> fontStyleList) {
        this.fontStyleList = fontStyleList;
    }

}

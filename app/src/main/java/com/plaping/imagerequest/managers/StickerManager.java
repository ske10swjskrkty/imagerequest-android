package com.plaping.imagerequest.managers;

import android.content.Context;


import com.plaping.imagerequest.api.model.custom.StickerImageValue;
import com.plaping.imagerequest.api.model.custom.StickerTextValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bestiiz on 16/3/2559.
 */
public class StickerManager {
    private List<StickerImageValue> stickerList;
    private List<StickerTextValue> stickerTextList;
    private Context context;
    public StickerManager(Context context){
        this.context = context;
        stickerList = new ArrayList<StickerImageValue>();
        stickerTextList = new ArrayList<StickerTextValue>();
    }

    public List<StickerImageValue> getStickerList() {
        return stickerList;
    }

    public void setStickerList(List<StickerImageValue> stickerList) {
        this.stickerList = stickerList;
    }

    public List<StickerTextValue> getStickerTextList() {
        return stickerTextList;
    }

    public void setStickerTextList(List<StickerTextValue> stickerTextList) {
        this.stickerTextList = stickerTextList;
    }
}

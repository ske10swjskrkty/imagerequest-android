package com.plaping.imagerequest.utils;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;


import com.plaping.imagerequest.api.model.custom.Photo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by deknaew_bws on 10/12/15.
 */
public class GalleryUtils {

    public static ArrayList<Photo> getImagesPath(Activity activity) {
        Uri uri;
        ArrayList<Photo> listOfAllImages = new ArrayList<Photo>();

        HashMap<String, String> imagesMaps = new HashMap<String, String>();
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        String pathOfImage = null;
        String folderOfImage = null;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        cursor = activity.getContentResolver().query(uri, projection, null, null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            pathOfImage = cursor.getString(column_index_data);
            folderOfImage = cursor.getString(column_index_folder_name);

            imagesMaps.put(folderOfImage, pathOfImage);

            Log.d("GalleryUtils", "Album : " + cursor.getString(column_index_folder_name) + " >> Path : " + pathOfImage);

            listOfAllImages.add(new Photo(pathOfImage, cursor.getString(column_index_folder_name)));
        }
        return listOfAllImages;
    }
}

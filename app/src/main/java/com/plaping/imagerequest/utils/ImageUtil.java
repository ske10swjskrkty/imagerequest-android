package com.plaping.imagerequest.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

public class ImageUtil {

    public static Bitmap getBitmapFromImageView(ImageView view, Integer backgroundColor, Float scale,
                                           Integer width, Integer height)
    {
        Bitmap returnedBitmap = Bitmap.createBitmap(width == null ? view.getMeasuredWidth() : width,
                height == null ? view.getMeasuredHeight() : height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getDrawable();

        if (backgroundColor != null)
        {
            canvas.drawColor(backgroundColor);
        }
        else if (bgDrawable != null)
        {
            bgDrawable.draw(canvas);
        }
        else
        {
            canvas.drawColor(Color.WHITE);
        }

        if (width != null && height != null) {
            canvas.scale((float) width/ (float) view.getWidth(),
                    (float) height / (float) view.getHeight());
        }

        view.draw(canvas);

        return returnedBitmap;
    }

    public static Bitmap getBitmapFromView(View view, Integer backgroundColor, Float scale,
                                           Integer width, Integer height) {
        Bitmap returnedBitmap = Bitmap.createBitmap(width == null ? view.getWidth() : width,
                height == null ? view.getHeight() : height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();

        if (backgroundColor != null) {
            canvas.drawColor(backgroundColor);
        } else if (bgDrawable != null) {
            bgDrawable.draw(canvas);
        } else {
            canvas.drawColor(Color.WHITE);
        }

        if (width != null && height != null) {
            canvas.scale((float) width / (float) view.getWidth(),
                    (float) height / (float) view.getHeight());
        }

        view.draw(canvas);

        return returnedBitmap;
    }


}
